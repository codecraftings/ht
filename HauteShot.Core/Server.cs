﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace HauteShot.Core
{
	public class Server
	{
		private ApiServer apiServer;
		private ISettings userSettings;
		public const string BASE_URL = "http://api.myhauteshot.com";
		private static Server _instance;

		public static Server Instance {
			get {
				if (_instance == null) {
					_instance = new Server ();
				}
				return _instance;
			}
		}

		private Server ()
		{
			userSettings = ServiceContainer.Resolve<ISettings> ();
			initApiServer ();
		}

		private void initApiServer ()
		{
			apiServer = new ApiServer (userSettings.UserToken, App.Instance.ID.ToString (), BASE_URL + "/api/v1");
		}

		public void Login (string username, string password)
		{
			var response = apiServer.Post<TokenResponse> ("login", new Dictionary<string, object> {
				{ "username", username },
				{ "password", password }
			});
			userSettings.UserToken = response.Data.Token;
			userSettings.CurrentUserID = response.Data.UserID;
			userSettings.CurrentUserData = response.Data.User;
			userSettings.save ();
			initApiServer ();
		}

		public void Logout ()
		{
			userSettings.UserToken = "";
			userSettings.CurrentUserID = 0;
			userSettings.CurrentUserData = null;
			userSettings.save ();
			initApiServer ();
		}

		public void Register (string name, string email, string username, string password)
		{
			var response = apiServer.Post<TokenResponse> ("register", new Dictionary<string, object> {
				{ "username", username },
				{ "password", password },
				{ "full_name", name },
				{ "email", email }
			});
			userSettings.UserToken = response.Data.Token;
			userSettings.CurrentUserID = response.Data.UserID;
			userSettings.CurrentUserData = response.Data.User;
			userSettings.save ();
			initApiServer ();
		}

		public CurrentUser UpdateUserData (string name, string username, string email)
		{
			var response = apiServer.Post<CurrentUser> ("me", new Dictionary<string, object> {
				{ "full_name", name },
				{ "username", username },
				{ "email", email }
			});
			return response.Data;
		}

		public void ChangeUserPassword (string oldPass, string newPass)
		{
			apiServer.Post<object> ("me/password", new Dictionary<string, object> {
				{ "old_pass" , oldPass },
				{ "new_pass", newPass }
			});
		}

		public CurrentUser ChangeProfilePic (string newPicPath)
		{
			var response = apiServer.Post<CurrentUser> ("me/picture", new Dictionary<string, object> {
				{ "photo", new UploadableFile (newPicPath) }
			});
			return response.Data;
		}

		public Photo CreatePhotoObject (string filePath)
		{
			var response = apiServer.Post<Photo> ("photos/create_dummy", new Dictionary<string, object> {
				{ "user_id", userSettings.CurrentUserID.ToString () },
				{ "file_path", filePath }
			});
			return response.Data;
		}

		public Photo UploadPhoto (int photoID, string path)
		{
			var endpoint = String.Format ("photos/{0}/upload_photo", photoID.ToString ());
			var response = apiServer.Post<Photo> (endpoint, new Dictionary<string, object> {
				{ "user_id", userSettings.CurrentUserID.ToString () },
				{ "photo", new UploadableFile (path) }
			});
			return response.Data;
		}

		public List<Photo> GetHomeFeed (string afterOffset, string beforeOffset, int pagging = 10)
		{
			var querys = new Dictionary<string, string> ();
			querys.Add ("pagging", pagging.ToString ());
			if (afterOffset != null)
				querys.Add ("after_offset", afterOffset);
			if (beforeOffset != null)
				querys.Add ("before_offset", beforeOffset);
			var response = apiServer.Get<List<Photo>> ("feeds", querys);
			return response.Data;
		}

		public User GetUser (int uid)
		{
			var path = String.Format ("users/{0}", uid);
			var response = apiServer.Get<User> (path, null);
			return response.Data;
		}

		public void FollowUser (int uid)
		{
			var path = String.Format ("users/{0}/follow", uid);
			var response = apiServer.Post<object> (path, null);
		}

		public void UnFollowUser (int uid)
		{
			var path = String.Format ("users/{0}/follow", uid);
			var response = apiServer.Delete<object> (path, null);
		}

		public Photo GetPhoto (int pid)
		{
			var path = String.Format ("photos/{0}", pid);
			var response = apiServer.Get<Photo> (path, null);
			return response.Data;
		}

		public Photo PromotePhoto (int pid)
		{
			var path = String.Format ("photos/{0}/votes", pid);
			var response = apiServer.Post<Photo> (path, null);
			return response.Data;
		}

		public Photo UnPromotePhoto (int pid)
		{
			var path = String.Format ("photos/{0}/votes/1", pid);
			var response = apiServer.Delete<Photo> (path, null);
			return response.Data;
		}

		public bool DeletePhoto (int pid)
		{
			var path = String.Format ("photos/{0}", pid);
			var response = apiServer.Delete<object> (path, null);
			return true;
		}

		public bool UpdatePhoto (int pid, Photo photoData)
		{
			var path = String.Format ("photos/{0}/update", pid);
			var response = apiServer.Post<object> (path, new Dictionary<string, object> {
				{ "caption", photoData.caption }
			});
			return true;
		}

		public List<Album> GetUserAlbums (int uid)
		{
			var path = String.Format ("users/{0}/albums", uid);
			var response = apiServer.Get<List<Album>> (path, null);
			return response.Data;
		}

		public List<Photo> GetAlbumPhotos (Album album)
		{
			var path = String.Format ("users/{0}/feeds", album.user_id);
			var response = apiServer.Get<List<Photo>> (path, new Dictionary<string, string> {
				{ "date1", album.date_range [0].ToString () },
				{ "date2", album.date_range [1].ToString () }
			});
			return response.Data;
		}

		public List<Badge> GetUserBadges (User user)
		{
			var path = String.Format ("users/{0}/badges", user.ID);
			var response = apiServer.Get<List<Badge>> (path, null);
			return response.Data;
		}

		public List<Badge> GetLatestBadges ()
		{
			var response = apiServer.Get<List<Badge>> ("contests/badges", null);
			return response.Data;
		}

		public List<Contest> GetActiveContests ()
		{
			var response = apiServer.Get<List<Contest>> ("contests/active", null);
			return response.Data;
		}

		public bool ReportObject (string objectId, string objectType, string flagType, string message)
		{
			var response = apiServer.Post<bool> ("report", new Dictionary<string, object> {
				{ "object_id", objectId },
				{ "object_type", objectType },
				{ "flag_type", flagType },
				{ "message", message }
			});
			return response.Data;
		}

		public Badge GetBadge (int id)
		{
			var response = apiServer.Get<Badge> ("badges/" + id.ToString (), null);
			return response.Data;
		}

		private void handleError (int code, string msg)
		{
			if (code == 401) {
				throw new NotLoggedInException ("not logged in");
			}
			if (code == 100) {
				throw new NotConnectedException ();
			} else {
				throw new Exception (msg);
			}
		}

		private class TokenResponse
		{
			[JsonProperty ("token")]
			public string Token{ get; set; }

			[JsonProperty ("app_id")]
			public string AppID{ get; set; }

			[JsonProperty ("user_id")]
			public int UserID{ get; set; }

			[JsonProperty ("expire_at")]
			public string ExpireAt{ get; set; }

			[JsonProperty ("user")]
			public CurrentUser User{ get; set; }
		}
	}
}

