﻿using System;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public class FeedViewModel:BaseViewModel
	{
		public List<Photo> FeedItems;
		public ITaskHandler taskHandler;
		public static object _feedLocker = 0;
		private IResponseFactory responseFactory;
		public FeedViewModel (IViewController uicontrol)
		{
			UIController = uicontrol;
			FeedItems = new List<Photo> ();
			taskHandler = ServiceContainer.Resolve<ITaskHandler> ();
			responseFactory = ServiceContainer.Resolve<IResponseFactory> ();
		}
		public void FetchFromCache(){
//			Local.Instance.ClearCache ();
			var feeds = Local.Instance.GetFeeds ();
			addOlderFeedItems (feeds);
			UIController.NotifyViewModelUpdated ();
		}
		public IPromise<List<Photo>> FetchFreshFeeds(){
			var response = responseFactory.Create<List<Photo>> ();
			var offset = getNewFeedOffset ();
			if (offset == null) {
				FetchOlderFeeds ().IfSuccess (response.Resolve).IfFail (response.Reject);
			}
			var task = new FetchFreshFeedTask (offset);
			task.TaskCompleteHandler += (sender, e)=>{
				addNewFeedsItem(task.FetchedFeeds);
				UIController.NotifyViewModelUpdated();
				response.Resolve(FeedItems);
			};
			task.TaskFailedHandler += (sender, e) => {
				response.Reject(e.ErrorException);
			};
			taskHandler.Handle (task);
			return response.Promise ();
		}
		private string getNewFeedOffset(){
			if (FeedItems.Count > 0) {
				return FeedItems [0].created_at.ToString ();
			}
			return null;
		}
		private void addNewFeedsItem(List<Photo> feeds){
			feeds.Reverse ();
			lock (_feedLocker) {
				foreach (var feed in feeds) {
					if (FeedItems.Exists (photo => {
						return photo.id == feed.id;
					})) {
						continue;
					} else {
						FeedItems.Insert (0, feed);
					}
				}
			}
		}
		public IPromise<List<Photo>> FetchOlderFeeds(){
			var response = responseFactory.Create<List<Photo>> ();
			var task = new FetchOlderFeedTask (getOlderFeedOffset());
			task.TaskCompleteHandler += (sender, e)=>{
				addOlderFeedItems(task.FetchedFeeds);
				UIController.NotifyViewModelUpdated();
				response.Resolve(FeedItems);
			};
			task.TaskFailedHandler += (sender, e) => {
				response.Reject(e.ErrorException);
			};
			taskHandler.Handle (task);
			return response.Promise ();
		}
		private string getOlderFeedOffset(){
			if (FeedItems.Count > 0) {
				return FeedItems [FeedItems.Count-1].created_at.ToString ();
			}
			return DateTime.UtcNow.ToString ();
		}
		private void addOlderFeedItems(List<Photo> feeds){
			lock (_feedLocker) {
				foreach (var feed in feeds) {
					if (FeedItems.Exists (photo => {
						return photo.id == feed.id;
					})) {
						continue;
					} else {
						FeedItems.Add (feed);
					}
				}
			}
		}
	}
}

