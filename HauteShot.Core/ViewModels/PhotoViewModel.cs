﻿using System;

namespace HauteShot.Core
{
	public class PhotoViewModel:BaseViewModel
	{
		public Photo PhotoData {
			get;
			set;
		}
		private ITaskHandler taskHandler;
		public PhotoViewModel (IViewController ui)
		{
			this.UIController = ui;
			taskHandler = ServiceContainer.Resolve<ITaskHandler> ();
		}
		public void LoadFromCache(int id){
			var photo = Local.Instance.GetPhoto(id);
			if (photo != null) {
				PhotoData = photo;
			}
			UIController.NotifyViewModelUpdated ("dataLoaded");
		}
		public void LoadPhoto (int id)
		{
			var task = new FetchPhotoDataTask (id);
			task.TaskCompleteHandler += (sender, e) => {
				PhotoData = task.PhotoData;
				UIController.NotifyViewModelUpdated ("dataLoaded");
			};
			taskHandler.Handle (task);
		}
	}
}

