﻿using System;

namespace HauteShot.Core
{
	public class ChangePPViewModel:BaseViewModel
	{
		private ITaskHandler taskHandler;
		public ChangePPViewModel (IViewController uiControl)
		{
			UIController = uiControl;
			taskHandler = ServiceContainer.Resolve<ITaskHandler> ();
		}
		public void UploadPhoto(string path, Action onDone){
			var task = new UpdateUserDataTask(path);
			task.TaskCompleteHandler += (object sender, EventArgs e) => {
				UIController.InvokeOnOwnThread(onDone);
			};
			task.TaskFailedHandler += (arg1, arg2) => {
				UIController.InvokeOnOwnThread(onDone);
			};
			taskHandler.Handle (task);
		}
	}
}

