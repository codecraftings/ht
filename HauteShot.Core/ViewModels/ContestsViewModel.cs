﻿using System;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public class ContestsViewModel:BaseViewModel
	{
		public List<Contest> ContestsList;
		public ITaskHandler taskHandler;
		public IResponseFactory deferred;
		public ContestsViewModel (IViewController ui)
		{
			this.UIController = ui;
			taskHandler = ServiceContainer.Resolve<ITaskHandler> ();
			deferred = ServiceContainer.Resolve<IResponseFactory> ();
		}
		public IPromise<List<Contest>> FetchContestList(){
			var defer = deferred.Create<List<Contest>> ();
			var task = new FetchContestsListTask ();
			task.TaskCompleteHandler += (object sender, EventArgs e) => {
				ContestsList = task.Contests;
				UIController.NotifyViewModelUpdated();
				defer.Resolve(ContestsList);
			};
			task.TaskFailedHandler += (arg1, arg2) => {
				defer.Reject(arg2.ErrorException);
			};
			taskHandler.Handle (task);
			return defer.Promise ();
		}
	}
}

