﻿using System;

namespace HauteShot.Core
{
	public class LoginViewModel:BaseViewModel
	{
		public string UserName;
		public string Password;
		public string Error;
		private ITaskHandler taskHandler;
		public LoginViewModel (IViewController uiController)
		{
			UIController = uiController;
			taskHandler = ServiceContainer.Resolve<ITaskHandler> ();
		}
		public void TryLogin(Action onDone, Action onFailed){
			var task = new UserLoginTask(UserName, Password);
			task.TaskCompleteHandler += (object sender, EventArgs e) => {
				UIController.InvokeOnOwnThread(onDone);
			};
			task.TaskFailedHandler += (sender, e) => {
				this.Error = e.ErrorException.Message;
				UIController.InvokeOnOwnThread(onFailed);
			};
			taskHandler.Handle (task);
		}
	}
}

