﻿using System;

namespace HauteShot.Core
{
	public class ProfileEditViewModel:BaseViewModel
	{
		public CurrentUser UserData{ get; set; }

		private ITaskHandler taskHandler;
		private IResponseFactory response;

		public ProfileEditViewModel (IViewController controller)
		{
			UIController = controller;
			response = ServiceContainer.Resolve<IResponseFactory> ();
			taskHandler = ServiceContainer.Resolve<ITaskHandler> ();
		}

		public void LoadUserData ()
		{
			UserData = Local.Instance.GetCurrentUser ();
			if (UserData != null)
				UIController.NotifyViewModelUpdated ();
		}

		public IPromise<object> SaveProfile (CurrentUser profileData, string profilePicPath)
		{
			var df = response.Create<object> ();
			var task = new UpdateUserDataTask (profileData, profilePicPath);
			task.TaskCompleteHandler += (object sender, EventArgs e) => {
				df.Resolve (sender);
			};
			task.TaskFailedHandler += (arg1, arg2) => {
				df.Reject (arg2.ErrorException);
			};
			taskHandler.Handle (task);
			return df.Promise ();
		}

		public IPromise<object> ChangePassword (string oldPass, string newPass, string confirmNewPass)
		{
			var df = response.Create<object> ();
			if (newPass != confirmNewPass) {
				df.Reject (new Exception ("Password confirmation failed"));
			} else {
				var task = new UpdateUserDataTask (oldPass, newPass);
				task.TaskCompleteHandler += (object sender, EventArgs e) => {
					df.Resolve (sender);
				};
				task.TaskFailedHandler += (arg1, arg2) => {
					df.Reject (arg2.ErrorException);
				};
				taskHandler.Handle (task);
			}
			return df.Promise ();
		}
	}
}

