﻿using System;

namespace HauteShot.Core
{
	public class PhotoInfoEditorViewModel:BaseViewModel
	{
		public Photo PhotoData {
			get;
			set;
		}

		private ITaskHandler taskHandler;
		public PhotoInfoEditorViewModel (IViewController ui)
		{
			this.UIController = ui;
			taskHandler = ServiceContainer.Resolve<ITaskHandler> ();
		}

		public void LoadPhoto (int id)
		{
			var task = new FetchPhotoDataTask (id);
			task.TaskCompleteHandler += (sender, e) => {
				PhotoData = task.PhotoData;
				UIController.NotifyViewModelUpdated ("dataLoaded");
			};
			taskHandler.Handle (task);
		}

		public void DeletePhoto (Action onDone)
		{
			if (PhotoData == null) {
				return;
			}
			var task = new DeletePhotoTask (PhotoData.id);
			task.TaskCompleteHandler += (sender, e) => {
				UIController.InvokeOnOwnThread (onDone);
			};
			task.TaskFailedHandler += (sender, e) => {
				UIController.InvokeOnOwnThread (onDone);
			};
			taskHandler.Handle (task);
		}

		public void UpdatePhoto (Action onDone)
		{
			if (PhotoData == null) {
				return;
			}
			var task = new UpdatePhotoTask (PhotoData);
			task.TaskCompleteHandler += (object sender, EventArgs e) => {
				UIController.InvokeOnOwnThread (onDone);
				startUploadingPhotoInBackground();
			};
			task.TaskFailedHandler += (sender, e) => {
				UIController.InvokeOnOwnThread (onDone);
			};
			taskHandler.Handle (task);
		}
		private void startUploadingPhotoInBackground(){
			if (PhotoData.in_progress == false) {
				return;
			}
			var task = new UploadPhotoTask (PhotoData.id, PhotoData.src);
			task.TaskCompleteHandler += (object sender, EventArgs e) => {

			};
			taskHandler.Handle (task);
		}
	}
}

