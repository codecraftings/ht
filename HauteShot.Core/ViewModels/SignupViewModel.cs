﻿using System;
using System.Threading.Tasks;

namespace HauteShot.Core
{
	public class SignupViewModel:BaseViewModel
	{
		public string Name;
		public string Email;
		public string Username;
		public string Password;
		public string Error;
		public ITaskHandler taskHandler;
		public SignupViewModel (IViewController uiController)
		{
			UIController = uiController;
			taskHandler = ServiceContainer.Resolve<ITaskHandler> ();
		}
		public void TrySignup(Action onDone, Action onFailed){
			var task = new UserRegisterTask (Name, Email, Username, Password);
			task.TaskCompleteHandler += (object sender, EventArgs e) => {
				UIController.InvokeOnOwnThread(onDone);
			};
			task.TaskFailedHandler += (sender, e) => {
				Error = e.ErrorException.Message;
				UIController.InvokeOnOwnThread(onFailed);
			};
			taskHandler.Handle (task);

		}
	}
}

