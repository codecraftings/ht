﻿using System;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public class UserProfileViewModel:BaseViewModel
	{
		public User UserData;
		private ITaskHandler taskHandler;
		public UserProfileViewModel (IViewController ui)
		{
			UIController = ui;
			taskHandler = ServiceContainer.Resolve<ITaskHandler> ();
		}
		public void LoadFromCache(int id){
			var user = Local.Instance.GetUser (id);
			if (user != null) {
				UserData = user;
			}
			UIController.NotifyViewModelUpdated ("userdata");
		}
		public void FetchFromServer(int id){
			var task = new FetchUserTask (id);
			task.TaskCompleteHandler += (object sender, EventArgs e) => {
				UserData = task.FetchedUser;
				UIController.NotifyViewModelUpdated("userdata");
			};
			taskHandler.Handle (task);
		}
		public void FollowUser(Action onDone){
			var task = new FollowUserTask (UserData.ID);
			task.TaskCompleteHandler += (object sender, EventArgs e) => {
				UIController.InvokeOnOwnThread(onDone);
			};
			task.TaskFailedHandler += (sender, e) => {
				UIController.InvokeOnOwnThread(onDone);
			};
			taskHandler.Handle (task);
		}
		public void UnFollowUser(Action onDone){
			var task = new UnFollowUserTask (UserData.ID);
			task.TaskCompleteHandler += (object sender, EventArgs e) => {
				UIController.InvokeOnOwnThread(onDone);
			};
			task.TaskFailedHandler += (sender, e) => {
				UIController.InvokeOnOwnThread(onDone);
			};
			taskHandler.Handle (task);
		}
	}
}

