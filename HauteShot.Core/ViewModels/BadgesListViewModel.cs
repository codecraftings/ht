﻿using System;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public class BadgesListViewModel:BaseViewModel
	{
		public List<Badge> BadgesList;
		public ITaskHandler taskHandler;
		public IResponseFactory deferred;
		public BadgesListViewModel (IViewController ui)
		{
			this.UIController = ui;
			taskHandler = ServiceContainer.Resolve<ITaskHandler> ();
			deferred = ServiceContainer.Resolve<IResponseFactory> ();
		}

		public IPromise<List<Badge>> FetchBadges (User user)
		{
			var defer = deferred.Create<List<Badge>> ();
			var task = new FetchBadgesTask (user);
			task.TaskCompleteHandler += (object sender, EventArgs e) => {
				BadgesList = task.Badges;
				UIController.NotifyViewModelUpdated();
				defer.Resolve(BadgesList);
			};
			task.TaskFailedHandler += (arg1, arg2) => {
				defer.Reject(arg2.ErrorException);
			};
			taskHandler.Handle (task);
			return defer.Promise ();
		}
	}
}

