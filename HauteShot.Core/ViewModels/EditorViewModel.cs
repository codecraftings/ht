﻿using System;
using System.Threading.Tasks;

namespace HauteShot.Core
{
	public class EditorViewModel:BaseViewModel
	{
		private ITaskHandler taskHandler;
		public EditorViewModel (IViewController UI):base()
		{
			UIController = UI;
			taskHandler = ServiceContainer.Resolve<ITaskHandler> ();
		}
		public void CreateImage(string path, Action<Photo> onDone, Action<string> onFailed){
			var task = new CreatePhotoTask (path);
			task.TaskCompleteHandler += (object sender, EventArgs e) => {
				UIController.InvokeOnOwnThread(()=>{
					onDone(task.CreatedPhoto);
				});
			};
			task.TaskFailedHandler += (sender, e) => {
				if(onFailed!=null){
					onFailed.Invoke(e.ErrorException.Message);
				}
			};
			taskHandler.Handle (task);
		}
	}
}

