﻿using System;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public class BaseViewModel:IDisposable
	{
		protected Dictionary<string, List<Action>> Bindings; 
		protected IViewController UIController;
		public BaseViewModel ()
		{
			Bindings = new Dictionary<string, List<Action>> ();
		}
		public void Dispose(){
			UIController = null;
			Bindings = null;
		}
		public void BindDataChange(string propName, Action action){
			if (!Bindings.ContainsKey (propName)) {
				Bindings [propName] = new List<Action> ();
			}
			Bindings [propName].Add (action);
		}
		public void OnDataChange(string propName){
			if (Bindings.ContainsKey (propName)) {
				foreach (var action in Bindings[propName]) {
					if (UIController != null) {
						UIController.CallDataChangeAction (action);
					}
				}
			}
		}
	}
}

