﻿using System;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public class FetchOlderFeedTask:ATask
	{
		public List<Photo> FetchedFeeds;

		string beforeTimeOffset;

		public FetchOlderFeedTask (string TimeOffset)
		{
			this.beforeTimeOffset = TimeOffset;
		}
		public override void Handle ()
		{
			try{
				FetchedFeeds = Server.Instance.GetHomeFeed(null, beforeTimeOffset);
				Local.Instance.SaveFeeds(FetchedFeeds);
				Local.Instance.SaveCache();
				HandleTaskComplete();
			}catch(Exception e){
				HandleTaskFailed (e);
				throw e;
			}
		}
	}
}

