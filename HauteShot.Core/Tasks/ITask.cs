using System;

namespace HauteShot.Core
{
	public interface ITask
	{
		void Handle();
		void TerminationRequested();
	}

}

