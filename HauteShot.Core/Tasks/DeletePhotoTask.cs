﻿using System;

namespace HauteShot.Core
{
	public class DeletePhotoTask:ATask
	{
		private int photoID;
		public DeletePhotoTask (int photoID)
		{
			this.photoID = photoID;
		}
		public override void Handle ()
		{
			try{
				Server.Instance.DeletePhoto(photoID);
				HandleTaskComplete();
			}catch(Exception e){
				HandleTaskFailed (e);
				throw e;
			}
		}
	}
}

