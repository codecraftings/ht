﻿using System;

namespace HauteShot.Core
{
	public abstract class ATask:ITask
	{

		public event EventHandler TaskCompleteHandler;

		public event Action<ATask, TaskFailedEventArgs> TaskFailedHandler;

		public event EventHandler TaskProgressHandler;

		public virtual void Handle ()
		{
			throw new NotImplementedException ();
		}

		public virtual void TerminationRequested ()
		{
			return;
		}

		protected virtual void HandleTaskComplete ()
		{
			if (TaskCompleteHandler != null) {
				TaskCompleteHandler.Invoke (this, new EventArgs ());
			}
		}

		protected virtual void HandleTaskFailed (Exception e)
		{
			var args = new TaskFailedEventArgs (e);
			if (TaskFailedHandler != null) {
				TaskFailedHandler.Invoke (this, args);
			}
		}

		protected virtual void HandleTaskProgress ()
		{
			if (TaskProgressHandler != null) {
				TaskProgressHandler.Invoke (this, new EventArgs ());
			}
		}

	}
	public class TaskFailedEventArgs:EventArgs{
		public Exception ErrorException;
		public TaskFailedEventArgs(Exception ErrorException){
			if (ErrorException == null)
				throw new ArgumentNullException ("ErrorException");
			this.ErrorException = ErrorException;
		}
	}
}

