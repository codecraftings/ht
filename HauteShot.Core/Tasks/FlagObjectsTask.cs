﻿using System;

namespace HauteShot.Core
{
	public class FlagObjectsTask:ATask
	{
		string objectId;

		string objectType;

		string flagType;

		string message;

		public FlagObjectsTask (string objectId, string flagType = "spam", string objectType = "photo", string message = "user flagged a photo")
		{
			this.message = message;
			this.flagType = flagType;
			this.objectType = objectType;
			this.objectId = objectId;
		}
		public override void Handle(){
			Server.Instance.ReportObject(objectId, objectType, flagType, message);
			HandleTaskComplete ();
		}
	}
}

