﻿using System;

namespace HauteShot.Core
{
	public class FollowUserTask:ATask
	{
		private int userId;
		public FollowUserTask (int userId)
		{
			this.userId = userId;
		}
		public override void Handle ()
		{
			try{
				Server.Instance.FollowUser(userId);
				HandleTaskComplete();
			}catch(Exception e){
				HandleTaskFailed (e);
			}
		}
	}
}

