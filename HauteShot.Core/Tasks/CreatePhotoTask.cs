﻿using System;

namespace HauteShot.Core
{
	public class CreatePhotoTask:ATask
	{
		public Photo CreatedPhoto;

		string filePath;

		public CreatePhotoTask (string filePath)
		{
			this.filePath = filePath;
		}
		public override void Handle ()
		{
			try{
				CreatedPhoto = Server.Instance.CreatePhotoObject(filePath);
				Local.Instance.SavePhoto(CreatedPhoto);
				Local.Instance.SaveCache();
				HandleTaskComplete();
			}catch(Exception e){
				HandleTaskFailed (e);
				throw e;
			}
		}
	}
}

