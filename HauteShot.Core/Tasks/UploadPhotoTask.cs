﻿using System;

namespace HauteShot.Core
{
	public class UploadPhotoTask:ATask
	{
		string filePath;

		public Photo UploadedPhoto;

		int photoID;

		public UploadPhotoTask (int photoID, string filePath)
		{
			this.photoID = photoID;
			this.filePath = filePath;
		}

		public override void Handle ()
		{
			try {
				UploadedPhoto = Server.Instance.UploadPhoto (photoID, filePath);
				Local.Instance.SavePhoto(UploadedPhoto);
				Local.Instance.SaveCache();
				HandleTaskComplete ();
			} catch (Exception e) {
				HandleTaskFailed (e);
			}
		}
	}
}

