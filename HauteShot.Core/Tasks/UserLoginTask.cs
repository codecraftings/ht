﻿using System;

namespace HauteShot.Core
{
	public class UserLoginTask:ATask
	{
		string username;

		string password;

		public UserLoginTask (string username, string password)
		{
			this.password = password;
			this.username = username;
		}

		public override void Handle ()
		{
			try{
				Server.Instance.Login(username, password);
				Local.Instance.ClearCache();
				HandleTaskComplete ();
			}catch(Exception e){
				HandleTaskFailed (e);
			}
		}

	}
}

