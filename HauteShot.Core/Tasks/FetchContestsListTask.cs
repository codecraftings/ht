﻿using System;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public class FetchContestsListTask:ATask
	{
		public List<Contest> Contests;
		public FetchContestsListTask ()
		{
		}
		public override void Handle ()
		{
			try{
				Contests = Server.Instance.GetActiveContests();
				HandleTaskComplete();
			}catch(Exception e){
				HandleTaskFailed (e);
				throw e;
			}
		}
	}
}

