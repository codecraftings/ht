﻿using System;

namespace HauteShot.Core
{
	public class UpdatePhotoTask:ATask
	{
		public Photo PhotoData;
		public UpdatePhotoTask (Photo photo)
		{
			this.PhotoData = photo;
		}
		public override void Handle ()
		{
			try{
				Server.Instance.UpdatePhoto(PhotoData.id, PhotoData);
				Local.Instance.SavePhoto(PhotoData);
				Local.Instance.SaveCache();
				HandleTaskComplete();
			}catch(Exception e){
				HandleTaskFailed (e);
				throw e;
			}
		}
	}
}

