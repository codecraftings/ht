﻿using System;

namespace HauteShot.Core
{
	public class UserRegisterTask:ATask
	{
		string name;

		string email;

		string username;

		string password;

		public UserRegisterTask (string name, string email, string username, string password)
		{
			this.password = password;
			this.username = username;
			this.email = email;
			this.name = name;
		}
		public override void Handle ()
		{
			try{
				Server.Instance.Register(name, email, username, password);
				Local.Instance.ClearCache();
				HandleTaskComplete();
			}catch(Exception e){
				HandleTaskFailed (e);
			}
		}
	}
}

