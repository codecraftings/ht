﻿using System;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public class FetchFreshFeedTask:ATask
	{
		public List<Photo> FetchedFeeds;

		string TimeOffset;

		public FetchFreshFeedTask (string TimeOffset)
		{
			this.TimeOffset = TimeOffset;
		}
		public override void Handle ()
		{
			try{
				FetchedFeeds = Server.Instance.GetHomeFeed(TimeOffset, null);
				Local.Instance.SaveFeeds(FetchedFeeds);
				Local.Instance.SaveCache();
				HandleTaskComplete();
			}catch(Exception e){
				HandleTaskFailed (e);
				throw e;
			}
		}
	}
}

