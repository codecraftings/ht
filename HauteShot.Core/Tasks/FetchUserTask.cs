﻿using System;

namespace HauteShot.Core
{
	public class FetchUserTask:ATask
	{
		public User FetchedUser;
		private int userId;
		public FetchUserTask (int userId)
		{
			this.userId = userId;
		}
		public override void Handle ()
		{
			try{
				FetchedUser = Server.Instance.GetUser(userId);
				Local.Instance.SaveUser(FetchedUser);
				Local.Instance.SaveCache();
				HandleTaskComplete();
			}catch(Exception e){
				HandleTaskFailed (e);
				throw e;
			}
		}
	}
}

