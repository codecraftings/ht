﻿using System;

namespace HauteShot.Core
{
	public interface ITaskHandler
	{
		event Action<Exception> TaskExceptionHandler;
		void Handle(ITask task);
	}
}

