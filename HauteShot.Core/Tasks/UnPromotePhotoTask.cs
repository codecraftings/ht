﻿using System;

namespace HauteShot.Core
{
	public class UnPromotePhotoTask:ATask
	{
		public Photo UpdatedPhotoData;

		int photoId;

		public UnPromotePhotoTask (int photoId)
		{
			this.photoId = photoId;
		}
		public override void Handle ()
		{
			try{
				UpdatedPhotoData = Server.Instance.UnPromotePhoto(photoId);
				Local.Instance.SavePhoto(UpdatedPhotoData);
				Local.Instance.SaveCache();
				HandleTaskComplete();
			}catch(Exception e){
				HandleTaskFailed (e);
				throw e;
			}
		}
	}
}

