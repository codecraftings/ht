﻿using System;

namespace HauteShot.Core
{
	public class PromotePhotoTask:ATask
	{
		public Photo UpdatedPhotoData;

		int photoId;

		public PromotePhotoTask (int photoId)
		{
			this.photoId = photoId;
		}
		public override void Handle ()
		{
			try{
				UpdatedPhotoData = Server.Instance.PromotePhoto(photoId);
				Local.Instance.SavePhoto(UpdatedPhotoData);
				Local.Instance.SaveCache();
				HandleTaskComplete();
			}catch(Exception e){
				HandleTaskFailed (e);
				throw e;
			}
		}
	}
}

