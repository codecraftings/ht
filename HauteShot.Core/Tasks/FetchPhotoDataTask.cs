﻿using System;

namespace HauteShot.Core
{
	public class FetchPhotoDataTask:ATask
	{
		public Photo PhotoData;
		private int photoID;
		public FetchPhotoDataTask (int photoID)
		{
			this.photoID = photoID;
		}
		public override void Handle ()
		{
			try{
				PhotoData = Server.Instance.GetPhoto(photoID);
				Local.Instance.SavePhoto(PhotoData);
				Local.Instance.SaveCache();
				HandleTaskComplete();
			}catch(Exception e){
				HandleTaskFailed (e);
				throw e;
			}
		}
	}
}

