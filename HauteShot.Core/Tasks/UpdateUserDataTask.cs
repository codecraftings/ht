﻿using System;

namespace HauteShot.Core
{
	public class UpdateUserDataTask:ATask
	{
		private CurrentUser profileData;

		string profilePicPath;

		string oldPass;

		string newPass;
		public UpdateUserDataTask(string profilePicPath){
			init(null, profilePicPath, null, null);
		}
		public UpdateUserDataTask(string oldPass, string newPass){
			init(null, null, oldPass, newPass);
		}
		public UpdateUserDataTask(CurrentUser profileData, string profilePicPath){
			init(profileData, profilePicPath, null, null);
		}
		private void init (CurrentUser profileData, string profilePicPath, string oldPass, string newPass)
		{
			this.newPass = newPass;
			this.oldPass = oldPass;
			this.profilePicPath = profilePicPath;
			this.profileData = profileData;
		}

		public override void Handle ()
		{
			CurrentUser userdata = Local.Instance.GetCurrentUser ();
			try {
				if(profileData!=null){
					userdata = Server.Instance.UpdateUserData (profileData.FullName, profileData.UserName, profileData.Email);
				}
				if(profilePicPath!=null){
					userdata = Server.Instance.ChangeProfilePic(profilePicPath);
				}
				if(oldPass!=null&&newPass!=null){
					Server.Instance.ChangeUserPassword(oldPass, newPass);
				}
				HandleTaskComplete ();
			} catch (Exception e) {
				HandleTaskFailed (e);
				throw e;
			}finally{
				Local.Instance.UpdateCurrentUserData (userdata);
			}
		}
	}
}

