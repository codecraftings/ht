﻿using System;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public class FetchBadgesTask:ATask
	{
		public List<Badge> Badges;
		User user;

		public FetchBadgesTask (User user)
		{
			this.user = user;
		}
		public override void Handle(){
			try{
				if(this.user!=null){
					Badges = Server.Instance.GetUserBadges(this.user);
				}else{
					Badges = Server.Instance.GetLatestBadges();
				}
				HandleTaskComplete();
			}catch(Exception e){
				HandleTaskFailed (e);
				throw e;
			}
		}
	}
}

