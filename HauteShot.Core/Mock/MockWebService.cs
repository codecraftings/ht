﻿using System;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace HauteShot.Core
{
	public class MockWebService
	{
		public MockWebService ()
		{
		}
		public string CallUrl(string url, object param, string method = "get"){
			return "ss";
		}
		public string CallApi (string api, object param, string method = "get")
		{
			string str = @"{
					'State': 'ok',
					'ID': 1,
					'Name': 'Mahfuz',
					'Email': 'makhan075@gmail.com'
				}";
			return str;
		}

		public async Task<string> DownloadImageAsync (string url)
		{
			await Task.Delay (3000);
			return url;
		}
	}
}

