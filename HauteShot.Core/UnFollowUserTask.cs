﻿using System;

namespace HauteShot.Core
{
	public class UnFollowUserTask:ATask
	{
		int userId;

		public UnFollowUserTask (int userId)
		{
			this.userId = userId;
		}
		public override void Handle ()
		{
			try{
				Server.Instance.UnFollowUser(userId);
				HandleTaskComplete();
			}catch(Exception e){
				HandleTaskFailed (e);
			}
		}
	}
}

