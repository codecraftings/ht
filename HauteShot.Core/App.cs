﻿using System;
using System.Threading.Tasks;

namespace HauteShot.Core
{
	public class App
	{
		public event Action<Exception> HandleUncaughtException;

		private static App _instance;

		public static App Instance {
			get {
				if (_instance == null) {
					_instance = new App ();
				}
				return _instance;
			}
		}

		private int _id;

		public int ID {
			get {
				return _id;
			}
		}

		private App ()
		{
			ServiceContainer.Register<IUserRepo> (() => new UserRepo ());
			ServiceContainer.Register<IResponseFactory> (() => new ResponseFactory ());
			//ServiceContainer.Register<IWebService>(()=> new MockWebService());
		}

		public void OnSetupDroid ()
		{

		}

		public void OnSetupIOS ()
		{
			this._id = 1234;
		}

		public void OnSetupTest ()
		{
			this._id = 1234;
		}

		public static async void HandledAsyncTask (Action thingsToDo)
		{
			await Task.Run (() => {
				try {
					thingsToDo.Invoke ();
				} catch (Exception e) {
					App.Instance.OnUnHandledException (e);
				}
			});
		}

		public void OnUnHandledException (Exception e)
		{
			if (HandleUncaughtException != null) {
				HandleUncaughtException.Invoke (e);
			}
		}
	}
}

