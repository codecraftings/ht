﻿using System;
using Newtonsoft.Json;
using Humanizer;

namespace HauteShot.Core
{
	public class Badge
	{
		[JsonProperty ("id")]
		public long ID{ get; set; }

		[JsonProperty ("caption")]
		public string Caption{ get; set; }

		[JsonProperty ("winner")]
		public Photo Winner{ get; set; }

		[JsonProperty ("contest")]
		public Contest Contest{ get; set; }

		[JsonProperty ("user")]
		public User User{ get; set; }

		[JsonProperty ("icon_url")]
		public string IconUrl{ get; set; }

		public Badge ()
		{
		}
	}
}

