﻿using System;
using Newtonsoft.Json;
using Humanizer;
namespace HauteShot.Core
{
	public class Photo
	{
		public int id;

		public int user_id;
		public string original_url;
		public string thumb_url;
		public string caption;
		public string status;
		public string src;
		public int promotes_count;
		public int view_count;
		public int points;
		public string type;
		public DateTime created_at;
		public DateTime updated_at;
		public Boolean in_progress;
		public Boolean is_promoted;

		public Badge[] badges;

		public User user;
		public Photo ()
		{
		}
		public string GetThumbSrc(float width=0, float height=0){
			if (in_progress) {
				return "local:/" + src;
			}
			return String.Format ("{0}/resource/photo/{1}/t/{2}x{3}/htphoto.jpg", Server.BASE_URL, id.ToString (), width.ToString (), height.ToString ());
		}
		public string GetFullSrc(float width=0, float height=0){
			if (in_progress) {
				return "local:/" + src;
			}
			return String.Format ("{0}/resource/photo/{1}/o/{2}x{3}/htphoto.jpg", Server.BASE_URL, id.ToString (), width.ToString (), height.ToString ());
		}
		public string GetHumanDate(){
			return created_at.Humanize ();
		}
	}
}

