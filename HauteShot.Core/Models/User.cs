﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace HauteShot.Core
{
	public class User
	{
		[JsonProperty ("id")]
		public int ID {
			set;
			get;
		}

		[JsonProperty ("full_name")]
		public string FullName {
			set;
			get;
		}

		[JsonProperty ("promotes_count")]
		public int PromotesCount{ get; set; }

		[JsonProperty ("email")]
		public string Email {
			get;
			set;
		}

		[JsonProperty ("username")]
		public string UserName {
			get;
			set;
		}

		[JsonProperty ("profile_pic_url")]
		public string ProfilePicUrl {
			get {
				return ProfilePic ();
			}
			set {

			}
		}

		[JsonProperty ("is_followed")]
		public Boolean IsFollowed{ get; set; }

		[JsonProperty ("photos_count")]
		public int PhotosCount{ get; set; }

		[JsonProperty ("albums")]
		public List<Album> Albums{ get; set; }

		[JsonProperty ("badges_won")]
		public List<Badge> Badges{ get; set; }

		public User ()
		{
		}

		public override int GetHashCode ()
		{
			return this.ID;
		}

		public override bool Equals (object user)
		{
			return Equals (user as User);
		}

		public bool Equals (User user)
		{
			if (user == null) {
				return false;
			}
			return this.ID == user.ID;
		}

		public virtual string ProfilePic (int width = 240, int height = 240)
		{
			return String.Format ("{0}/users/{1}/{2}x{3}/picture.jpg", Server.BASE_URL, ID.ToString (), width.ToString (), height.ToString ());
		}
	}
}

