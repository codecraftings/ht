﻿using System;

namespace HauteShot.Core
{
	public class CurrentUser:User
	{
		public CurrentUser ()
		{
		}

		public bool ChangingPhotoInProgress{ get; set; }

		public string LocalPicPath {
			get;
			set;
		}

		public override string ProfilePic (int width = 100, int height = 100)
		{
			if (ChangingPhotoInProgress) {
//				return "local:/" + LocalPicPath;
			}
			return base.ProfilePic (width, height);
		}
	}
}

