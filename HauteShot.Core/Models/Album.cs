﻿using System;

namespace HauteShot.Core
{
	public class Album
	{
		public string title;
		public DateTime[] date_range;
		public Photo[] feature_photos;

		public int user_id;

		public string year;
		public string month;

		public int photo_count;
		public Album ()
		{
		}
	}
}

