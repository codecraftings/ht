﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using Humanizer;

namespace HauteShot.Core
{
	public class Contest
	{
		[JsonProperty ("id")]
		public long ID{ get; set; }

		[JsonProperty ("title")]
		public string Title{ get; set; }

		[JsonProperty ("description")]
		public string Description{ get; set; }

		[JsonProperty ("start_date")]
		public DateTime StartDate{ get; set; }

		[JsonProperty ("end_date")]
		public DateTime EndDate{ get; set; }

		[JsonProperty ("icon_url")]
		public string IconUrl{ get; set; }

		[JsonProperty ("total_entry")]
		public int TotalEntry{ get; set; }

		[JsonProperty ("rank_list")]
		public List<Photo> RankList;

		[JsonProperty ("status")]
		public string Status{ get; set; }

		[JsonProperty ("friendly_date_info")]
		public string FriendlyDateInfo{ get; set; }

		public string GetEndDateInDays ()
		{
			return EndDate.Humanize ();
		}

		public Contest ()
		{
		}
	}
}

