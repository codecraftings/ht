﻿using System;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public class ApiServer
	{
		private string accessToken;
		private IWebService webService;
		private string appId;
		public string apiURL;

		public ApiServer (string accessToken, string appId, string apiURL)
		{
			if (appId == null)
				throw new ArgumentNullException ("appId");
			if (accessToken == null)
				this.accessToken = "default_token";
			else
				this.accessToken = accessToken;
			this.appId = appId;
			webService = ServiceContainer.Resolve<IWebService> ();
			this.apiURL = apiURL;
		}

		private IWebRequest createWebRequest (string path)
		{
			var request = webService.CreateRequest (String.Format ("{0}/{1}", apiURL, path));
			request.SetGetParam ("access_token", accessToken);
			request.SetGetParam ("app_id", appId);
			return request;
		}

		public ApiResponse<T> Get<T> (string path, Dictionary<string, string> getParams)
		{
			return Call<T> (path, "GET", getParams, null);
		}

		public ApiResponse<T> Post<T> (string path, Dictionary<string, object> postParams)
		{
			return Call<T> (path, "POST", null, postParams);
		}

		public ApiResponse<T> Delete<T> (string path, Dictionary<string, string> getParams)
		{
			return Call<T> (path, "DELETE", getParams, null);
		}

		public ApiResponse<T> Put<T> (string path, Dictionary<string, object> postParams)
		{
			return Call<T> (path, "PUT", null, postParams);
		}

		private ApiResponse<T> Call<T> (string path, string method, Dictionary<string, string> getParams, Dictionary<string, object> postParams)
		{
			var request = createWebRequest (path);
			if (getParams != null)
				request.SetGetParam (getParams);
			if (postParams != null)
				request.SetPostParam (postParams);
			try {
				request.Send (method);
			} catch (Exception e) {
				handleError (600, e.Message);
			}
			var response = request.GetResponse<ApiResponse<T>> ();
			if (response == null) {
				handleError (100, "Cannot connect to server");
			}
			else if (response.Status != 200) {
				handleError (response.Status, response.Error.Message);
			}
			return response;
		}

		private void handleError (int code, string msg)
		{
			if (code == 401) {
				throw new NotLoggedInException ("not logged in");
			}
			if (code == 100) {
				throw new NotConnectedException ();
			} else {
				throw new Exception (msg);
			}
		}
	}

}

