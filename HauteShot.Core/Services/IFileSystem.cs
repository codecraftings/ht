﻿using System;

namespace HauteShot.Core
{
	public interface IFileSystem
	{
		IOperationalFile FromTempFolder (string filename);

		IOperationalFile FromDocumentFolder (string filename);

		IOperationalFile FromCacheFolder (string filename);
	}
	public interface IOperationalFile{
		string GetPath();
		string Read();
		void Write(byte[] data);
		void Write(string data);
		void Delete();
	}
}

