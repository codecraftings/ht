﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public interface IWebService
	{
		IWebRequest CreateRequest (string url);
		byte[] ReadImageBytes (string url);
	}
}

