﻿using System;
using Newtonsoft.Json;

namespace HauteShot.Core
{
	public class ApiResponse<T>
	{
		[JsonProperty ("status")]
		public int Status;
		[JsonProperty ("data")]
		public T Data;
		[JsonProperty("error")]
		public ApiError Error;

		public ApiResponse ()
		{

		}

		public class ApiError
		{
			[JsonProperty("message")]
			public string Message;
		}
	}

}

