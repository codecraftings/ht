﻿using System;

namespace HauteShot.Core
{
	public interface ISettings
	{
		string UserToken {
			get;
			set;
		}
		int CurrentUserID {
			get;
			set;
		}
		CurrentUser CurrentUserData {
			get;
			set;
		}
		void save();

		void clear();

	}
}

