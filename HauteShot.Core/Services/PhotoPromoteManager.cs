﻿using System;

namespace HauteShot.Core
{
	public class PhotoPromoteManager
	{
		private ITaskHandler taskHandler;
		private bool taskRuning;
		public PhotoPromoteManager ()
		{
			taskHandler = ServiceContainer.Resolve<ITaskHandler> ();
		}
		public void PromotePhoto(Photo photoData){
			if (taskRuning||photoData.is_promoted) {
				return;
			}
			taskRuning = true;
			photoData.is_promoted = true;
			photoData.promotes_count += 1;
			Local.Instance.SavePhoto (photoData);
			var task = new PromotePhotoTask (photoData.id);
			task.TaskCompleteHandler += (object sender, EventArgs e) => {
				taskRuning = false;
			};
			task.TaskFailedHandler += (arg1, arg2) => {
				taskRuning = false;
			};
			taskHandler.Handle (task);
		}
		public void UnPromotePhoto(Photo photoData){
			if (taskRuning||photoData.is_promoted == false) {
				return;
			}
			taskRuning = true;
			photoData.is_promoted = false;
			photoData.promotes_count -= 1;
			Local.Instance.SavePhoto (photoData);
			var task = new UnPromotePhotoTask (photoData.id);
			task.TaskCompleteHandler += (object sender, EventArgs e) => {
				taskRuning = false;
			};
			task.TaskFailedHandler += (arg1, arg2) => {
				taskRuning = false;
			};
			taskHandler.Handle (task);
		}
	}
}

