﻿using System;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public interface IWebRequest
	{
		void SetPostParam (string key, object value);
		void SetGetParam (string key, string value);
		void SetPostParam (Dictionary<string, object> parameters);
		void SetGetParam (Dictionary<string, string> parameters);
		void Send(string method);
		T GetResponse<T>();
	}
}

