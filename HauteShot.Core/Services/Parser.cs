﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public class Parser
	{
		public Parser ()
		{
		}

		public static List<T> JSON<T> (string str)
		{
			return JsonConvert.DeserializeObject<List<T>> (str);
		}

		public static string ToJSON (object obj)
		{
			if (obj == null) {
				return "";
			}
			return JsonConvert.SerializeObject (obj);
		}

		public static T getObject<T> (string str)
		{
			T obj = default(T);
			try {
				obj = JsonConvert.DeserializeObject<T> (str);
			} catch (Exception e) {
				return default(T);
			}
			return obj;
		}
	}
}

