using System;
namespace HauteShot.Core
{
	public class ResponseFactory:IResponseFactory
	{
		public IDefferedResponse<T> Create<T>() where T:class{
			return new DefferedResponse<T>();
		}
	}
}