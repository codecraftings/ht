using System;
namespace HauteShot.Core
{
	public class DefferedPromise<T>:IPromise<T> where T:class{

		IDefferedResponse<T> defferedResponse;

		public DefferedPromise(IDefferedResponse<T> defferedResponse){
			this.defferedResponse = defferedResponse;

		}
		public IPromise<T> IfSuccess(Action<T> fn){
			defferedResponse.AddResolveHandler (fn);
			return this;
		}
		public IPromise<T> IfFail(Action<Exception> fn){
			defferedResponse.AddRejectedHandler (fn);
			return this;
		}

		public IPromise<T> Finally (Action fn)
		{
			defferedResponse.AddFinallyHandler (fn);
			return this;
		}

		public IPromise<T> AndThen(Action fn){
			return Finally (fn);
		}
	}
}