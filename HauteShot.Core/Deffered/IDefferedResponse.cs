using System;

namespace HauteShot.Core
{
	public interface IDefferedResponse<T>
	{

		void Resolve (T response);

		void Reject (Exception e);

		IPromise<T> Promise ();

		void AddResolveHandler(Action<T> fn);

		void AddRejectedHandler(Action<Exception> fn);

		void AddFinallyHandler(Action fn);
	}

}