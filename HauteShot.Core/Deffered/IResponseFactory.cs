using System;

namespace HauteShot.Core
{
	public interface IResponseFactory
	{
		IDefferedResponse<T> Create<T>() where T:class;
	}

}