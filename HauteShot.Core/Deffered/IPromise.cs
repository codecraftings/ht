using System;

namespace HauteShot.Core
{
	public interface IPromise<T>
	{
		IPromise<T> IfSuccess(Action<T> fn);
		IPromise<T> IfFail(Action<Exception> fn);
		IPromise<T> AndThen(Action fn);
		IPromise<T> Finally(Action fn);
	}

}