using System;
using System.Threading;

namespace HauteShot.Core
{
	public class DefferedResponse<T>:IDefferedResponse<T> where T:class
	{
		private T resolvedResponse;
		private Exception rejectedResponse;
		private int responseReady = 0;

		public Action<T> ResolvedHandler;

		public Action<Exception> RejectHanlder;

		public Action FinallyHandler;

		public DefferedResponse ()
		{
		}

		public void Resolve (T response)
		{
			if (1 == Interlocked.Exchange (ref responseReady, 1)) {
				return;
			}
			resolvedResponse = response;
			callHandlers ();
		}

		public void Reject (Exception e)
		{
			if (1 == Interlocked.Exchange (ref responseReady, 1)) {
				return;
			}
			rejectedResponse = e;
			callHandlers ();
		}

		private void seeIfResponseReady ()
		{
			if (responseReady == 1) {
				callHandlers ();
			}
		}

		private void callHandlers ()
		{
			if (resolvedResponse != null)
				invokeResolved ();
			if (rejectedResponse != null)
				invokeRejected ();
			invokeFinally ();
			dereferenceHandlers ();
		}

		private void dereferenceHandlers(){
			ResolvedHandler = null;
			RejectHanlder = null;
			FinallyHandler = null;
		}
		public void AddResolveHandler (Action<T> fn)
		{
			if (responseReady==1) {
				if (resolvedResponse != null) {
					fn.Invoke (resolvedResponse);
				}
				return;
			}
			ResolvedHandler += fn;
		}

		public void AddRejectedHandler (Action<Exception> fn)
		{
			if (responseReady==1) {
				if (rejectedResponse != null) {
					fn.Invoke (rejectedResponse);
				}
				return;
			}
			RejectHanlder += fn;
		}

		public void AddFinallyHandler (Action fn)
		{
			if (responseReady==1) {
				fn.Invoke ();
				return;
			}
			FinallyHandler += fn;
		}

		private void invokeResolved ()
		{
			if (resolvedResponse != null && ResolvedHandler != null) {
				ResolvedHandler.Invoke (resolvedResponse);
			}
		}


		private void invokeRejected ()
		{
			if (rejectedResponse != null && RejectHanlder != null) {
				RejectHanlder.Invoke (rejectedResponse);
			}
		}


		private void invokeFinally ()
		{
			if (FinallyHandler != null) {
				FinallyHandler.Invoke ();
			}
		}

		public IPromise<T> Promise ()
		{
			return new DefferedPromise<T> (this);
		}
	}
}