﻿using System;

namespace HauteShot.Core
{
	public class NotLoggedInException:Exception
	{
		public NotLoggedInException() : base() { }
		public NotLoggedInException(string message) : base(message) { }
		public NotLoggedInException(string message, System.Exception inner) : base(message, inner) { }
	}
}

