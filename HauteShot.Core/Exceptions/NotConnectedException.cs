﻿using System;

namespace HauteShot.Core
{
	public class NotConnectedException:Exception
	{
		public NotConnectedException ():base("Data connection not available")
		{
		}
	}
}

