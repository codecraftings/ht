﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HauteShot.Core
{
	public class Local
	{
		private static Local _instance;

		public static Local Instance {
			get {
				if (_instance == null) {
					_instance = new Local ();
				}
				return _instance;
			}
		}

		private CacheStore cacheStore;
		private ISettings settings;

		public Local ()
		{
			cacheStore = new CacheStore ();
			settings = ServiceContainer.Resolve<ISettings> ();
		}

		public bool IsLoggedIn ()
		{
			return settings.CurrentUserData != null && settings.CurrentUserID > 0;
		}

		public CurrentUser GetCurrentUser ()
		{
			try {
				var user = settings.CurrentUserData;
				return user;
			} catch {
				return null;
			}
		}

		public void UpdateCurrentUserData (CurrentUser userData)
		{
			settings.CurrentUserData = userData;
		}

		public List<Photo> GetFeeds ()
		{
			List<Photo> feeds = new List<Photo> ();
			var feedIds = cacheStore.Feeds;
			foreach (var feedId in feedIds) {
				if (cacheStore.Photos.ContainsKey (feedId)) {
					feeds.Add (cacheStore.Photos [feedId]);
				}
			}
			feeds = feeds.OrderByDescending (photo => photo.created_at).ToList ();
			return feeds;
		}

		public void SaveFeeds (List<Photo> feeds)
		{
			foreach (var photo in feeds) {
				if (cacheStore.Feeds.Contains (photo.id)) {
					continue;
				} else {
					cacheStore.Feeds.Add (photo.id);
					SavePhoto (photo);
				}
			}
		}

		public Photo GetPhoto (int photoId)
		{
			if (cacheStore.Photos.ContainsKey (photoId))
				return cacheStore.Photos [photoId];
			else
				return null;
		}

		public void SavePhoto (Photo photo)
		{
			if (cacheStore.Photos.ContainsKey (photo.id)) {
				cacheStore.Photos [photo.id] = photo;
				return;
			}
			cacheStore.Photos.Add (photo.id, photo);
			if (photo.user != null) {
				SaveUser (photo.user);
			}
		}

		public User GetUser (int userId)
		{
			if (cacheStore.Users.ContainsKey (userId))
				return cacheStore.Users [userId];
			else
				return null;
		}

		public void SaveUser (User user)
		{
			if (cacheStore.Users.ContainsKey (user.ID)) {
				cacheStore.Users [user.ID] = user;
				return;
			}
			cacheStore.Users.Add (user.ID, user);
		}

		public void ClearCache ()
		{
			cacheStore.Clear ();
		}

		public void SaveCache ()
		{
			SaveCacheToFile ();
		}

		public void SaveCacheToFile ()
		{
			cacheStore.SaveCache ();
		}
	}
}

