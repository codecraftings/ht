﻿using System;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public class CacheStore
	{
		private Dictionary<int, Photo> _photos;
		public Dictionary<int, Photo> Photos{
			get{
				if (_photos == null) {
					_photos = readFromCacheFile<Dictionary<int, Photo>> ("photos.cache");
					if (_photos == null) {
						_photos = new Dictionary<int, Photo> ();
					}
				}
				return _photos;
			}
		}
		private Dictionary<int, User> _users;
		public Dictionary<int, User> Users{
			get{
				if (_users == null) {
					_users = readFromCacheFile<Dictionary<int, User>> ("users.cache");
					if (_users == null) {
						_users = new Dictionary<int, User> ();
					}
				}
				return _users;
			}
		}
		private Dictionary<int, Album> _albums;
		public Dictionary<int, Album> Albums{
			get{
				if (_albums == null) {
					_albums = readFromCacheFile<Dictionary<int, Album>> ("albums.cache");
					if (_albums == null) {
						_albums = new Dictionary<int, Album> ();
					}
				}
				return _albums;
			}
		}
		private List<int> feeds;
		public List<int> Feeds{
			get{
				if (feeds == null) {
					feeds = readFromCacheFile<List<int>> ("feeds.cache");
					if (feeds == null) {
						feeds = new List<int> ();
					}
				}
				return feeds;
			}
		}
		protected IFileSystem FileSystem;

		public CacheStore ()
		{
			FileSystem = ServiceContainer.Resolve<IFileSystem> ();
		}

		public void SaveCache ()
		{
			FileSystem.FromCacheFolder ("photos.cache").Write (
				Parser.ToJSON (Photos));
			FileSystem.FromCacheFolder ("users.cache").Write (
				Parser.ToJSON (Users));
			FileSystem.FromCacheFolder ("albums.cache").Write (
				Parser.ToJSON (Albums));
			FileSystem.FromCacheFolder ("feeds.cache").Write (
				Parser.ToJSON (Feeds));
		}
		public void Clear(){
			feeds = new List<int> ();
			_users = new Dictionary<int, User> ();
			_photos = new Dictionary<int, Photo> ();
			_albums = new Dictionary<int, Album> ();
			SaveCache ();
		}
		private T readFromCacheFile<T> (string fileName)
		{
			var json = FileSystem.FromCacheFolder (fileName).Read ();
			if (json != null) {
				return Parser.getObject<T> (json);
			}
			return default(T);
		}
	}
}

