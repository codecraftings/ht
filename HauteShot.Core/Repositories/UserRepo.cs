﻿using System;

using Newtonsoft.Json;
using System.Threading.Tasks;

namespace HauteShot.Core
{
	public class UserRepo:IUserRepo
	{
		private readonly IWebService webservice;
		private readonly IFileSystem cacheservice;
		private readonly ISettings settings;

		public UserRepo ()
		{
			this.webservice = ServiceContainer.Resolve<IWebService> ();
			this.cacheservice = ServiceContainer.Resolve<IFileSystem> ();
			this.settings = ServiceContainer.Resolve<ISettings> ();

		}

		public async Task GetAsync (int id, Action<User> onDone)
		{
			await Task.Run(()=>{
				var user = Server.Instance.GetUser(id);
				onDone(user);
			});
		}

		public User Get(int id, bool cache = true){
			return null;
		}

		public async Task GetAlbumsAsync (int id, Action<System.Collections.Generic.List<Album>> onDone)
		{
			await Task.Run(()=>{
				var albums = Server.Instance.GetUserAlbums(id);
				onDone(albums);
			});
		}
		public bool IsLoggedIn(){
			return this.settings.UserToken != null&&this.settings.CurrentUserID!=0;
		}

		public User GetCurrentUser(){
			var id = this.settings.CurrentUserID;
			if (this.settings.UserToken == null||id==0) {
				return null;
			}
			return this.Get (id, true);
		}

		public void SetCurrentUser(int id){
			this.settings.UserToken = "TestToken";
			this.settings.CurrentUserID = id;
		}
	}
}

