﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public interface IPhotoRepo
	{
		void GetPhotoFeedAsync(Action<List<Photo>> onDone);
		Task GetAsync(int id, Action<Photo> onDone);
		void DeleteAsync(int id, Action<bool> onDone);
		void UpdateAsync(Photo PhotoData, Action<bool> onDone);
	}
}

