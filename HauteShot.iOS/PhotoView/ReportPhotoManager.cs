﻿using System;
using HauteShot.Core;

namespace HauteShot.iOS
{
	public class ReportPhotoManager
	{
		Photo photo;

		ITaskHandler taskHandler;

		public ReportPhotoManager (Photo photo)
		{
			this.photo = photo;
			taskHandler = ServiceContainer.Resolve<ITaskHandler> ();
		}

		public void ShowFlagActions(){
			HTNavigator.Instance.ActionSheet (new System.Collections.Generic.Dictionary<string, Action> {
				{"Flag As Spam", FlagAsSpam},
				{"Flag As Nudity", FlagAsNudity},
				{"Flag As Inappropriate", FlagAsInappropriate}
			}, "Flag Action");
		}
		public void FlagAsSpam(){
			taskHandler.Handle (new FlagObjectsTask (photo.id.ToString()));
			HTNavigator.Instance.Alert ("Thank you! Our stuff will take action within next 24 hours", "Flagged As Spam");
		}
		public void FlagAsNudity(){
			taskHandler.Handle (new FlagObjectsTask (photo.id.ToString(), "nudity"));
			HTNavigator.Instance.Alert ("Thank you! Our stuff will take action within next 24 hours", "Flagged As Nudity");
		}
		public void FlagAsInappropriate(){
			taskHandler.Handle (new FlagObjectsTask (photo.id.ToString(), "inappropriate"));
			HTNavigator.Instance.Alert ("Thank you! Our stuff will take action within next 24 hours", "Flagged As Inappropriate");
		}

	}
}

