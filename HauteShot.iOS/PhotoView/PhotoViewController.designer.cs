// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace HauteShot.iOS
{
	[Register ("PhotoViewController")]
	partial class PhotoViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextView CaptionCard { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		PhotoViewer PhotoCard { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		PromoteButtonView Promoter { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIScrollView ScrollContainer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView ShareCard { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel TimeLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CardView UserCard { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel UserName { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CircularImage UserPhoto { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (CaptionCard != null) {
				CaptionCard.Dispose ();
				CaptionCard = null;
			}
			if (PhotoCard != null) {
				PhotoCard.Dispose ();
				PhotoCard = null;
			}
			if (Promoter != null) {
				Promoter.Dispose ();
				Promoter = null;
			}
			if (ScrollContainer != null) {
				ScrollContainer.Dispose ();
				ScrollContainer = null;
			}
			if (ShareCard != null) {
				ShareCard.Dispose ();
				ShareCard = null;
			}
			if (TimeLabel != null) {
				TimeLabel.Dispose ();
				TimeLabel = null;
			}
			if (UserCard != null) {
				UserCard.Dispose ();
				UserCard = null;
			}
			if (UserName != null) {
				UserName.Dispose ();
				UserName = null;
			}
			if (UserPhoto != null) {
				UserPhoto.Dispose ();
				UserPhoto = null;
			}
		}
	}
}
