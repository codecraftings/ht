using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using System.Drawing;
using HauteShot.Core;
using System.Text.RegularExpressions;

namespace HauteShot.iOS
{
	partial class PhotoViewController : BaseUIController
	{
		public new PhotoViewModel ViewModel{
			get{
				return (PhotoViewModel)base.ViewModel;
			}
			set{
				base.ViewModel = (BaseViewModel)value;
			}
		}
		public int PhotoID;
		public bool BackButtonEnabled;
		public PhotoViewController (IntPtr handle) : base (handle)
		{
			ViewModel = new PhotoViewModel (this);
			BackButtonEnabled = true;
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			var gesture = new UITapGestureRecognizer (goToUserProfile);
			gesture.CancelsTouchesInView = true;
			UserCard.AddGestureRecognizer (gesture);
			if (PhotoID != 0) {
				ViewModel.LoadFromCache (PhotoID);
				ViewModel.LoadPhoto (PhotoID);
			}
		}
		public override void NotifyViewAppeared ()
		{
			base.NotifyViewAppeared ();
			NavController.FooterMenu ().TabHome ();
		}
		public override void OnViewModelUpdated (string updateId)
		{
			base.OnViewModelUpdated (updateId);
			if (ViewModel.PhotoData == null) {
				NavController.ShowLoading ();
				return;
			} else {
				NavController.HideLoading ();
			}
			UserCard.Hidden = false;
			ShareCard.Hidden = false;
			Promoter.PhotoData = ViewModel.PhotoData;
			Promoter.UpdateView ();
			UserName.Text = ViewModel.PhotoData.user.FullName;
			TimeLabel.Text = ViewModel.PhotoData.GetHumanDate();
			setCaption (ViewModel.PhotoData.caption);
			CaptionCard.TextContainerInset = new UIEdgeInsets (10, 10, 10, 10);
			ImageLoader.Instance.LoadImage (ViewModel.PhotoData.user.ProfilePic(), img => {
				UserPhoto.Image = img;
			});
			ImageLoader.Instance.LoadImage (ViewModel.PhotoData.GetFullSrc((float)UIScreen.MainScreen.Bounds.Width), image => {
				PhotoCard.SetImage(image);
			});

		}
		private void setCaption(string text){
			if (String.IsNullOrWhiteSpace (text)) {
				CaptionCard.Text = "<No Caption>";
			} else {
				MatchCollection matches = Regex.Matches (text, "#(\\w+)");
				var formatedText = new NSMutableAttributedString (text);
				foreach (Match match in matches) {
					formatedText.AddAttributes (new UIStringAttributes {
						ForegroundColor = UIColor.FromRGB (0, 128, 128)
					}, new NSRange (match.Index, match.Length));
				}
				CaptionCard.AttributedText = formatedText;
			}
//			CaptionCard.SizeToFit ();
			CaptionCard.ApplyBoldShadow ();
			CaptionCard.Hidden = false;
		}
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			NavController.NeedFooter ();
			NavController.NeedBackButton ();
			NavController.HaveShadow ();
		}
		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
		}
		public void goToUserProfile(){
			Navigator.UserProfileScreen (ViewModel.PhotoData.user_id);
		}
	}
}
