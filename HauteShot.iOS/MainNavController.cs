using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using CoreGraphics;
using System.Collections.Generic;
using HauteShot.Core;

namespace HauteShot.iOS
{
	public partial class MainNavController : UIViewController
	{
		const string RootViewID = "InitialView";
//		const string RootViewID = "PPChangeView";

		public HTNavigator Navigator {
			get {
				return HTNavigator.Instance;
			}
		}

		public override UIStatusBarStyle PreferredStatusBarStyle ()
		{
			return UIStatusBarStyle.LightContent;
		}

		private BaseUIController currentViewController;

		public MainNavController (IntPtr handle) : base (handle)
		{
			Navigator.ViewsContainer = this;
			AppDomain.CurrentDomain.UnhandledException += HandleUnhandledException;
			App.Instance.HandleUncaughtException += HandleUnhandledException;
			TaskHandler.UnhandledExceptionEventHandler += HandleUnhandledException;
		}

		void HandleUnhandledException (Exception e)
		{
			if ((e as NotLoggedInException) != null) {
				InvokeOnMainThread (() => {
					Navigator.LoginScreen ();
				});
			} else if (e is NotConnectedException) {
				InvokeOnMainThread (() => {
					ShowErrorNotice(e.Message);
				});
			}
		}

		void HandleUnhandledException (object sender, UnhandledExceptionEventArgs e)
		{
			var exception = e.ExceptionObject;
		}

		public void ChangeView (BaseUIController toView, string animation, string direction)
		{
			if (isErrorNoticeVisible) {
				HideErrorNotice ();
			}
			if (currentViewController == toView) {
				return;
			}
			if (currentViewController is BaseUIController) {
				(currentViewController as BaseUIController).NotifyViewDisappearing ();
			}
			currentViewController.WillMoveToParentViewController (null);
			toView.WillMoveToParentViewController (this);
			AddChildViewController (toView);
			UICompletionHandler onTrasitionComplete = (finished) => {
				currentViewController.RemoveFromParentViewController ();
				currentViewController.DidMoveToParentViewController (null);
				if (toView is BaseUIController) {
					(toView as BaseUIController).NotifyViewAppeared ();
				}
				currentViewController = toView;
				currentViewController.DidMoveToParentViewController (this);
			};
//			toView.View.LayoutIfNeeded ();
			switch (animation) {
			case "slide":
				var frame = ContainerView.Frame;
				if (direction == "left")
					frame.X = frame.Width * 2;
				else
					frame.X = frame.Width * (-2);
				toView.View.Frame = frame;
				toView.View.ApplyBoldShadow ();
				Transition (currentViewController, toView, .3, UIViewAnimationOptions.CurveEaseOut | UIViewAnimationOptions.LayoutSubviews, () => {
					toView.View.Frame = ContainerView.Frame;
				}, onTrasitionComplete);
				break;
			case "fade":
				Transition (currentViewController, toView, 0.2, UIViewAnimationOptions.TransitionCrossDissolve | UIViewAnimationOptions.LayoutSubviews, () => {
				}, onTrasitionComplete);
				break;
			case "none":
				Transition (currentViewController, toView, .1, UIViewAnimationOptions.TransitionNone | UIViewAnimationOptions.LayoutSubviews, () => {

				}, onTrasitionComplete);
				break;
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			Header.OnBackButtonTapped += RequestLastView;
			Footer.Hidden = true;
			setDefaultView ();
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
		}
		public HTFooterView FooterMenu(){
			return Footer;
		}
		public void HaveShadow ()
		{
			Header.SetShadow ();
			Footer.SetShadow ();
		}

		public void NoShadow ()
		{
			Header.HideShadow ();
			Footer.HideShadow ();
		}

		public void NeedFooter ()
		{
			Footer.Show ();
			Footer.BrowseMode ();
		}

		public void RegisterActionFooterEvents (EventHandler onDelete, EventHandler onDone)
		{
			Footer.OnDeleteButtonClicked += onDelete;
			Footer.OnDoneButtonClicked += onDone;
		}

		public void UnRegisterActionFooterEvents (EventHandler onDelete, EventHandler onDone)
		{
			Footer.OnDeleteButtonClicked -= onDelete;
			Footer.OnDoneButtonClicked -= onDone;
		}

		public void NeedActionFooter ()
		{
			Footer.Show ();
			Footer.ActionMode ();

		}

		public void NoFooter ()
		{
			Footer.Hide ();
		}

		public void NeedBackButton ()
		{
			Header.ShowBackButton ();
		}

		public void NoBackButton ()
		{
			Header.HideBackButton ();
		}

		public void Alert (string message, string title = "Alert")
		{
			try {
				AlertNew (message, title);
			} catch {
				var alert = new UIAlertView (title, message, null, "Ok");
				alert.Show ();
			}

		}

		public void AlertNew (string message, string title)
		{
			var alert = UIAlertController.Create (title, message, UIAlertControllerStyle.Alert);
			alert.AddAction (UIAlertAction.Create ("Ok", UIAlertActionStyle.Cancel, null));
			PresentViewController (alert, true, null);
		}

		public void ShowConfirmAlert (string message, Action accepted, Action rejected)
		{
			try {
				confirmAlertNew (message, accepted, rejected);
			} catch {
				confirmAlertOld (message, accepted, rejected);
			}
		}

		private void confirmAlertOld (string message, Action accepted, Action rejected)
		{
			var alert = new UIAlertView ("Confirm", message, null, "Yes", "No");
			alert.Clicked += (object sender, UIButtonEventArgs e) => {
				if (e.ButtonIndex == alert.CancelButtonIndex) {
					if (accepted != null)
						accepted.Invoke ();
				} else {
					if (rejected != null)
						rejected.Invoke ();
				}
			};
			alert.Show ();
		}

		private void confirmAlertNew (string message, Action accepted, Action rejected)
		{
			var alert = UIAlertController.Create ("Confirm", message, UIAlertControllerStyle.Alert);
			alert.AddAction (UIAlertAction.Create ("Yes", UIAlertActionStyle.Default, ii => {
				if (accepted != null) {
					accepted.Invoke ();
				}
			}));
			alert.AddAction (UIAlertAction.Create ("No", UIAlertActionStyle.Default, ii => {
				if (rejected != null) {
					rejected.Invoke ();
				}
			}));
			PresentViewController (alert, true, null);
		}

		public void ShowActionSheet (Dictionary<string, Action> actions, string title)
		{
			try {
				actionSheetNew (actions, title);
			} catch {
				actionSheetOld (actions, title);
			}
		}

		private void actionSheetOld (Dictionary<string, Action> actions, string title)
		{
			var sheet = new UIActionSheet (title);
			List<Action> handlers = new List<Action> ();
			foreach (var action in actions) {
				sheet.AddButton (action.Key);
				handlers.Add (action.Value);
			}
			sheet.AddButton ("Cancel");
			sheet.Clicked += (object sender, UIButtonEventArgs e) => {
				
				if ((int)e.ButtonIndex < handlers.Count && handlers [(int)e.ButtonIndex] != null) {
					handlers [(int)e.ButtonIndex].Invoke ();
				}
			};
			sheet.ShowInView (Footer);
		}

		private void actionSheetNew (Dictionary<string, Action> actions, string title)
		{
			var sheet = UIAlertController.Create (title, null, UIAlertControllerStyle.ActionSheet);
			foreach (var action in actions) {
				sheet.AddAction (UIAlertAction.Create (action.Key, UIAlertActionStyle.Default, dd => action.Value.Invoke ()));
			}
			sheet.AddAction (UIAlertAction.Create ("Cancel", UIAlertActionStyle.Cancel, null));
			PresentViewController (sheet, true, null);
		}

		private UIView errorNoticeContainer;
		private UITextView errorNoticeText;
		private NSLayoutConstraint noticeTopEdgeConstraint;
		private void initErrorContainer(){
			errorNoticeText = new UITextView {
				BackgroundColor = UIColor.Clear,
				TextColor = UIColor.White,
				ContentInset = new UIEdgeInsets (0, 5, 5, 5),
				TextAlignment = UITextAlignment.Center,
				Selectable = false,
				Editable = false,
				ScrollEnabled = false,
				Font = UIFont.SystemFontOfSize(15),
				TranslatesAutoresizingMaskIntoConstraints = false,
			};
			errorNoticeContainer = new UIView {
				BackgroundColor = UIColor.FromRGBA(43,43,43, 200),
				TranslatesAutoresizingMaskIntoConstraints = false,
			};
			errorNoticeContainer.AddSubview (errorNoticeText);
			errorNoticeContainer.DefineLayout (new string[]{ "|-0@1000-[text]-0@1000-|","V:|-0@1000-[text(>=25@1000)]-0@1000-|" }, "text", errorNoticeText);
			errorNoticeContainer.AddConstraint (NSLayoutConstraint.Create (errorNoticeText, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null, NSLayoutAttribute.Width, 1, View.Bounds.Size.Width));
			View.InsertSubviewBelow (errorNoticeContainer, Header);
			var widthConstraint = NSLayoutConstraint.Create (errorNoticeContainer, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null, NSLayoutAttribute.Width, 1, View.Bounds.Width);
			widthConstraint.Priority = 1000;
			View.AddConstraint (widthConstraint);
			View.DefineLayout (new string[]{ "|-0@1000-[notice]-0@1000-|" }, "notice", errorNoticeContainer);
			noticeTopEdgeConstraint = NSLayoutConstraint.Create (errorNoticeContainer, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View, NSLayoutAttribute.Top, 1, 0);
			noticeTopEdgeConstraint.Constant = 0;
			View.AddConstraint (noticeTopEdgeConstraint);
			errorNoticeContainer.AddGestureRecognizer (new UITapGestureRecognizer (() => {
				HideErrorNotice();
			}));
		}
		public void ShowErrorNotice(string message){
			if (errorNoticeContainer == null) {
				initErrorContainer ();
			}
			errorNoticeText.Text = message;
			errorNoticeText.InvalidateIntrinsicContentSize ();
			errorNoticeContainer.LayoutIfNeeded ();
			if (!isErrorNoticeVisible) {
				makeErrorVisible ();
			}
		}
		private bool isErrorNoticeVisible;
		private void makeErrorVisible(){
			isErrorNoticeVisible = true;
			errorNoticeContainer.Hidden = false;
			UIView.Animate (0.7,0, UIViewAnimationOptions.CurveEaseOut, () => {
				noticeTopEdgeConstraint.Constant = 60;
				errorNoticeContainer.LayoutIfNeeded();
			}, () => {
			});
		}
		public void HideErrorNotice(){
			if (errorNoticeContainer == null) {
				return;
			}
			UIView.Animate (0.7,0, UIViewAnimationOptions.CurveEaseIn, () => {
				noticeTopEdgeConstraint.Constant = 0;
				errorNoticeContainer.LayoutIfNeeded();
			}, () => {
				errorNoticeContainer.Hidden = true;
				isErrorNoticeVisible = false;
			});
		}
		private UIView LoadingView;

		public void ShowLoading (bool disableFooter = false)
		{
			if (LoadingView != null) {
				return;
			}
			LoadingView = new UIView {
				BackgroundColor = UIColor.FromRGBA (255, 255, 255, 240),
			};
			var animatorView = new LoadingIndicator ();
			animatorView.StartAnimating ();
			LoadingView.AddSubview (animatorView);
			animatorView.TranslatesAutoresizingMaskIntoConstraints = false;
			LoadingView.TranslatesAutoresizingMaskIntoConstraints = false;
			LoadingView.AddConstraint (NSLayoutConstraint.Create (animatorView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, LoadingView, NSLayoutAttribute.CenterX, 1, 0));
			LoadingView.AddConstraint (NSLayoutConstraint.Create (animatorView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, LoadingView, NSLayoutAttribute.CenterY, 1, 0));
			LoadingView.Layer.Opacity = 0;
			if (disableFooter) {
				View.InsertSubviewAbove (LoadingView, Footer);
			} else {
				View.InsertSubviewBelow (LoadingView, Footer);
			}
			View.DefineLayout (new string[] {
				"|-0-[LoadingView]-0-|",
				"V:|-0-[LoadingView]-0-|"
			}, "LoadingView", LoadingView);
			UIView.Animate (0.3, () => {
				LoadingView.Layer.Opacity = 1;
			});
		}

		public void HideLoading ()
		{
			if (LoadingView != null) {
				UIView.Animate (0.3, () => {
					LoadingView.Layer.Opacity = 0;
				}, () => {
					if(LoadingView!=null)
					LoadingView.RemoveFromSuperview ();
					LoadingView = null;
				});
			}
		}

		private void setDefaultView ()
		{
			currentViewController = Storyboard.InstantiateViewController (RootViewID) as BaseUIController;
			AddChildViewController (currentViewController);
			currentViewController.DidMoveToParentViewController (this);
			ContainerView.AddSubview (currentViewController.View);
			currentViewController.NotifyViewAppeared ();
//			currentViewController.ViewDidLoad ();
		}

		private void animateHeader ()
		{
			var frame = Header.Frame;
			frame.Height = UIScreen.MainScreen.Bounds.Height;
			Header.Frame = frame;
			Header.LayoutSubviews ();
			UIView.Animate (0.3, 0, UIViewAnimationOptions.LayoutSubviews | UIViewAnimationOptions.CurveEaseInOut, () => {
				Header.LayoutIfNeeded ();
			}, () => {
			});
		}

		public void RequestLastView ()
		{
			Navigator.LastScreen ();
		}
	}
}
