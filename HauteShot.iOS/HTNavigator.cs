﻿using System;
using System.Collections.Generic;
using UIKit;
using HauteShot.Core;

namespace HauteShot.iOS
{
	public class HTNavigator
	{
		public NavHistory ViewHistory{ get; set; }

		private NavHistoryItem _view;

		protected NavHistoryItem CurrentView { 
			get { return _view; } 
			set { 
				if (_view != null && _view != value && _view.Returnable) {
					ViewHistory.Push (_view);
				}
				_view = value;
			} 
		}

		private static HTNavigator _instance;

		public static HTNavigator Instance {
			get {
				if (_instance == null) {
					_instance = new HTNavigator ();
				}
				return _instance;
			}
		}

		public MainNavController ViewsContainer;

		private HTNavigator ()
		{
			ViewHistory = new NavHistory ();
		}

		private BaseUIController getViewController (string storyboardId)
		{
			return ViewsContainer.Storyboard.InstantiateViewController (storyboardId) as BaseUIController;
		}

		private void navigate (string storyboardId)
		{
			navigate (storyboardId, storyboardId, null);
		}

		private void navigate (string historyId, string storyboardId, Action<BaseUIController> provision, string animationType = "slide")
		{
			if (CurrentView != null && CurrentView.ID == historyId) {
				return;
			}
			NavHistoryItem item;
			string animationDirection;
			if (ViewHistory.Exists (historyId)) {
				item = ViewHistory.Pull (historyId);
				animationDirection = "right";
			} else {
				item = new NavHistoryItem (historyId, storyboardId, getViewController (storyboardId));
				if (provision != null) {
					provision.Invoke (item.ViewInstance);
				}
				animationDirection = "left";
			}
			CurrentView = item;
			ViewsContainer.ChangeView (item.ViewInstance, animationType, animationDirection);
		}

		public bool LastScreen ()
		{
			var item = ViewHistory.Pop ();
			if (item != null) {
				_view = null;
				CurrentView = item;
				ViewsContainer.ChangeView (item.ViewInstance, "slide", "right");
				return true;
			}
			return false;
		}

		public void LoginScreen ()
		{
			ViewHistory = new NavHistory ();
			var viewId = "LoginView";
			navigate (viewId);
		}

		public void SingupScreen ()
		{
			var viewId = "SignupView";
			navigate (viewId);
		}
		public void ChangePPScreen()
		{
			var viewId = "PPChangeView";
			navigate (viewId);
		}
		public void ProfileEditScreen ()
		{
			var viewId = "ProfileEditScreen";
			navigate (viewId);
		}
		public void ChangePasswordScreen(){
			var viewId = "ChangePasswordScreen";
			navigate (viewId);
		}
		public void HomeFeedScreen ()
		{
			var viewId = "HomeFeedView";
			navigate (viewId);
		}

		public void UserProfileScreen (int id)
		{
			var viewId = "UserProfileView";
			navigate (String.Format ("{0}-{1}", viewId, id.ToString ()), viewId, vc => {
				var v = vc as UserProfileViewController;
				v.UserID = id;
			}, "slide");
		}

		public void PhotoScreen (int id)
		{
			var viewId = "PhotoDetailViewer";
			navigate (String.Format ("{0}-{1}", viewId, id.ToString ()), viewId, vc => {
				var v = vc as PhotoViewController;
				v.PhotoID = id;
			}, "slide");
		}

		public void BadgesListScreen (User user)
		{
			var viewId = "BadgesView";
			string userID;
			if (user == null) {
				userID = "0";
			} else {
				userID = user.ID.ToString();
			}
			navigate (String.Format ("{0}-{1}", viewId, userID), viewId, vc => {
				var v = vc as BadgesListController;
				if(user!=null)
					v.User = user;
			});

		}
		public void ContestListScreen(){
			var viewId = "ContestsView";
			navigate (viewId);
		}
		public void TryPickMedia ()
		{
			ViewsContainer.InvokeOnMainThread (() => {
				if (Editor.Instance.HasCamera ())
					ViewsContainer.PresentViewController (Editor.Instance.CameraPicker (), true, ()=>{});
				else
					ViewsContainer.PresentViewController (Editor.Instance.PhotoPicker (), true, ()=>{});
			});
		}

		public void EditorScreen ()
		{
			var viewId = "EditorView";
			var view = getViewController (viewId);
			CurrentView = new NavHistoryItem (viewId, viewId, view, false);
			ViewsContainer.ChangeView (view, "fade", "none");
		}

		public void PhotoInfoEditorScreen (int id, bool backButton = true)
		{
			var viewId = "PhotoInfoEditor";
			var view = getViewController (viewId) as PhotoInfoEditorController;
			view.PhotoID = id;
			view.NoBackButton = !backButton;
			CurrentView = new NavHistoryItem (viewId, viewId, view, false);
			ViewsContainer.ChangeView (view, "slide", "left");
		}
		public void ShowConfirmAlert(string message, Action accepted, Action rejected){
			ViewsContainer.ShowConfirmAlert (message, accepted, rejected);
		}
		public void Alert(string message, string title="Alert"){
			ViewsContainer.Alert (message, title);
		}
		public void ActionSheet(Dictionary<string, Action> actions, string title="Pick An Option"){
			ViewsContainer.ShowActionSheet (actions, title);
		}
		public void ShowModal(UIViewController modalController){
			ViewsContainer.PresentViewController (modalController, true, null);
		}
		public void SettingsScreen ()
		{
			var viewId = "SettingsView";
			navigate (viewId);
		}
		public void LoadWebContent(String name){
			var viewId = "AboutScreen";
			var historyId = viewId + "-"+name;
			navigate (historyId, viewId, c => {
				var controller = c as AboutScreenController;
				if(controller!=null){
					controller.PageName = name+".html";
				}
			});
		}
		public void InfoScreen ()
		{
			LoadWebContent ("about");
		}
		public void PrivacyScreen(){
			LoadWebContent ("privacy");
		}
		public void TermsScreen(){
			LoadWebContent ("terms");
		}
	}

	public class NavHistory
	{
		protected List<NavHistoryItem> history;
		protected int MaxItem;

		public NavHistory ()
		{
			history = new List<NavHistoryItem> ();
			MaxItem = 10;
		}

		public void Trim ()
		{
			if (history.Count > MaxItem) {
				var excess = history.Count - MaxItem;
				var olds = history.GetRange (0, excess);
				history.RemoveRange (0, excess);
				foreach (NavHistoryItem item in olds) {
					item.ViewInstance.Dispose ();
				}
			}
		}

		public NavHistoryItem Push (string id, string storyboardId, BaseUIController viewInstance)
		{
			var item = new NavHistoryItem {
				ID = id,
				StoryBoardID = storyboardId,
				ViewInstance = viewInstance,
			};
			return Push (item);
		}

		public NavHistoryItem Push (NavHistoryItem item)
		{
			history.Add (item);
			return item;
		}

		public NavHistoryItem Pop ()
		{
			if (history.Count < 1) {
				return null;
			}
			var item = history [history.Count - 1];
			if (item != null) {
				history.Remove (item);
			}
			return item;
		}

		public bool Exists (string id)
		{
			return history.Exists (item => {
				return item.ID == id;
			});
		}

		public NavHistoryItem Pull (string id)
		{
			var item = history.Find (i => {
				return i.ID == id;
			});
			if (item != null) {
				history.Remove (item);
			}
			return item;
		}

	}

	public class NavHistoryItem
	{
		public string ID{ get; set; }

		public string StoryBoardID{ get; set; }

		public bool Returnable{ get; set; }

		public BaseUIController ViewInstance{ get; set; }

		public NavHistoryItem ()
		{

		}

		public NavHistoryItem (string id, string storyBoardId, BaseUIController viewInstance, bool returnable = true)
		{
			ID = id;
			StoryBoardID = storyBoardId;
			ViewInstance = viewInstance;
			Returnable = returnable;
		}
	}
}

