﻿using System;
using UIKit;
using System.Collections.Generic;
using HauteShot.Core;

namespace HauteShot.iOS
{
	public class SettingsTableSource:UITableViewSource
	{
		public Dictionary<string, List<SettingsMenuItem>> MenuItmes;
		public string[] Sections;

		public SettingsTableSource ()
		{
			Sections = new string[] {
				"Account",
				"About"
			};
			MenuItmes = new Dictionary<string, List<SettingsMenuItem>> ();
			MenuItmes ["Account"] = new List<SettingsMenuItem> () {
				new SettingsMenuItem {
					Icon = "Icons/user.png",
					Label = "Edit Profile",
					OnSelected = () => {
						HTNavigator.Instance.ProfileEditScreen ();
					}
				},
				new SettingsMenuItem {
					Icon = "Icons/pass.png",
					Label = "Change Password",
					OnSelected = () => {
						HTNavigator.Instance.ChangePasswordScreen ();
					}
				},
				new SettingsMenuItem {
					Icon = "Icons/cog.png",
					Label = "Logout",
					OnSelected = () => {
						HTNavigator.Instance.ShowConfirmAlert ("Do you really want to logout?", () => {
							Server.Instance.Logout ();
							HTNavigator.Instance.LoginScreen ();
						}, () => {

						});
					}
				}
			};
			MenuItmes ["About"] = new List<SettingsMenuItem> () {
				new SettingsMenuItem {
					Icon = "info.png",
					Label = "Privacy Policy",
					OnSelected = ()=>{
						HTNavigator.Instance.PrivacyScreen();
					}
				},
				new SettingsMenuItem {
					Icon = "info.png",
					Label = "About HauteShot",
					OnSelected = ()=>{
						HTNavigator.Instance.InfoScreen();
					}
				},
			};
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return Sections.Length;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			var sectionName = Sections [section];
			return MenuItmes [sectionName].Count;
		}

		public override string TitleForHeader (UITableView tableView, nint section)
		{
			return Sections [section];
		}

		public override void WillDisplayHeaderView (UITableView tableView, UIView hview, nint section)
		{
			var headerView = hview as UITableViewHeaderFooterView;
			if (headerView == null) {
				return;
			}
			headerView.TextLabel.TextColor = UIColor.FromRGB (43, 43, 43);
			headerView.TextLabel.Font = UIFont.SystemFontOfSize (13);
		}

		public override UITableViewCell GetCell (UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			var menuItem = getMenuItem (indexPath);
			var cell = tableView.DequeueReusableCell ("settingsCell");
			cell.ImageView.Image = UIImage.FromBundle (menuItem.Icon);
			cell.TextLabel.Text = menuItem.Label;
			cell.TextLabel.TextColor = UIColor.FromRGB (43, 43, 43);
			if (menuItem.DetailsText != null) {
				cell.DetailTextLabel.Text = menuItem.DetailsText;
			}
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			return cell;
		}

		SettingsMenuItem getMenuItem (Foundation.NSIndexPath indexPath)
		{
			var sectionName = Sections [indexPath.Section];
			var menuItem = MenuItmes [sectionName] [indexPath.Row];
			return menuItem;
		}

		public override void RowSelected (UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			var menuItem = getMenuItem (indexPath);
			if (menuItem.OnSelected != null) {
				menuItem.OnSelected.Invoke ();
			}
			tableView.ReloadRows (new Foundation.NSIndexPath[]{ indexPath }, UITableViewRowAnimation.Automatic);
		}
	}

	public class SettingsMenuItem
	{
		public string Icon{ get; set; }

		public string Label{ get; set; }

		public string DetailsText{ get; set; }

		public Action OnSelected{ get; set; }
	}
}

