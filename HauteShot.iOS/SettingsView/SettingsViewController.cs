using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;

namespace HauteShot.iOS
{
	partial class SettingsViewController : BaseUIController
	{
		public SettingsViewController (IntPtr handle) : base (handle)
		{
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			SettingsTable.Source = new SettingsTableSource ();
		}
		public override void NotifyViewAppeared ()
		{
			base.NotifyViewAppeared ();
			NavController.NeedFooter ();
			NavController.HaveShadow ();
			NavController.NeedBackButton();
			NavController.FooterMenu ().TabSettings ();
		}
	}
}
