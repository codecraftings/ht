using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

using HauteShot.Core;

namespace HauteShot.iOS
{
	public class Application
	{
		// This is the main entry point of the application.
		static void Main (string[] args)
		{
			// if you want to use a different Application Delegate class from "AppDelegate"
			// you can specify it here.
			App.Instance.OnSetupIOS ();
			ServiceContainer.Register<IWebService> (() => new iOSWebService ());
			ServiceContainer.Register<ISettings> (() => new Settings ());
			ServiceContainer.Register<IFileSystem> (() => new FileSystem ());
			ServiceContainer.Register<ITaskHandler> (() => new TaskHandler ());
			UIApplication.Main (args, null, "AppDelegate");
		}

		public static void HandleException (Exception e)
		{
			if (e is NotLoggedInException) {
				Console.WriteLine (e.Message);
			}
		}
	}
}
