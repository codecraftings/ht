﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;

namespace HauteShot.iOS
{
	[Register("CircularImageView"), DesignTimeVisible(true)]
	public class CircularImage:UIImageView
	{
		public CircularImage (IntPtr p):base(p)
		{
		}
		public CircularImage():base()
		{
			ClipsToBounds = true;
		}
		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			ClipsToBounds = true;
		}
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			Frame = new CoreGraphics.CGRect (Frame.X, Frame.Y, Bounds.Width, Bounds.Width);
			Layer.CornerRadius = Bounds.Width / 2;
		}
	}
}

