﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;

namespace HauteShot.iOS
{
	[Register ("HoverButton"), DesignTimeVisible (true)]
	public class HoverableButton:UIButton
	{
		public UIColor HoverBGColor;
		public UIColor NormalBGColor;
		public UIColor NormalTintColor;
		public UIColor HoverTintColor;
//		public new UIButtonType ButtonType{
//			get{
//				return UIButtonType.Custom;
//			}
//		}
		public HoverableButton (IntPtr p) : base (p)
		{
			registerEvents ();
		}

		public HoverableButton (string imageSrc, string hoverImage, float padding = 5):base(UIButtonType.Custom)
		{
			Initialize (imageSrc, padding, hoverImage, 0);
		}

		public HoverableButton (string imageSrc, float padding = 5):base(UIButtonType.Custom)
		{
			Initialize (imageSrc, padding, null, 0);
		}

		public HoverableButton (string imageSrc, UIColor normalColor, UIColor HoverColor):base(UIButtonType.Custom)
		{
			Initialize (imageSrc, 0, null, UIImageRenderingMode.AlwaysTemplate);
			NormalTintColor = normalColor;
			HoverTintColor = HoverColor;
			TintColor = NormalTintColor;
		}
		public HoverableButton (string imageSrc, UIColor normalColor, UIColor HoverColor, float padding):base(UIButtonType.Custom)
		{
			Initialize (imageSrc, padding, null, UIImageRenderingMode.AlwaysTemplate);
			NormalTintColor = normalColor;
			HoverTintColor = HoverColor;
			TintColor = NormalTintColor;
		}
		void Initialize (string imageSrc, float padding, string hoverImageSrc, UIImageRenderingMode iconRenderMode)
		{
			if (iconRenderMode == 0) {
				iconRenderMode = UIImageRenderingMode.AlwaysOriginal;
			}
			var normalImage = UIImage.FromBundle (imageSrc).ImageWithRenderingMode (iconRenderMode);
			this.SetImage (normalImage, UIControlState.Normal);
			if (hoverImageSrc != null) {
				var hoveredImage = UIImage.FromBundle (hoverImageSrc).ImageWithRenderingMode (iconRenderMode);
				SetImage (hoveredImage, UIControlState.Highlighted);
				SetImage (hoveredImage, UIControlState.Disabled);
				SetImage (hoveredImage, UIControlState.Selected);
			}
			ContentEdgeInsets = new UIEdgeInsets (padding, padding, padding, padding);
			HoverBGColor = UIColor.FromRGBA (33, 33, 33, 200);
			BackgroundColor = UIColor.Clear;
			ContentMode = UIViewContentMode.ScaleAspectFit;
			this.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
			registerEvents ();
		}
		public void SetActive(){
			Selected = true;
			BackgroundColor = HoverBGColor;
			if (HoverTintColor != null) {
				TintColor = HoverTintColor;
			}
		}
		public void SetNormal(){
			BackgroundColor = UIColor.Clear;
			Selected = false;
			if (NormalTintColor != null) {
				TintColor = NormalTintColor;
			}
		}
		void registerEvents ()
		{
			TouchDown += HandleTouchDown;
			TouchUpInside += HandleTouchUpInside;
			TouchUpOutside += HandleTouchUpOutside;
			TouchCancel += HandleTouchUpOutside;
		}

		void HandleTouchUpOutside (object sender, EventArgs e)
		{
			resetState ();
		}

		void HandleTouchUpInside (object sender, EventArgs e)
		{
			resetState (0.5);
		}

		void resetState (double animTime = 0.3)
		{
			if (NormalBGColor != null) {
				Animate (animTime, () => {
					this.SetTitleColor (HoverBGColor, UIControlState.Normal);
					BackgroundColor = NormalBGColor;
					if(NormalTintColor!=null){
						TintColor = NormalTintColor;
					}
				}, () => {
					NormalBGColor = null;
				});
			}
		}

		void HandleTouchDown (object sender, EventArgs e)
		{
			if (HoverBGColor != null && NormalBGColor == null) {
				NormalBGColor = BackgroundColor;
				SetTitleColor (BackgroundColor, UIControlState.Normal);
				Animate (0.2, () => {
					BackgroundColor = HoverBGColor;
					if(HoverTintColor!=null){
						TintColor = HoverTintColor;
					}
				}, () => {
				});
			}
		}
	}
}

