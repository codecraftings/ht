﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;
using System.Drawing;
using CoreGraphics;

namespace HauteShot.iOS
{
	[Register ("FooterView"), DesignTimeVisible (true)]
	public class HTFooterView:UIView
	{
		public HoverableButton HomeButton;
		public HoverableButton SettingsButton;
		public HoverableButton CameraButton;
		public HoverableButton RankButton;
		public HoverableButton InfoButton;
		public HoverableButton DeleteButton;

		public event EventHandler OnDeleteButtonClicked;

		public HoverableButton DoneButton;

		public event EventHandler OnDoneButtonClicked;

		public BaseUIController Controller;

		public HTNavigator Navigator {
			get {
				return HTNavigator.Instance;
			}
		}

		public HTFooterView (IntPtr p) : base (p)
		{
		}

		public override void AwakeFromNib ()
		{
			//base.AwakeFromNib ();
			this.Initialize ();
		}

		private void Initialize ()
		{
			//this.BackgroundColor = UIColor.Blue;
//			var normal1 = UIColor.FromRGB(149, 173, 194);
//			var normal1 = UIColor.FromRGB(203, 203, 203);
			var normal1 = UIColor.FromRGB(227, 227, 227);
			var hover1 = UIColor.White;
			var normal2 = UIColor.FromRGB(242, 136, 119);
			var hover2 = UIColor.White;
			var padding = 7;
			HomeButton = new HoverableButton ("home2.png", normal1, hover1, padding);
			HomeButton.TouchUpInside += (s, o) => {
				TabHome();
				Navigator.HomeFeedScreen ();
			};
			SettingsButton = new HoverableButton ("settings2.png", normal1, hover1, padding);
			SettingsButton.TouchUpInside += (s, e) => {
				TabSettings();
				Navigator.SettingsScreen ();
			};
//			CameraButton = new HoverableButton ("camera2.png", normal2, hover2, 3);
//			CameraButton = new HoverableButton ("camera.png");
			CameraButton = new HoverableButton ("camera-g.png");
			CameraButton.TouchUpInside += (sender, e) => Navigator.TryPickMedia ();

			RankButton = new HoverableButton ("promote-btn.png", normal1, hover1, padding);
			RankButton.TouchUpInside += (s, o) => {
				TabRank();
				Navigator.ContestListScreen();
			};
			InfoButton = new HoverableButton ("info3.png", normal1, hover1, padding);
			InfoButton.TouchUpInside += (object sender, EventArgs e) => {
				TabInfo();
				Navigator.InfoScreen();
			};
			DeleteButton = new HoverableButton ("Icons/delete.png");
			DeleteButton.TouchUpInside += HandleDeleteButtonClicked;

			DoneButton = new HoverableButton ("Icons/tick.png");
			DoneButton.TouchUpInside += HandleDoneButtonClicked;

//			this.AddSubviews (new UIView[]{ HomeButton, SettingsButton, CameraButton, RankButton, InfoButton });
			SetShadow ();
			BackgroundColor = UIColor.FromRGB (72, 66, 66);
			this.notVisible = true;
		}
		private void unsetAll(){
			HomeButton.SetNormal ();
			SettingsButton.SetNormal ();
			RankButton.SetNormal ();
			InfoButton.SetNormal ();
		}
		public void TabHome(){
			unsetAll ();
			HomeButton.SetActive ();
		}
		public void TabSettings(){
			unsetAll ();
			SettingsButton.SetActive ();
		}
		public void TabRank(){
			unsetAll ();
			RankButton.SetActive ();
		}
		public void TabInfo(){
			unsetAll ();
			InfoButton.SetActive ();
		}
		const string ToolbarModeAction = "action";
		const string ToolbarModeBrowse = "browse";
		private string _currentToolbarMode;

		public void BrowseMode ()
		{
			changeToolbarMode (ToolbarModeBrowse);
		}

		public void ActionMode ()
		{
			changeToolbarMode (ToolbarModeAction);
		}

		private void changeToolbarMode (string mode)
		{
			if (mode == _currentToolbarMode) {
				return;
			}
			switch (mode) {
			case ToolbarModeAction:
				setActionToolbarMode ();
				break;
			case ToolbarModeBrowse:
				setBrowseToolbarMode ();
				break;
			}
		}

		private void setBrowseToolbarMode ()
		{
			_currentToolbarMode = ToolbarModeBrowse;
			removeButtons (Subviews);
			showButtons (new UIView[]{ HomeButton, SettingsButton, CameraButton, RankButton, InfoButton });
		}

		private void setActionToolbarMode ()
		{
			_currentToolbarMode = ToolbarModeAction;
			removeButtons (Subviews);
			showButtons (new UIView[]{ DeleteButton, null, null, null, DoneButton });
		}

		private void showButtons (UIView[] views)
		{
			nfloat sW = Bounds.Width / 5;
			nfloat sH = Bounds.Height;
			for (int i = 0; i < views.Length; i++) {
				var view = views [i];
				if (view is UIButton) {
					AddSubview (view);
					var frame = new CGRect (0, 0, sW, sH);
					view.Frame = frame;
					Animate (0.3, () => {
						frame.X = sW * i;
						view.Frame = frame;
						view.Layer.Opacity = 1f;
					}, () => {
					});
				}
			}
		}

		private void removeButtons (UIView[] views)
		{
			foreach (var view in views) {
				if (view is UIButton) {
					var frame = view.Frame;
					Animate (0.2, () => {
						frame.X += 100;
						view.Frame = frame;
						view.Layer.Opacity = 0.3f;
					}, () => {
						view.RemoveFromSuperview ();
					});
				}
			}
		}

		void HandleDoneButtonClicked (object sender, EventArgs e)
		{
			if (OnDoneButtonClicked != null) {
				OnDoneButtonClicked.Invoke (this, new EventArgs ());
			}
		}

		void HandleDeleteButtonClicked (object sender, EventArgs e)
		{
			if (OnDeleteButtonClicked != null) {
				OnDeleteButtonClicked.Invoke (this, new EventArgs ());
			}
		}

		public void SetShadow ()
		{
			this.Layer.ShadowPath = CGPath.FromRect (this.Layer.Bounds);
			this.Layer.ShadowColor = UIColor.Black.CGColor;
			this.Layer.ShadowOpacity = .5f;
			this.Layer.ShadowOffset = new SizeF (.7f, .7f);
		}

		public void HideShadow ()
		{
			this.Layer.ShadowOpacity = 0;
		}
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
//			nfloat sW = Bounds.Width / 5;
//			nfloat sH = Bounds.Height;
//			HomeButton.Frame = new CGRect (0, 0, sW, sH);
//			SettingsButton.Frame = new CGRect (sW, 0, sW, sH);
//			CameraButton.Frame = new CGRect (2 * sW, 0, sW, sH);
//			RankButton.Frame = new CGRect (3 * sW, 0, sW, sH);
//			InfoButton.Frame = new CGRect (4 * sW, 0, sW, sH);
		}
		private bool notVisible;
		public void Show ()
		{
			if (!this.notVisible) {
				return;
			}
			this.notVisible = false;
			CGRect frame = this.Frame;
			frame.Y += 100;
			this.Frame = frame;
			this.Hidden = false;
			UIView.Animate (0.5, 0, UIViewAnimationOptions.CurveEaseIn, () => {
				frame.Y -= 100;
				this.Frame = frame;
			}, null);
		}

		public void Hide ()
		{
			if (this.notVisible) {
				return;
			}
			this.notVisible = true;
			CGRect frame = this.Frame;
			UIView.Animate (.5, 0, UIViewAnimationOptions.CurveEaseOut, () => {
				frame.Y += 100;
				this.Frame = frame;
			}, () => {
				this.Hidden = this.notVisible;
				frame.Y -= 100;
				this.Frame = frame;
			});
		}
	}
}

