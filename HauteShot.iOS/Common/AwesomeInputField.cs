﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;

namespace HauteShot.iOS
{
	[Register ("AwesomeInputField"), DesignTimeVisible (true)]
	public class AwesomeInputField:UIView
	{
		//		[Export ("LabelText"), Browsable (true)]
		public string LabelText{ get; set; }

		//		[Export ("LabelIcon"), Browsable (true)]
		public UIImage LabelIcon{ get; set; }

		//		[Export ("PlaceholderText"), Browsable (true)]
		public string PlaceholderText{ get; set; }

		public UILabel LabelTextView;
		public UIImageView LabelIconView;
		public UITextField AweInput;

		public string Value {
			get {
				return AweInput.Text;
			}
			set {
				AweInput.Text = value;
			}
		}

		public AwesomeInputField NextFormElement{ get; set; }

		public Action OnDone{ get; set; }

		public AwesomeInputField () : base ()
		{
			initialize ();
		}

		public AwesomeInputField (IntPtr p) : base (p)
		{
		}

		public AwesomeInputField (string labelText, string labelIcon, string placeholderText) : base ()
		{
			LabelText = labelText;
			LabelIcon = UIImage.FromBundle (labelIcon);
			PlaceholderText = placeholderText;
			initialize ();
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			initialize ();
		}

		public NSLayoutConstraint WidthConstraint;

		public NSLayoutConstraint HeightConstraint;

		public override bool CanBecomeFirstResponder {
			get {
				return false;
			}
		}

		public override UIView InputView {
			get {
				return null;
			}
		}

		public override UIView InputAccessoryView {
			get {
				return null;
			}
		}

		private void initialize ()
		{
			BackgroundColor = UIColor.White;
			Layer.BorderWidth = 1;
			Layer.BorderColor = UIColor.FromRGB (68, 181, 158).CGColor;
			Layer.CornerRadius = 3;
			Tag = 122;
			LabelTextView = new UILabel {
//				BackgroundColor = UIColor.Red,
				Font = UIFont.SystemFontOfSize (15),
				TextColor = UIColor.FromRGB (64, 64, 64)
			};
			LabelIconView = new UIImageView {
				ClipsToBounds = true,
				ContentMode = UIViewContentMode.ScaleAspectFit
			};
			AweInput = new UITextField {
				Font = UIFont.SystemFontOfSize (15),
				TextColor = UIColor.FromRGB (55, 55, 55),
				TranslatesAutoresizingMaskIntoConstraints = false,
				AutocapitalizationType = UITextAutocapitalizationType.None,
				AutocorrectionType = UITextAutocorrectionType.No
			};
			AweInput.ShouldReturn = HandleShouldReturn;
			AweInput.ValueChanged += HandleInputValueChanged;
			var dummyView = new UIView {
//				Hidden = true,
//				BackgroundColor = UIColor.Yellow
			};
			AddSubview (dummyView);
			AddSubviews (LabelTextView, LabelIconView, AweInput);
			WidthConstraint = NSLayoutConstraint.Create (this, NSLayoutAttribute.Width, NSLayoutRelation.GreaterThanOrEqual, null, NSLayoutAttribute.Width, 1, 150);
			WidthConstraint.Priority = 400;
			this.AddConstraint (WidthConstraint);
			HeightConstraint = NSLayoutConstraint.Create (this, NSLayoutAttribute.Height, NSLayoutRelation.GreaterThanOrEqual, null, NSLayoutAttribute.Height, 1, 42);
			HeightConstraint.Priority = 400;
			this.AddConstraint (HeightConstraint);
			this.DefineLayout (new string[] {
				"|-6@1000-[Icon(28@1000)]-5@1000-[Label(<=100@200)]-5@1000-[Input(Dummy@1000)]-4@800-|",
				"[Label]-5@1000-[Dummy(>=100@800)]-4@1000-|",
				"V:|-4@300-[Icon(28@1000)]-4@300-|",
				"V:|-4@300-[Label(Icon@1000)]-4@300-|",
				"V:|-4@300-[Input(Icon@1000)]-4@300-|",
				"V:|-4@300-[Dummy(Icon@1000)]-4@300-|"
			}, "Label", LabelTextView, "Icon", LabelIconView, "Input", AweInput, "Dummy", dummyView);
			var labelPositionConstrain = NSLayoutConstraint.Create (LabelTextView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterY, 1, 0);
			labelPositionConstrain.Priority = 1000;
			var iconPositionConstrain = NSLayoutConstraint.Create (LabelIconView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterY, 1, 0);
			iconPositionConstrain.Priority = 1000;
			var inputPositionConstrain = NSLayoutConstraint.Create (AweInput, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterY, 1, 0);
			inputPositionConstrain.Priority = 1000;
			AddConstraints (new NSLayoutConstraint[]{ labelPositionConstrain, iconPositionConstrain, inputPositionConstrain });
			UpdateApperance ();
			this.AddGestureRecognizer (new UITapGestureRecognizer (this.OnTouchUp));
		}

		void HandleInputValueChanged (object sender, EventArgs e)
		{
//			TextInputView.InvalidateIntrinsicContentSize ();
//			TextInputView.SizeToFit ();
		}

		public bool HandleShouldReturn (UITextField textfield)
		{
			AweInput.ResignFirstResponder ();
			if (NextFormElement != null) {
				NextFormElement.CatchFocus ();
			}
			if (OnDone != null) {
				OnDone.Invoke ();
			}
			return false;
		}

		public void CatchFocus ()
		{
			if (!AweInput.IsFirstResponder)
				AweInput.BecomeFirstResponder ();
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);

		}

		public void OnTouchUp (UITapGestureRecognizer tap)
		{
			tap.CancelsTouchesInView = true;
			if (!AweInput.IsFirstResponder)
				AweInput.BecomeFirstResponder ();
		}

		public void Update (string label, string icon, Action onDone)
		{
			Update (label, icon, null, null, onDone);
		}

		public void Update (string label, string icon, Action onDone, string placeholder)
		{
			Update (label, icon, null, placeholder, onDone);
		}

		public void Update (string label, string icon, AwesomeInputField nextElement)
		{
			Update (label, icon, nextElement, null, null);
		}

		public void Update (string label, string icon, AwesomeInputField nextElement, string placeholder)
		{
			Update (label, icon, nextElement, placeholder, null);
		}

		public void Update (string label, string icon, AwesomeInputField nextElement, string placeholder, Action onDone)
		{
			LabelText = label;
			LabelIcon = UIImage.FromBundle (icon);
			NextFormElement = nextElement;
			PlaceholderText = placeholder;
			OnDone = onDone;
			UpdateApperance ();
		}

		public void SecureInput ()
		{
			AweInput.SecureTextEntry = true;
		}

		public void UpdateApperance ()
		{
			if (LabelText != null) {
				LabelTextView.Text = LabelText;
				LabelTextView.InvalidateIntrinsicContentSize ();
				LabelTextView.SizeToFit ();
//				TextInputView.InvalidateIntrinsicContentSize ();
			}
			if (LabelIcon != null) {
				LabelIconView.Image = LabelIcon;
			}
			if (PlaceholderText != null) {
				AweInput.Placeholder = PlaceholderText;
			}
			if (NextFormElement == null) {
				AweInput.ReturnKeyType = UIReturnKeyType.Done;
			} else {
				AweInput.ReturnKeyType = UIReturnKeyType.Next;
			}
			LayoutIfNeeded ();
			LayoutSubviews ();
		}
	}
}

