﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;
using HauteShot.Core;

namespace HauteShot.iOS
{
	[Register("PromoteButton"), DesignTimeVisible(true)]
	public class PromoteButtonView:UIView
	{
		public int PromoteCount {
			get;
			set;
		}
		public HoverableButton Button;
		public CounterView Counter;
		public Photo PhotoData{get; set;}
		private PhotoPromoteManager promoteManager;
		public PromoteButtonView (IntPtr p):base(p)
		{

		}
		public PromoteButtonView(){
			Initialize ();
		}
		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			Initialize ();
		}
		public void Initialize(){
			promoteManager = new PhotoPromoteManager ();
			Button = new HoverableButton ("promote-btn.png","promote-btn-hover.png", 10);
			Button.HoverBGColor = UIColor.FromRGB (255, 223, 128);
			Button.TouchUpInside += HandleButtonTapped;
			Counter = new CounterView ();
//			CounterValue.BackgroundColor = UIColor.Green;
			Counter.Value = 1;
			this.AddSubviews (new UIView[]{Button, Counter});
			this.DefineLayout (new string[] {
				"|-0-[CounterValue]-5-[Button]-0-|",
				"V:|[Button]|"
			},"CounterValue", Counter, "Button", Button);
			this.AddConstraint (NSLayoutConstraint.Create (Counter, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterY, 1, 0));
			this.AddConstraint (NSLayoutConstraint.Create (Counter, NSLayoutAttribute.Width, NSLayoutRelation.Equal, this, NSLayoutAttribute.Width, 0.35f, 0));
//			this.AddConstraint (NSLayoutConstraint.Create (Button, NSLayoutAttribute.Width, NSLayoutRelation.Equal, this, NSLayoutAttribute.Width, 0.60f, 0));
			this.AddConstraint (NSLayoutConstraint.Create (Button, NSLayoutAttribute.Width, NSLayoutRelation.LessThanOrEqual, null, NSLayoutAttribute.Width, 1, 48f));
//			SetPromoted ();
//			UnSetPromoted ();
			Button.TouchUpInside += (object sender, EventArgs e) => {
				Console.WriteLine(Button.Selected);
			};
		}

		void HandleButtonTapped (object sender, EventArgs e)
		{
			if (PhotoData.is_promoted) {
				promoteManager.UnPromotePhoto (PhotoData);
			} else {
				promoteManager.PromotePhoto (PhotoData);
			}
			UpdateView ();
		}
		public void UpdateView(){
			if (PhotoData.is_promoted) {
				SetPromoted ();
			} else {
				UnSetPromoted ();
			}
			Counter.Value = PhotoData.promotes_count;
		}
		public void SetDisabled(){
			Button.Enabled = false;
		}
		public void SetEnabled(){
			Button.Enabled = true;
		}
		public void SetPromoted(){
			Button.SetActive ();
		}
		public void UnSetPromoted(){
			Button.SetNormal ();
		}

	}
}

