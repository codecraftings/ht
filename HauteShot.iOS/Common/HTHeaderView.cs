﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;
using System.Drawing;
using CoreGraphics;

namespace HauteShot.iOS
{
	[Register ("HeaderView"), DesignTimeVisible (true)]
	public class HTHeaderView:UIView
	{
		UIImageView Logo;
		public HoverableButton BackButton;
		public Action OnBackButtonTapped;
		public HTHeaderView (IntPtr p) : base (p)
		{
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			Initialize ();
		}

		private void Initialize ()
		{
			Logo = new UIImageView {
				Image = UIImage.FromBundle ("logo.png"),
				BackgroundColor = UIColor.Clear,
				ContentMode = UIViewContentMode.ScaleAspectFit
			};
			BackButton = new HoverableButton ("back-btn.png", UIColor.White, UIColor.FromRGB(70,113,208));
			BackButton.TouchUpInside += HandleBackButtonTouched;
			AddSubview (BackButton);
			BackButton.Hidden = true;
			BackButton.ContentEdgeInsets = new UIEdgeInsets (5, 5, 5, 5);
			BackgroundColor = UIColor.FromRGB (72, 66, 66);
			BackButton.HoverBGColor = BackgroundColor;
			AddSubview (Logo);
		}

		void HandleBackButtonTouched (object sender, EventArgs e)
		{
//			BackButton.SetActive ();
			if (OnBackButtonTapped != null) {
				OnBackButtonTapped.Invoke ();
			}
		}
		public void ShowBackButton(){
			if (!BackButton.Hidden) {
				return;
			}
			BackButton.SetNormal ();
			var frame = BackButton.Frame;
			frame.X -= frame.Width;
			BackButton.Frame = frame;
			BackButton.Hidden = false;
			Animate (0.2, 0, UIViewAnimationOptions.CurveLinear, () => {
				frame.X += frame.Width;
				BackButton.Frame = frame;
			}, () => {
			});
		}
		public void HideBackButton(){
			if (BackButton.Hidden) {
				return;
			}
			var frame = BackButton.Frame;
			Animate (0.2, 0, UIViewAnimationOptions.CurveLinear, () => {
				frame.X -= frame.Width;
				BackButton.Frame = frame;
			}, () => {
				BackButton.Hidden = true;
				frame.X += frame.Width;
				BackButton.Frame = frame;
			});
		}
		public void SetShadow ()
		{
			this.Layer.ShadowPath = CGPath.FromRect (new CGRect (0, 1, Bounds.Width, Bounds.Height));
			this.Layer.ShadowColor = UIColor.Black.CGColor;
			this.Layer.ShadowOpacity = .5f;
			this.Layer.ShadowOffset = new SizeF (.4f, .4f);
		}
		public void HideShadow(){
			this.Layer.ShadowOpacity = 0;
		}
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			SetShadow ();
			var logoW = 70;
			var logoH = Bounds.Height - 33;
			Logo.Frame = new CGRect ((Bounds.Width - logoW) / 2, 25, logoW, logoH);
			BackButton.Frame = new CGRect (0, 20, 40, Bounds.Height-20);
		}

		public void Show ()
		{
			if (!this.Hidden) {
				return;
			}
			CGRect frame = this.Frame;
			frame.Y -= 100;
			this.Frame = frame;
			this.Hidden = false;
			UIView.Animate (0.5, 0, UIViewAnimationOptions.CurveEaseIn, () => {
				frame.Y += 100;
				this.Frame = frame;
			}, ()=>{
//				this.ShowBackButton();
			});
		}

		public void Hide ()
		{
			if (this.Hidden) {
				return;
			}
			var frame = this.Frame;
			frame.Y = 0;
			this.Frame = frame;
			UIView.Animate (0.5, 0, UIViewAnimationOptions.CurveEaseOut, () => {
				frame.Y -= 100;
				this.Frame = frame;
			}, () => {
				this.Hidden = true;
			});
		}
	}
}

