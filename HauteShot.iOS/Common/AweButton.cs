﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;

namespace HauteShot.iOS
{
	[Register ("AwesomeButton"), DesignTimeVisible (true)]
	public class AweButton:UIView
	{
		public UIImageView BackgroundImageContainer;
		public UIView ContentLayerContainer;
		public UIImageView IconContainer;
		public UILabel TextLabel;
		public AweButtonStyle NormalStyle;
		public AweButtonStyle HoverStyle;
		public AweButtonStyle ActiveStyle;
		public AweButton ()
		{
		}

		public AweButton (IntPtr p) : base (p)
		{

		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			Initialize ();
		}

		void Initialize ()
		{

		}
	}
	public class AweButtonStyle
	{
		public UIImage Icon{ get; set; }
		public UIImage BackgroundImage{ get; set; }
		public UIColor IconColor{get;set;}
		public UIColor BackgroundColor{ get; set;}
		public UIColor TextColor{ get; set; }
		public int CornerRadius{get;set;}
		public int IconCornerRadius{ get; set; }
		public UIEdgeInsets Padding{ get; set; }
	}
}

