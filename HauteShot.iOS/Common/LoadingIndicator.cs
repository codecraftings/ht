﻿using System;
using UIKit;

namespace HauteShot.iOS
{
	public class LoadingIndicator:UIImageView
	{
		private UIImage[] frames;
		public LoadingIndicator ()
		{
			frames = new UIImage[] {
				UIImage.FromBundle("loader/frame-001.png"),
				UIImage.FromBundle("loader/frame-002.png"),
				UIImage.FromBundle("loader/frame-003.png"),
				UIImage.FromBundle("loader/frame-004.png"),
				UIImage.FromBundle("loader/frame-005.png"),
				UIImage.FromBundle("loader/frame-006.png"),
				UIImage.FromBundle("loader/frame-007.png"),
				UIImage.FromBundle("loader/frame-008.png"),
				UIImage.FromBundle("loader/frame-009.png"),
				UIImage.FromBundle("loader/frame-010.png"),
				UIImage.FromBundle("loader/frame-011.png"),
				UIImage.FromBundle("loader/frame-012.png"),
			};
			AnimationImages = frames;
		}
	}
}

