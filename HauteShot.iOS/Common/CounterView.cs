﻿using System;
using UIKit;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using System.ComponentModel;

namespace HauteShot.iOS
{
	[Register("CounterView"), DesignTimeVisible(true)]
	public class CounterView:UIView
	{
		protected static UIColor BorderColor;
//		protected static UIColor BackgroundColor;
		protected static UIColor TextColor;
		protected static int SlotWidth = 19;
		protected static int SlotHeight = 18;
		protected static int SlotBorderWidth = 1;
		protected static int MAX_SLOT = 4;
		protected static int TextFontSize = 16;
		private int _value;

		public int Value {
			get {
				return _value;
			}
			set {
				if (value != _value) {
					_value = value;
				}
				updateCounter ();
			}
		}

		public char[] ValueDigits;
		private List<CounterSlotView> Slots;

		public override CGSize IntrinsicContentSize {
			get {
				return new CGSize ((SlotWidth + SlotBorderWidth) * Slots.Count, SlotHeight + 2 * SlotBorderWidth);
			}
		}

		public CounterView ()
		{
			BorderColor = UIColor.FromRGB (255, 223, 128);
			BackgroundColor = UIColor.White;
			TextColor = UIColor.FromRGB (255, 223, 128);
			ValueDigits = new char[MAX_SLOT];
			Slots = new List<CounterSlotView> ();
		}

		private void updateCounter ()
		{
			var newDigits = Value.ToString ().PadLeft(MAX_SLOT-1, '0').ToCharArray ();
			if (newDigits.Length > (MAX_SLOT-1)) {
				newDigits = getMaxValueChar ();
			}
			while (newDigits.Length > Slots.Count) {
				addNewSlot ();
			}
			ValueDigits = newDigits;
			updateSlotWithDigits ();
		}

		private void updateSlotWithDigits ()
		{
			Slots.Reverse ();
			for (int i = 0; i < ValueDigits.Length; i++) {
				Slots [i].SetValue (ValueDigits [i]);
				var x = (SlotWidth + SlotBorderWidth) * i;
				Slots [i].Frame = new CGRect (x, 0, SlotWidth + 2 * SlotBorderWidth, SlotHeight + 2 * SlotBorderWidth); 
			}
			Slots.Reverse ();
		}

		private void addNewSlot ()
		{
			var slot = new CounterSlotView ();
			slot.SetValue ('0');
			Slots.Add (slot);
			AddSubview (slot);
		}

		private char[] getMaxValueChar ()
		{
			char[] digits = new char[MAX_SLOT];
			for (int i = 0; i < (MAX_SLOT - 1); i++) {
				digits [i] = '9';
			}
			digits [MAX_SLOT - 1] = '+';
			return digits;
		}

		protected class CounterSlotView:UIView
		{
			char Value;
			int Position;
			UILabel primaryLabel;
			UILabel secondaryLabel;

			public CounterSlotView ()
			{
				initialize ();
			}

			public override CoreGraphics.CGSize IntrinsicContentSize {
				get {
					return new CGSize (SlotWidth, SlotHeight);
				}
			}

			public void initialize ()
			{
				BackgroundColor = BackgroundColor;
				Layer.BorderWidth = SlotBorderWidth;
				Layer.BorderColor = BorderColor.CGColor;
				ClipsToBounds = true;
				primaryLabel = new UILabel {
					Text = "0",
					Font = UIFont.SystemFontOfSize (TextFontSize),
					TextColor = TextColor,
					TextAlignment = UITextAlignment.Center,
					ClipsToBounds = true
				};
				secondaryLabel = new UILabel {
					Text = "0",
					Font = UIFont.SystemFontOfSize (TextFontSize),
					TextColor = TextColor,
					TextAlignment = UITextAlignment.Center,
					ClipsToBounds = true
				};
				this.AddSubviews (new UIView[]{ primaryLabel, secondaryLabel });
//				this.AddConstraint(NSLayoutConstraint.Create(primaryLabel, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterY, 1, 0));
//				this.AddConstraint(NSLayoutConstraint.Create(primaryLabel, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterX, 1, 0));
//				this.AddConstraint(NSLayoutConstraint.Create(secondaryLabel, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterY, 1, 0));
//				this.AddConstraint(NSLayoutConstraint.Create(secondaryLabel, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterX, 1, 0));
			}

			public void DesignAsFirstSlot ()
			{

			}

			public void DesignAsLastSlot ()
			{

			}

			public void SetValue (char newValue)
			{
				if (newValue == Value) {
					return;
				}
				Value = newValue;
				var w = SlotWidth + 2 * SlotBorderWidth;
				var h = SlotHeight + 2*SlotBorderWidth;
				primaryLabel.Frame = new CGRect (0, 0, w, h);
				secondaryLabel.Frame = new CGRect (0, h, w, h);
				secondaryLabel.Hidden = false;
				secondaryLabel.Text = newValue.ToString ();
				Animate (0.3, () => {
					primaryLabel.Frame = new CGRect (0, -h, w, h);
					secondaryLabel.Frame = new CGRect (0, 0, w, h);
				}, () => {
					primaryLabel.Text = newValue.ToString ();
					primaryLabel.Frame = new CGRect (0, 0, w, h);
					secondaryLabel.Hidden = true;
					primaryLabel.LayoutIfNeeded();
				});
			}
		}
	}

}

