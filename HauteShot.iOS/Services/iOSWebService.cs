﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using HauteShot.Core;
using System.Net.Cache;

namespace HauteShot.iOS
{
	public class iOSWebService:IWebService
	{
		public iOSWebService ()
		{

		}

		public IWebRequest CreateRequest (string url)
		{
			return new WebRequest (url);
		}

		public byte[] ReadImageBytes (string url)
		{
			var request = HttpWebRequest.Create (url);
			request.Method = "GET";
			HttpWebResponse response = request.GetResponse () as HttpWebResponse;
			using (Stream stream = response.GetResponseStream ()) {
				using (MemoryStream ms = new MemoryStream ()) {
					stream.CopyTo (ms);
					return ms.ToArray ();
				}
			}
		}
	}
}