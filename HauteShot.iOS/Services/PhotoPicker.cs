﻿using System;
using UIKit;
using CoreImage;
using CoreGraphics;
using HauteShot.Core;
using System.Threading.Tasks;

namespace HauteShot.iOS
{
	public class PhotoPicker
	{
		public UIImage PickedPhoto{ get; set; }

		public event EventHandler PhotoPickingCancelled;
		public event EventHandler PhotoPicked;

		private UIImagePickerController picker;

		public PhotoPicker ()
		{
			picker = new UIImagePickerController ();
			picker.FinishedPickingMedia += HandleFinishedPickingMedia;
			picker.Canceled += HandleCanceled;
		}

		void HandleCanceled (object sender, EventArgs e)
		{
			if (PhotoPickingCancelled != null) {
				PhotoPickingCancelled.Invoke (this, new EventArgs ());
			}
			Dismiss ();
		}

		void HandleFinishedPickingMedia (object sender, UIImagePickerMediaPickedEventArgs e)
		{
			PickedPhoto = e.OriginalImage;
			if (PhotoPicked != null) {
				PhotoPicked.Invoke (this, new EventArgs ());
			}
			Dismiss ();
		}

		public void FromCamera ()
		{
			picker.SourceType = UIImagePickerControllerSourceType.Camera;
			HTNavigator.Instance.ShowModal (picker);
		}

		public void FromGallery ()
		{
			picker.SourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum;
			HTNavigator.Instance.ShowModal (picker);
		}

		public void Dismiss ()
		{
			if (picker != null) {
				picker.DismissViewController (true, null);
//				picker = null;
			}
		}

		public Task<string> PrepareImageForPP (Action<string> onDone)
		{
			return Task.Run<string> (() => {
				string photoPath = "";
				var resized = PhotoPicker.resizeImage (PickedPhoto, 200, 0);
				photoPath = PhotoPicker.MakeAPath (resized);
				if (onDone != null)
					onDone.Invoke (photoPath);
				return photoPath;
			});
		}

		public static UIImage resizeImage (UIImage image, nfloat newW, nfloat newH)
		{
			var ciImage = CIImage.FromCGImage (image.CGImage);
			if (newH == 0)
				newH = newW * image.Size.Height / image.Size.Width;
			if (newW == 0)
				newW = newH * image.Size.Width / image.Size.Height;
			ciImage = ciImage.ImageByApplyingTransform (CGAffineTransform.MakeScale (newW / image.Size.Width, newH / image.Size.Height));
			var context = CIContext.FromOptions (null);
			return UIImage.FromImage (context.CreateCGImage (ciImage, ciImage.Extent));
		}

		public static string MakeAPath (UIImage image)
		{
			var filesystem = ServiceContainer.Resolve<IFileSystem> ();
			var path = filesystem.FromCacheFolder (System.Guid.NewGuid ().ToString () + ".jpg").GetPath ();
			var data = image.AsJPEG (70);
			data.Save (path, true);
			return path;
		}
	}
}

