﻿using System;
using System.Net;

namespace HauteShot.iOS
{
	public class ImageDownloader:IDisposable
	{
		public string URL{ get; set;}
		public event Action<int> OnProgress;
		public event Action<bool, byte[]> OnComplete;
		private WebClient client;
		public ImageDownloader (string url)
		{
			client = new WebClient ();
			client.DownloadDataCompleted += HandleDownloadDataCompleted;
			client.DownloadProgressChanged += HandleDownloadProgressChanged;
			URL = url;
		}

		void HandleDownloadProgressChanged (object sender, DownloadProgressChangedEventArgs e)
		{
			Console.WriteLine (DateTime.Now.Millisecond);
			Console.WriteLine (e.ProgressPercentage.ToString()+":"+URL);
			if (OnProgress != null) {
				OnProgress.Invoke (e.ProgressPercentage);
			}
		}

		void HandleDownloadDataCompleted (object sender, DownloadDataCompletedEventArgs e)
		{
//			Console.WriteLine (e.Result.Length/1000);
			if (OnComplete != null) {
				OnComplete.Invoke (!e.Cancelled, e.Result);
			}
		}
		public void startAsync(){
			client.DownloadDataAsync (new Uri(URL));
		}

		public void Dispose ()
		{
			client.Dispose ();
		}

	}
}

