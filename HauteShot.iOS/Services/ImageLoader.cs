﻿using System;
using UIKit;
using HauteShot.Core;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Foundation;
using System.Security.Cryptography;
using System.Threading.Tasks;


namespace HauteShot.iOS
{
	public class ImageLoader:NSObject
	{
		static readonly object _memory_cache_lock = new object ();
		static readonly object _file_cache_lock = new object ();
		static readonly object _down_track_lock = new object ();
		IWebService webService;
		private Dictionary<string, UIImage> OnMemory;
		private LinkedList<string> CachedImages;
		private static ImageLoader _instance;
		private List<string> ActiveDownload;
		private int memoryCacheLimit = 10;
		private int FileCacheLimit = 50;
		private IFileSystem fileSystem;

		public static ImageLoader Instance {
			get {
				if (_instance == null) {
					_instance = new ImageLoader ();
				}
				return _instance;
			}
		}

		private ImageLoader ()
		{
			webService = ServiceContainer.Resolve<IWebService> ();
			OnMemory = new Dictionary<string, UIImage> ();
			ActiveDownload = new List<string> ();
			fileSystem = ServiceContainer.Resolve<IFileSystem> ();
		}

		public void LoadIntoView (string url, UIImageView imgView, int TagID)
		{

		}

		public void LoadImage (string url, Action<UIImage> onload)
		{
			LoadImage (url, onload, null);
		}

		private void loadLocalImage (string url, Action<UIImage> onload)
		{
			NSData data;
			try {
				data = NSData.FromArray (File.ReadAllBytes (url.Substring (7, url.Length - 7)));
			} catch {
				return;
			}
			var image = UIImage.LoadFromData (data);
			onload (image);
		}

		public void LoadImage (string url, Action<UIImage> onload, Action<int> onProgress)
		{
			Console.WriteLine (new Uri (url).Scheme);
			if (new Uri (url).Scheme == "local") {
				loadLocalImage (url, onload);
				return;
			}
			UIImage image = null;
			//Console.WriteLine (url);
			Task.Run (() => {
				image = FromCache (url);
				//Console.WriteLine(url);
				if (image == null) {
					/**/
					FromServerAsync (url, (int num) => {
						if (onProgress != null) {
							onProgress.Invoke (num);
						}
					}, img => {
						InvokeOnMainThread (() => {
							onload (img);
						});
					});
					/**/
//					image = FromSever(url);
//					InvokeOnMainThread(()=>{
//						onload(image);
//					});
				} else {
					InvokeOnMainThread (() => {
						onload (image);
					});
				}
			});
		}

		private void SaveToMemory (string url, UIImage image)
		{
			lock (_memory_cache_lock) {
				if (OnMemory.Count > memoryCacheLimit) {
					string ass = "";
					foreach (var img in OnMemory) {
						ass = img.Key;
						break;
					}
					OnMemory.Remove (ass);
				}
				if (!OnMemory.ContainsKey (url)) {
					OnMemory.Add (url, image);
				}
			}
		}

		public UIImage FromMemory (string url)
		{
			lock (_memory_cache_lock) {
				if (OnMemory.ContainsKey (url)) {
					return OnMemory [url];
				}
				return null;
			}
		}

		public UIImage FromCache (string url)
		{
			var memory = this.FromMemory (url);
			if (memory != null) {
				Console.WriteLine ("From Memory " + url);
				return memory;
			}
			var id = this.CalculateMD5Hash (url);
			lock (_file_cache_lock) {
				if (CachedImages == null) {
					loadCacheListFromFile ();
				}
				if (!CachedImages.Contains (id)) {
					return null;
				}
			}
			byte[] buffer = null;
			try {
				buffer = File.ReadAllBytes (this.GetPath (id));
			} catch {
				return null;
			}
			NSData data = NSData.FromArray (buffer);
			var image = UIImage.LoadFromData (data);
			this.SaveToMemory (url, image);
			Console.WriteLine ("From File " + url);
			return image;
		}

		private void SaveToCache (string url, byte[] imagedata)
		{
			var md5 = this.CalculateMD5Hash (url);
			lock (_file_cache_lock) {
				if (CachedImages == null) {
					loadCacheListFromFile ();
				}
				if (CachedImages.Count > FileCacheLimit) {
					fileSystem.FromCacheFolder (CachedImages.First.Value).Delete ();
					CachedImages.RemoveFirst ();
				}
			}
			try {
				fileSystem.FromCacheFolder (md5).Write (imagedata);
			} catch {
				return;
			}
			lock (_file_cache_lock) {
				CachedImages.AddLast (md5);
				saveCacheListToFile ();
			}
		}

		public void FromServerAsync (string url, Action<int> onProgress, Action<UIImage> onComplete)
		{
			lock (_down_track_lock) {
				if (ActiveDownload.Contains (url)) {
					return;
				}
				ActiveDownload.Add (url);
			}
			var downloader = new ImageDownloader (url);
			downloader.OnProgress += (int obj) => {
				onProgress (obj);
			};
			downloader.OnComplete += (bool isFinished, byte[] buffer) => {
				if (!isFinished) {
					return;
				}
				UIImage image = uiImageFromBuffer (url, buffer);
				lock (_down_track_lock) {
					ActiveDownload.Remove (url);
				}
				onComplete (image);
			};
			downloader.startAsync ();
		}

		UIImage uiImageFromBuffer (string url, byte[] buffer)
		{
			UIImage image;
			if (buffer != null) {
				var data = NSData.FromArray (buffer);
				image = UIImage.LoadFromData (data);
				SaveToMemory (url, image);
				SaveToCache (url, buffer);
			} else {
				var data = NSData.FromFile ("error.png");
				image = UIImage.LoadFromData (data);
			}
			return image;
		}

		public UIImage FromSever (string url)
		{
			lock (_down_track_lock) {
				if (ActiveDownload.Contains (url)) {
					return null;
				}
				ActiveDownload.Add (url);
			}
			byte[] buffer = webService.ReadImageBytes (url);
			UIImage image = uiImageFromBuffer (url, buffer);
			lock (_down_track_lock) {
				ActiveDownload.Remove (url);
			}
			return image;
		}

		private void saveCacheListToFile ()
		{
			try {
				var str = Parser.ToJSON (CachedImages);
				fileSystem.FromCacheFolder ("cachefilelist.txt").Write (str);
			} catch {

			}
		}

		private void loadCacheListFromFile ()
		{
			CachedImages = Parser.getObject<LinkedList<string>> (fileSystem.FromCacheFolder ("cachefilelist.txt").Read ());
			if (CachedImages == null)
				CachedImages = new LinkedList<string> ();
		}
		public void ClearCache(){
			CachedImages = new LinkedList<string> ();
			OnMemory = new Dictionary<string, UIImage> ();
			saveCacheListToFile ();
		}
		public void LoadImageIntoView (UIImageView imageView, string url)
		{
			imageView.Image = null;
			//string newImage = await webService.DownloadImageAsync (url);
			//imageView.Image = UIImage.FromBundle (newImage);
		}

		private string GetPath (string md5)
		{
			return fileSystem.FromCacheFolder (md5).GetPath ();
		}

		private string CalculateMD5Hash (string input)
		{
			// step 1, calculate MD5 hash from input
			MD5 md5 = System.Security.Cryptography.MD5.Create ();
			byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes (input);
			byte[] hash = md5.ComputeHash (inputBytes);

			// step 2, convert byte array to hex string
			StringBuilder sb = new StringBuilder ();
			for (int i = 0; i < hash.Length; i++) {
				sb.Append (hash [i].ToString ("X2"));
			}
			return sb.ToString ();
		}

	}
}

