﻿using System;
using HauteShot.Core;
using Foundation;

namespace HauteShot.iOS
{
	public class Settings:ISettings
	{
		public string UserToken{
			get{
				return NSUserDefaults.StandardUserDefaults.StringForKey ("UserToken");
			}
			set{
				var defaults = NSUserDefaults.StandardUserDefaults;
				defaults.SetString (value, "UserToken");
				defaults.Synchronize ();
			}
		}
		public int CurrentUserID{
			get{
				return (int)NSUserDefaults.StandardUserDefaults.IntForKey ("CurrentUserID");
			}
			set{
				var defaults = NSUserDefaults.StandardUserDefaults;
				defaults.SetInt (value, "CurrentUserID");
				defaults.Synchronize ();
			}
		}

		public CurrentUser CurrentUserData {
			get {
				var str = NSUserDefaults.StandardUserDefaults.StringForKey ("CurrentUserData");
				return Parser.getObject<CurrentUser> (str);
			}
			set {
				var defaults = NSUserDefaults.StandardUserDefaults;
				defaults.SetString (Parser.ToJSON(value), "CurrentUserData");
				defaults.Synchronize ();
			}
		}

		public Settings ()
		{
		}
		public void save(){

		}
		public void clear(){
			UserToken = null;
			CurrentUserID = 0;
		}
	}
}

