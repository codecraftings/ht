﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace HauteShot.iOS
{
	public class KeyboardVisibilityManager
	{
		public UIScrollView ScrollContainer;
		public UIView MainView;

		public KeyboardVisibilityManager (UIScrollView scrollview, UIView mainView)
		{
			ScrollContainer = scrollview;
			MainView = mainView;
		}

		public void KeyboardVisible (NSNotification notification)
		{
			CGSize keyboardInfo = UIKeyboard.FrameEndFromNotification (notification).Size;
			var inset = new UIEdgeInsets (0, 0, keyboardInfo.Height, 0);
			ScrollContainer.ContentInset = inset;
			ScrollContainer.ScrollIndicatorInsets = inset;
			var activeField = ScrollContainer.FindFirstResponder ();
			if (activeField != null)
				ScrollContainer.ScrollRectToVisible (activeField.Frame, true);
		}

		public void KeyboardHidden (NSNotification notification)
		{
			ScrollContainer.ContentInset = UIEdgeInsets.Zero;
			ScrollContainer.ScrollIndicatorInsets = UIEdgeInsets.Zero;
		}

		protected void DismissKeyboardOnBackgroundTap ()
		{
			// Add gesture recognizer to hide keyboard
			var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
			tap.AddTarget (() => MainView.EndEditing (true));
			MainView.AddGestureRecognizer (tap);
		}

		public void SetUp ()
		{
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, KeyboardVisible);
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, KeyboardHidden);
			DismissKeyboardOnBackgroundTap ();
		}
	}
}

