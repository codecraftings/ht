﻿using System;
using HauteShot.Core;
using System.Threading.Tasks;
using UIKit;

namespace HauteShot.iOS
{
	public class TaskHandler:ITaskHandler
	{
		public event Action<Exception> TaskExceptionHandler;
		public static event Action<Exception> UnhandledExceptionEventHandler;
		public TaskHandler ()
		{
		}

		public void Handle (ITask task)
		{
			var taskId = UIApplication.SharedApplication.BeginBackgroundTask (() => {
				task.TerminationRequested();
			});
			Task.Run (() => {
				try{
					task.Handle();
				}catch(Exception e){
					handleTaskException(e);
				}finally{
					UIApplication.SharedApplication.EndBackgroundTask(taskId);
				}
			});
		}
		private void handleTaskException(Exception e){
			if (TaskExceptionHandler != null) {
				TaskExceptionHandler.Invoke (e);
			}
			if (TaskExceptionHandler == null && UnhandledExceptionEventHandler != null) {
				UnhandledExceptionEventHandler.Invoke (e);
			}
		}
	}
}

