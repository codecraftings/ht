﻿using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using HauteShot.Core;
using System.Text;

namespace HauteShot.iOS
{
	public class WebRequest:IWebRequest
	{
		private Dictionary<string, string> getParams;
		private Dictionary<string, object> postParams;

		public string URL{ get; set; }

		private string response;
		private string contentBoundary = "-----A34B324s";

		public WebRequest (string url)
		{
			URL = url;
			postParams = new Dictionary<string, object> ();
			getParams = new Dictionary<string, string> ();
		}

		public void SetPostParam (string key, object value)
		{
			postParams.Add (key, value);
		}


		public void SetGetParam (string key, string value)
		{
			getParams.Add (key, value);
		}


		public void SetPostParam (Dictionary<string, object> parameters)
		{
			foreach (var param in parameters) {
				SetPostParam (param.Key, param.Value);
			}
		}


		public void SetGetParam (Dictionary<string, string> parameters)
		{
			foreach (var param in parameters) {
				SetGetParam (param.Key, param.Value);
			}
		}

		public void Send (string method = "GET")
		{
			var request = HttpWebRequest.Create (this.getUrlWithQuery ());
			request.Method = method;
			if (postParams.Count > 0) {
				request.ContentType = String.Format ("multipart/form-data; boundary={0}", this.contentBoundary);
				using (var stream = request.GetRequestStream ()) {
					this.writePostData (stream);
				}
			}
			HttpWebResponse response;
			try {
				response = request.GetResponse () as HttpWebResponse;
			} catch (WebException e) {
				if (e.Response == null) {
					throw new NotConnectedException ();
				}
				response = e.Response as HttpWebResponse;
			}
			using (var stream = response.GetResponseStream ()) {
				var reader = new StreamReader (stream);
				this.response = reader.ReadToEnd ();
			}
		}

		public T GetResponse<T> ()
		{
			if (response == null) {
				throw new Exception ("No response!");
			}
			return Parser.getObject<T> (response);
		}

		private void writePostData (Stream stream)
		{
			var boundary = String.Format ("--{0}", this.contentBoundary);
			foreach (var param in postParams) {
				writeStringLine (stream, boundary);
				if (param.Value is string) {
					writeStringField (stream, param.Key, param.Value as string);
				} else if (param.Value is UploadableFile) {
					writeFileData (stream, param.Key, (param.Value as UploadableFile).Path);
				} else {
					writeStringField (stream, param.Key, Parser.ToJSON (param.Value));
				}
			}
			writeStringLine (stream, String.Format ("{0}--", boundary));
		}

		private void writeStringField (Stream stream, string key, string data)
		{
			writeStringLine (stream, String.Format ("Content-Disposition: form-data; name=\"{0}\"", key));
			writeStringLine (stream, "");
			writeStringLine (stream, data);
		}

		private void writeFileData (Stream stream, string key, string filePath)
		{
			writeStringLine (stream, String.Format ("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}.png\"", key, "chrome"));
			writeStringLine (stream, "Content-Type: image/png");
			writeStringLine (stream, "Content-Transfer-Encoding: binary");
			writeStringLine (stream, "");
			byte[] file = File.ReadAllBytes (filePath);
			stream.Write (file, 0, file.Length);
			writeStringLine (stream, "");
		}

		private void writeStringLine (Stream stream, string line)
		{
			StringBuilder str = new StringBuilder ();
			str.AppendLine (line);
			byte[] buffer = Encoding.Default.GetBytes (str.ToString ());
			stream.Write (buffer, 0, buffer.Length);
		}

		private string getUrlWithQuery ()
		{
			var query = new StringBuilder ();
			int i = 0;
			foreach (var param in getParams) {
				if (i == (getParams.Count - 1)) {
					query.AppendFormat ("{0}={1}", WebUtility.UrlEncode (param.Key), WebUtility.UrlEncode (param.Value));
				} else {
					query.AppendFormat ("{0}={1}&", WebUtility.UrlEncode (param.Key), WebUtility.UrlEncode (param.Value));
				}
				i += 1;
			}
			return String.Format ("{0}?{1}", URL, query.ToString ());
		}

	}
}

