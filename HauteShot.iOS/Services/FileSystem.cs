﻿using System;
using HauteShot.Core;
using System.IO;
using Foundation;

namespace HauteShot.iOS
{
	public class FileSystem:IFileSystem
	{
		public FileSystem ()
		{
		}

		public IOperationalFile FromTempFolder (string filename)
		{
			var uri = NSFileManager.DefaultManager.GetUrls (NSSearchPathDirectory.TrashDirectory, NSSearchPathDomain.User) [0];
			return new OperationalFile (Path.Combine (uri.Path, filename));
		}

		public IOperationalFile FromDocumentFolder (string filename)
		{
			var uri = NSFileManager.DefaultManager.GetUrls (NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomain.User) [0];
			return new OperationalFile (Path.Combine (uri.Path, filename));
		}

		public IOperationalFile FromCacheFolder (string filename)
		{
			var uri = NSFileManager.DefaultManager.GetUrls (NSSearchPathDirectory.CachesDirectory, NSSearchPathDomain.User) [0];
			return new OperationalFile (Path.Combine (uri.Path, filename));
		}

	}

	public class OperationalFile:IOperationalFile
	{

		private string filePath;

		public OperationalFile (string filePath)
		{
			this.filePath = filePath;
		}

		public string GetPath ()
		{
			return filePath;
		}

		public string Read ()
		{
			if (File.Exists (filePath))
				return File.ReadAllText (filePath);
			else
				return null;
		}

		public void Write (byte[] data)
		{
			File.WriteAllBytes (filePath, data);
		}

		public void Write (string data)
		{
			File.WriteAllText (filePath, data);
		}

		public void Delete ()
		{
			if(File.Exists(filePath))
			File.Delete (filePath);
		}

	}
}

