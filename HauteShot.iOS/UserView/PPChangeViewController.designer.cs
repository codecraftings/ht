// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace HauteShot.iOS
{
	[Register ("PPChangeViewController")]
	partial class PPChangeViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HoverableButton ChangeButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CircularImage PPView { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ChangeButton != null) {
				ChangeButton.Dispose ();
				ChangeButton = null;
			}
			if (PPView != null) {
				PPView.Dispose ();
				PPView = null;
			}
		}
	}
}
