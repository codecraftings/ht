using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using HauteShot.Core;

namespace HauteShot.iOS
{
	partial class PPChangeViewController : BaseUIController
	{
		private PhotoPicker picker;
		public new ChangePPViewModel ViewModel{
			get{
				return base.ViewModel as ChangePPViewModel;
			}
			set{
				base.ViewModel = value as BaseViewModel;
			}
		}
		public PPChangeViewController (IntPtr handle) : base (handle)
		{
			ViewModel = new ChangePPViewModel (this);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			PPView.Layer.BorderColor = UIColor.FromRGBA (68, 181, 158, 200).CGColor;
			PPView.Layer.BorderWidth = 2;
			ChangeButton.HoverBGColor = UIColor.FromRGBA (68, 181, 158, 200);
			ChangeButton.Layer.CornerRadius = 3;
			ChangeButton.ApplyMinimalShadow ();
			ChangeButton.TouchUpInside += HandleChangePPButtonClicked;
		}

		public override void NotifyViewAppeared ()
		{
			base.NotifyViewAppeared ();
			NavController.NoBackButton ();
			NavController.NeedActionFooter ();
			NavController.NoShadow ();
			NavController.RegisterActionFooterEvents (HandleOnDeleteButton, HandleOnDoneClicked);
		}

		public override void NotifyViewDisappearing ()
		{
			base.NotifyViewDisappearing ();
			NavController.UnRegisterActionFooterEvents (HandleOnDeleteButton, HandleOnDoneClicked);
		}

		public void HandleOnDeleteButton (object sender, EventArgs e)
		{
			Navigator.HomeFeedScreen ();
		}

		public async void HandleOnDoneClicked (object sender, EventArgs e)
		{
			if (picker != null && picker.PickedPhoto != null) {
				NavController.ShowLoading (true);
				var imgPath = await picker.PrepareImageForPP (null);
				ViewModel.UploadPhoto(imgPath, ()=>{
				});
				NavController.HideLoading ();
				Navigator.HomeFeedScreen ();
			}
		}

		void HandleChangePPButtonClicked (object sender, EventArgs e)
		{
			HTNavigator.Instance.ActionSheet (new Dictionary<string, Action> {
				{ "Take Photo", HandleTakingPhoto },
				{ "Browse Gallery", HandleBrowsingPhoto }
			});
		}

		public void HandleTakingPhoto ()
		{
			if (picker == null) {
				picker = new PhotoPicker ();
				picker.PhotoPicked += HandlePhotoPicked;
			}
			picker.FromCamera ();
		}

		void HandlePhotoPicked (object sender, EventArgs e)
		{
			PPView.ContentMode = UIViewContentMode.ScaleAspectFill;
			PPView.Image = picker.PickedPhoto;
		}

		public void HandleBrowsingPhoto ()
		{
			if (picker == null) {
				picker = new PhotoPicker ();
				picker.PhotoPicked += HandlePhotoPicked;
			}
			picker.FromGallery ();
		}
	}
}
