// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace HauteShot.iOS
{
	[Register ("UserProfileViewController")]
	partial class UserProfileViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView AlbumTiles { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView badge1 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView badge2 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView badge3 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView badge4 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView badges { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CardView DetailsCard { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HoverableButton FollowButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel NickName { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIScrollView ScrollContainer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HoverableButton UnfollowButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel UserMeta { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel UserName { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView UserPhoto { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (AlbumTiles != null) {
				AlbumTiles.Dispose ();
				AlbumTiles = null;
			}
			if (badge1 != null) {
				badge1.Dispose ();
				badge1 = null;
			}
			if (badge2 != null) {
				badge2.Dispose ();
				badge2 = null;
			}
			if (badge3 != null) {
				badge3.Dispose ();
				badge3 = null;
			}
			if (badge4 != null) {
				badge4.Dispose ();
				badge4 = null;
			}
			if (badges != null) {
				badges.Dispose ();
				badges = null;
			}
			if (DetailsCard != null) {
				DetailsCard.Dispose ();
				DetailsCard = null;
			}
			if (FollowButton != null) {
				FollowButton.Dispose ();
				FollowButton = null;
			}
			if (NickName != null) {
				NickName.Dispose ();
				NickName = null;
			}
			if (ScrollContainer != null) {
				ScrollContainer.Dispose ();
				ScrollContainer = null;
			}
			if (UnfollowButton != null) {
				UnfollowButton.Dispose ();
				UnfollowButton = null;
			}
			if (UserMeta != null) {
				UserMeta.Dispose ();
				UserMeta = null;
			}
			if (UserName != null) {
				UserName.Dispose ();
				UserName = null;
			}
			if (UserPhoto != null) {
				UserPhoto.Dispose ();
				UserPhoto = null;
			}
		}
	}
}
