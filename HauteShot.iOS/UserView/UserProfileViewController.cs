using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using HauteShot.Core;
using System.Collections.Generic;

namespace HauteShot.iOS
{
	partial class UserProfileViewController : BaseUIController
	{
		public new UserProfileViewModel ViewModel {
			get {
				return (UserProfileViewModel)base.ViewModel;
			}
			set {
				base.ViewModel = (BaseViewModel)value;
			}
		}

		public int UserID;

		public UserProfileViewController (IntPtr handle) : base (handle)
		{
			this.ViewModel = new UserProfileViewModel (this);
		}

		public List<AlbumTileView> AlbumViews;
		public override void NotifyViewAppeared ()
		{
			base.NotifyViewAppeared ();
			NavController.FooterMenu ().TabHome ();
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ScrollContainer.TranslatesAutoresizingMaskIntoConstraints = false;
			AlbumTiles.TranslatesAutoresizingMaskIntoConstraints = false;
			this.View.AddConstraint (NSLayoutConstraint.Create (ScrollContainer, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View, NSLayoutAttribute.Width, 1, 0));
			ScrollContainer.AddConstraint (NSLayoutConstraint.Create (DetailsCard, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ScrollContainer, NSLayoutAttribute.Width, 1, -12));
			ScrollContainer.AddConstraint (NSLayoutConstraint.Create (AlbumTiles, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ScrollContainer, NSLayoutAttribute.Width, 1, -12));
			ScrollContainer.DefineLayout (new string[] {
				"|-6-[DetailsCard]",
				"|-6-[AlbumTiles]",
				"V:|-5-[DetailsCard]-10-[AlbumTiles]-10-|"
			}, "DetailsCard", DetailsCard, "AlbumTiles", AlbumTiles);
			initializeFollowButtons ();
			var gesture = new UIHoverAndTapGesture (onBadgesTapped);
			badges.AddGestureRecognizer (gesture);
			if (UserID != 0) {
				ViewModel.LoadFromCache (UserID);
				ViewModel.FetchFromServer (UserID);
			}
		}

		private void initializeFollowButtons ()
		{
			FollowButton.ApplyMinimalShadow ();
			FollowButton.BackgroundColor = UIColor.White;
			FollowButton.SetTitleColor (UIColor.FromRGB (102, 204, 255), UIControlState.Normal);
			FollowButton.HoverBGColor = UIColor.FromRGB (102, 204, 255);
			UnfollowButton.HoverBGColor = UIColor.FromRGB (51, 51, 51);
			UnfollowButton.ApplyMinimalShadow ();
			FollowButton.Hidden = true;
			UnfollowButton.Hidden = true;
			FollowButton.TouchUpInside += HandleTouchFollowButton;
			UnfollowButton.TouchUpInside += HandleTouchUnfollowButton;
		}

		void HandleTouchUnfollowButton (object sender, EventArgs e)
		{
			UnfollowButton.Hidden = true;
			FollowButton.Enabled = false;
			FollowButton.Hidden = false;
			ViewModel.UnFollowUser (() => {
				FollowButton.Enabled = true;
			});
		}

		void HandleTouchFollowButton (object sender, EventArgs e)
		{
			FollowButton.Hidden = true;
			UnfollowButton.Enabled = false;
			UnfollowButton.Hidden = false;
			ViewModel.FollowUser (() => {
				UnfollowButton.Enabled = true;
			});
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			NavController.NeedFooter ();
			NavController.NeedBackButton ();
			NavController.HaveShadow ();
		}

		public void onBadgesTapped (UIHoverAndTapGesture tap)
		{
			if (tap.State == UIGestureRecognizerState.Began) {
				UIView.Animate (.2, () => {
					badges.BackgroundColor = UIColor.FromRGBA (200, 200, 200, 100);
				});
				return;
			}
			if (tap.State == UIGestureRecognizerState.Cancelled) {
				UIView.Animate (.2, () => {
					badges.BackgroundColor = UIColor.Clear;
				});
				return;
			}
			if (tap.State == UIGestureRecognizerState.Recognized) {
				UIView.Animate (.2, () => {
					badges.BackgroundColor = UIColor.Clear;
				});
				if (ViewModel.UserData.Badges.Count > 0)
					Navigator.BadgesListScreen (ViewModel.UserData);
				return;
			}
			return;
		}

		private UIImageView noBadgesNotice;

		private void updateBadges ()
		{
			badge1.Hidden = true;
			badge2.Hidden = true;
			badge3.Hidden = true;
			badge4.Hidden = true;
			if (ViewModel.UserData.Badges == null || ViewModel.UserData.Badges.Count < 1) {
				if (noBadgesNotice == null) {
					noBadgesNotice = new UIImageView {
						Image = UIImage.FromBundle ("nobadges.png"),
						BackgroundColor = UIColor.White,
						ContentMode = UIViewContentMode.ScaleAspectFit
					};
					badges.AddSubview (noBadgesNotice);
					badges.DefineLayout (new string[]{ "|[notice]|", "V:|[notice]|" }, "notice", noBadgesNotice);
				}
				return;
			} else {
				if (noBadgesNotice != null) {
					noBadgesNotice.RemoveFromSuperview ();
					noBadgesNotice = null;
				}
			}
			List<Badge> b;
			var total = ViewModel.UserData.Badges.Count;
			if (total > 4) {
				b = ViewModel.UserData.Badges.GetRange (0, 4);
			} else {
				b = ViewModel.UserData.Badges;
			}
			HideUIBusy (badges);
			if (total > 0) {
				badge1.Hidden = false;
				badge1.Image = null;
				ShowUIBusy (badge1);
				ImageLoader.Instance.LoadImage (b [0].IconUrl, img => {
					badge1.Image = img;
					HideUIBusy (badge1);
				});
			}
			if (total > 1) {
				badge2.Hidden = false;
				badge2.Image = null;
				ShowUIBusy (badge2);
				ImageLoader.Instance.LoadImage (b [1].IconUrl, img => {
					badge2.Image = img;
					HideUIBusy (badge2);
				});
			}
			if (total > 2) {
				badge3.Hidden = false;
				badge3.Image = null;
				ShowUIBusy (badge3);
				ImageLoader.Instance.LoadImage (b [2].IconUrl, img => {
					badge3.Image = img;
					HideUIBusy (badge3);
				});
			}
			if (total > 3) {
				badge4.Hidden = false;
				badge4.Image = null;
				ShowUIBusy (badge4);
				ImageLoader.Instance.LoadImage (b [3].IconUrl, img => {
					badge4.Image = img;
					HideUIBusy (badge4);
				});
			}
		}

		public void updateAlbumTiles ()
		{
			if (ViewModel.UserData.Albums == null) {
				return;
			}
			if (ViewModel.UserData.Albums.Count < 1) {
				return;
			}
			if (AlbumViews == null) {
				AlbumViews = new List<AlbumTileView> ();
			}
			HideUIBusy (AlbumTiles);


			UIView topView = new UIView ();
			AlbumTiles.Add (topView);

			AlbumTiles.DefineLayout (new string[] {
				"|[topView]|",
				"V:|[topView(1)]"
			}, "topView", topView);
			for (var i = 0; i < ViewModel.UserData.Albums.Count; i++) {
				var album = ViewModel.UserData.Albums [i];
				var tile = new AlbumTileView (album.feature_photos.Length);
				AlbumTiles.AddSubview (tile);

				AlbumTiles.DefineLayout (new string[] {
					"|[tile]|",
					"V:[topView]-10-[tile]"
				}, "tile", tile, "topView", topView);
				if (i == (ViewModel.UserData.Albums.Count - 1)) {
					//AlbumTiles.ConstrainLayout (() => tile.Frame.Bottom == AlbumTiles.Frame.Bottom);
					AlbumTiles.DefineLayout (new string[]{ "V:[tile]|" }, "tile", tile);
				}
				topView = tile;
				tile.AlbumTitle.Text = album.title;
				tile.onPhotoSelected = onPhotoClicked;
				foreach (var photo in album.feature_photos) {
					ImageLoader.Instance.LoadImage (photo.GetThumbSrc ((float)UIScreen.MainScreen.Bounds.Width / 3), image => {
						tile.AddPhoto (image, photo.id);
						updateScrollView ();
					});
				}
				tile.LayoutSubviews ();
				AlbumViews.Add (tile);
			}
			AlbumTiles.InvalidateIntrinsicContentSize ();
			AlbumTiles.ClipsToBounds = true;
		}

		private void updateScrollView ()
		{
			ScrollContainer.ScrollEnabled = true;
			ScrollContainer.LayoutSubviews ();
			ScrollContainer.ContentSize = new CoreGraphics.CGSize (ScrollContainer.Bounds.Width, AlbumTiles.Frame.Bottom + 40);
		}

		public void onPhotoClicked (int photoId)
		{
			Navigator.PhotoScreen (photoId);
		}

		public void updateUserCard ()
		{
			UserName.Text = ViewModel.UserData.FullName;
			NickName.Text = ViewModel.UserData.UserName;
			UserMeta.Text = ViewModel.UserData.PhotosCount + " uploads";
			if (ViewModel.UserData.IsFollowed) {
				UnfollowButton.Hidden = false;
				FollowButton.Hidden = true;
			} else {
				UnfollowButton.Hidden = true;
				FollowButton.Hidden = false;
			}
			if (ViewModel.UserData.Equals (Local.Instance.GetCurrentUser ())) {
				UnfollowButton.Hidden = true;
				FollowButton.Hidden = true;
			}
			ImageLoader.Instance.LoadImage (ViewModel.UserData.ProfilePicUrl, img => {
				UserPhoto.Image = img;
				HideUIBusy (UserPhoto);
			});
		}

		public override void OnViewModelUpdated (string updateId)
		{
			base.OnViewModelUpdated (null);

			if (ViewModel.UserData == null) {
				NavController.ShowLoading ();
			} else {
				NavController.HideLoading ();
				updateUserCard ();
				updateBadges ();
				updateAlbumTiles ();
			}
			updateScrollView ();
		}
	}
}
