using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using HauteShot.Core;

namespace HauteShot.iOS
{
	partial class ProfileEditViewController : BaseUIController
	{
		private PhotoPicker picker;

		public int UserID{ get; set; }

		public new ProfileEditViewModel ViewModel {
			get {
				return base.ViewModel as ProfileEditViewModel;
			}
			set {
				base.ViewModel = value as BaseViewModel;
			}
		}

		public ProfileEditViewController (IntPtr handle) : base (handle)
		{
			ViewModel = new ProfileEditViewModel (this);
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			NavController.NoShadow ();
		}

		public override void NotifyViewAppeared ()
		{
			NavController.NeedActionFooter ();
			NavController.NoShadow ();
			ViewModel.LoadUserData ();
			NavController.RegisterActionFooterEvents (HandleOnDeleteButton, HandleOnDoneClicked);
		}

		public override void NotifyViewDisappearing ()
		{
			base.NotifyViewDisappearing ();
			NavController.UnRegisterActionFooterEvents (HandleOnDeleteButton, HandleOnDoneClicked);

		}

		public void HandleOnDeleteButton (object sender, EventArgs e)
		{
			if (!Navigator.LastScreen ()) {
				Navigator.HomeFeedScreen ();
			}
		}

		public async void HandleOnDoneClicked (object sender, EventArgs e)
		{
			ViewModel.UserData.FullName = NameTF.Value;
			ViewModel.UserData.UserName = UserNameTF.Value;
			ViewModel.UserData.Email = EmailTF.Value;
			IPromise<object> promise;
			NavController.ShowLoading (true);
			if (picker != null && picker.PickedPhoto != null) {
				var imgPath = await picker.PrepareImageForPP (null);
				promise = ViewModel.SaveProfile (ViewModel.UserData, imgPath);
			} else {
				promise = ViewModel.SaveProfile (ViewModel.UserData, null);
			}
			promise.IfSuccess (b => {
				InvokeOnMainThread (() => {
					ImageLoader.Instance.ClearCache();
					if (!Navigator.LastScreen ()) {
						Navigator.HomeFeedScreen ();
					}
				});
			}).IfFail (error => {
				InvokeOnMainThread (() => {
					NavController.ShowErrorNotice (error.Message);
				});
			}).Finally (() => {
				InvokeOnMainThread (() => {
					NavController.HideLoading ();
				});
			});
		}

		public override void OnViewModelUpdated (string updateID)
		{
			base.OnViewModelUpdated (updateID);
			NameTF.Value = ViewModel.UserData.FullName;
			UserNameTF.Value = ViewModel.UserData.UserName;
			EmailTF.Value = ViewModel.UserData.Email;
			ImageLoader.Instance.LoadImage (ViewModel.UserData.ProfilePicUrl, img => {
				PPView.Image = img;
			});
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			designFormElements ();
			new KeyboardVisibilityManager (ScrollContainer, View).SetUp ();
			ScrollContainer.FixSuckingScrollViewError ();
			PPView.Layer.BorderColor = UIColor.FromRGBA (68, 181, 158, 200).CGColor;
			PPView.Layer.BorderWidth = 3;
			designPPButton ();
			ChangePPButton.TouchUpInside += HandleChangePPButtonClicked;
		}

		void HandleChangePPButtonClicked (object sender, EventArgs e)
		{
			HTNavigator.Instance.ActionSheet (new Dictionary<string, Action> {
				{ "Take Photo", HandleTakingPhoto },
				{ "Browse Gallery", HandleBrowsingPhoto }
			});
		}

		public void HandleTakingPhoto ()
		{
			if (picker == null) {
				picker = new PhotoPicker ();
				picker.PhotoPicked += HandlePhotoPicked;
			}
			picker.FromCamera ();
		}

		void HandlePhotoPicked (object sender, EventArgs e)
		{
			PPView.Image = picker.PickedPhoto;
		}

		public void HandleBrowsingPhoto ()
		{
			if (picker == null) {
				picker = new PhotoPicker ();
				picker.PhotoPicked += HandlePhotoPicked;
			}
			picker.FromGallery ();
		}

		void designPPButton ()
		{
			ChangePPButton.SetTitleColor (UIColor.FromRGBA (68, 181, 158, 200), UIControlState.Normal);
			ChangePPButton.SetTitleColor (UIColor.White, UIControlState.Highlighted);
			ChangePPButton.SetTitleColor (UIColor.White, UIControlState.Selected);
			ChangePPButton.HoverBGColor = UIColor.FromRGBA (68, 181, 158, 200);
			ChangePPButton.Layer.CornerRadius = 3;
			ChangePPButton.ApplyMinimalShadow ();
		}

		private void designFormElements ()
		{
			NameTF.Update ("Name", "Icons/user.png", UserNameTF, "");
			UserNameTF.Update ("User Name", "Icons/user.png", EmailTF, "");
			EmailTF.Update ("Email", "Icons/letter.png", () => {

			}, "");
		}


	}
}
