using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using HauteShot.Core;

namespace HauteShot.iOS
{
	partial class ChangePasswordController : BaseUIController
	{
		public new ProfileEditViewModel ViewModel {
			get {
				return base.ViewModel as ProfileEditViewModel;
			}
			set {
				base.ViewModel = value as BaseViewModel;
			}
		}

		public ChangePasswordController (IntPtr handle) : base (handle)
		{
			ViewModel = new ProfileEditViewModel (this);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			new KeyboardVisibilityManager (ScrollContainer, View).SetUp ();
			ScrollContainer.FixSuckingScrollViewError ();
			OldPassTF.Update ("Old Password", "Icons/pass.png", NewPassTf);
			OldPassTF.SecureInput ();
			NewPassTf.Update ("New Password", "Icons/pass.png", ConfirmPassTf);
			NewPassTf.SecureInput ();
			ConfirmPassTf.Update ("Confirm New Password", "Icons/pass.png", OnSubmit);
			ConfirmPassTf.SecureInput ();
		}

		public override void NotifyViewAppeared ()
		{
			NavController.NeedActionFooter ();
			NavController.NoShadow ();
			NavController.RegisterActionFooterEvents (HandleOnDelete, HandleOnDone);
		}

		public override void NotifyViewDisappearing ()
		{
			NavController.UnRegisterActionFooterEvents (HandleOnDelete, HandleOnDone);
		}

		public void HandleOnDelete (object sender, EventArgs e)
		{
			goBack ();
		}

		private void goBack ()
		{
			if (!Navigator.LastScreen ()) {
				Navigator.HomeFeedScreen ();
			}
		}

		public void HandleOnDone (object sender, EventArgs e)
		{
			OnSubmit ();
		}

		public void OnSubmit ()
		{
			NavController.ShowLoading (true);
			ViewModel.ChangePassword (OldPassTF.Value, NewPassTf.Value, ConfirmPassTf.Value)
				.IfFail (e => {
				InvokeOnMainThread (() => {
					NavController.ShowErrorNotice (e.Message);
				});
			})
				.IfSuccess (b => {
				InvokeOnMainThread (() => {
					goBack ();
				});
			})
				.Finally (() => {
				InvokeOnMainThread (() => {
					NavController.HideLoading ();
				});
			});
		}
	}
}
