// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace HauteShot.iOS
{
	[Register ("ProfileEditViewController")]
	partial class ProfileEditViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HoverableButton ChangePPButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		AwesomeInputField EmailTF { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		AwesomeInputField NameTF { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CircularImage PPView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIScrollView ScrollContainer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		AwesomeInputField UserNameTF { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ChangePPButton != null) {
				ChangePPButton.Dispose ();
				ChangePPButton = null;
			}
			if (EmailTF != null) {
				EmailTF.Dispose ();
				EmailTF = null;
			}
			if (NameTF != null) {
				NameTF.Dispose ();
				NameTF = null;
			}
			if (PPView != null) {
				PPView.Dispose ();
				PPView = null;
			}
			if (ScrollContainer != null) {
				ScrollContainer.Dispose ();
				ScrollContainer = null;
			}
			if (UserNameTF != null) {
				UserNameTF.Dispose ();
				UserNameTF = null;
			}
		}
	}
}
