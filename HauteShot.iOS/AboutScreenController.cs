using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using HauteShot.Core;

namespace HauteShot.iOS
{
	partial class AboutScreenController : BaseUIController
	{
		public String PageName{ get; set;}
		public AboutScreenController (IntPtr handle) : base (handle)
		{
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ScrollContainer.BackgroundColor = UIColor.White;
			NavController.ShowLoading ();
			ContentView.LoadFinished += (object sender, EventArgs e) => {
				NavController.HideLoading();
			};
			ContentView.LoadError += (object sender, UIWebErrorArgs e) => {
				NavController.HideLoading();
				NavController.ShowErrorNotice("Error connecting to server");
			};
			loadPage ();
		}
		private void loadPage(){
			ContentView.LoadRequest (new NSUrlRequest (new NSUrl(Server.BASE_URL+"/"+PageName)));
		}
	}
}
