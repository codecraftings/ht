using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using HauteShot.Core;
using System.Net;

namespace HauteShot.iOS
{
	partial class MainViewController : BaseUIController
	{
		IUserRepo userRepo;
		public MainViewController (IntPtr handle) : base (handle)
		{
		}
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			userRepo = ServiceContainer.Resolve<IUserRepo> ();
			if (!userRepo.IsLoggedIn()) {
				Navigator.LoginScreen ();
			} else {
				Navigator.HomeFeedScreen ();
			}
		}
	}
}
