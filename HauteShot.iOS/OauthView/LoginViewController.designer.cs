// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace HauteShot.iOS
{
	[Register ("LoginViewController")]
	partial class LoginViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HoverableButton LoginButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		AwesomeInputField PassTF { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIScrollView ScrollContainer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HoverableButton ToSignup { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		AwesomeInputField UserNameTF { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (LoginButton != null) {
				LoginButton.Dispose ();
				LoginButton = null;
			}
			if (PassTF != null) {
				PassTF.Dispose ();
				PassTF = null;
			}
			if (ScrollContainer != null) {
				ScrollContainer.Dispose ();
				ScrollContainer = null;
			}
			if (ToSignup != null) {
				ToSignup.Dispose ();
				ToSignup = null;
			}
			if (UserNameTF != null) {
				UserNameTF.Dispose ();
				UserNameTF = null;
			}
		}
	}
}
