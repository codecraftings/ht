using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using HauteShot.Core;
using CoreGraphics;

namespace HauteShot.iOS
{
	partial class SignupViewController : BaseUIController
	{
		public new SignupViewModel ViewModel {
			get {
				return (SignupViewModel)base.ViewModel;
			}
			set {
				base.ViewModel = (BaseViewModel)value;
			}
		}

		public SignupViewController (IntPtr handle) : base (handle)
		{
			ViewModel = new SignupViewModel (this);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ScrollContainer.FixSuckingScrollViewError ();
			new KeyboardVisibilityManager (ScrollContainer, View).SetUp ();
			NameTF.Update ("Full Name", "Icons/user.png", UserNameTF);
			UserNameTF.Update ("User Name", "Icons/user.png", EmailTF);
			EmailTF.Update ("Email", "Icons/letter.png", PassTF);
			EmailTF.AweInput.KeyboardType = UIKeyboardType.EmailAddress;
			PassTF.Update ("Password", "Icons/pass.png", () => {
				submitSignup();
			});
			PassTF.SecureInput ();
			SignupButton.ApplyMinimalShadow ();
			SignupButton.Layer.CornerRadius = 3;
			SignupButton.HoverBGColor = UIColor.FromRGB (68, 181, 158);
			SignupButton.TouchUpInside += (object sender, EventArgs e) => {
				submitSignup ();
			};
			EULALABEL.UserInteractionEnabled = true;
			EULALABEL.AddGestureRecognizer (new UITapGestureRecognizer (() => {
				Navigator.TermsScreen();
			}));
		}
		public override void NotifyViewAppeared ()
		{
			base.NotifyViewAppeared ();
			NavController.NoShadow ();
			NavController.NoFooter ();
		}
		void submitSignup ()
		{
			NavController.ShowLoading ();
			ViewModel.Name = NameTF.Value;
			ViewModel.Email =EmailTF.Value;
			ViewModel.Username = UserNameTF.Value;
			ViewModel.Password = PassTF.Value;
			ViewModel.TrySignup (() =>  {
				NavController.HideLoading();
				Navigator.ChangePPScreen ();
			}, () =>  {
				NavController.HideLoading();
				NavController.ShowErrorNotice(ViewModel.Error);
			});
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			NavController.NoFooter ();
			NavController.NeedBackButton ();
		}
	}
}
