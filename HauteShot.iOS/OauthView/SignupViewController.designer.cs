// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace HauteShot.iOS
{
	[Register ("SignupViewController")]
	partial class SignupViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		AwesomeInputField EmailTF { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextView EULALABEL { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		AwesomeInputField NameTF { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		AwesomeInputField PassTF { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIScrollView ScrollContainer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HoverableButton SignupButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		AwesomeInputField UserNameTF { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (EmailTF != null) {
				EmailTF.Dispose ();
				EmailTF = null;
			}
			if (EULALABEL != null) {
				EULALABEL.Dispose ();
				EULALABEL = null;
			}
			if (NameTF != null) {
				NameTF.Dispose ();
				NameTF = null;
			}
			if (PassTF != null) {
				PassTF.Dispose ();
				PassTF = null;
			}
			if (ScrollContainer != null) {
				ScrollContainer.Dispose ();
				ScrollContainer = null;
			}
			if (SignupButton != null) {
				SignupButton.Dispose ();
				SignupButton = null;
			}
			if (UserNameTF != null) {
				UserNameTF.Dispose ();
				UserNameTF = null;
			}
		}
	}
}
