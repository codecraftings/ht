using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using HauteShot.Core;
using System.Threading.Tasks;
using CoreGraphics;

namespace HauteShot.iOS
{
	partial class LoginViewController : BaseUIController
	{
		//private NSObject observer1;
		//private NSObject observer2;
		public new LoginViewModel ViewModel {
			get {
				return (LoginViewModel)base.ViewModel;
			}
			set {
				base.ViewModel = (BaseViewModel)value;
			}
		}

		public LoginViewController (IntPtr handle) : base (handle)
		{
			this.ViewModel = new LoginViewModel (this);
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
		}
		public override void NotifyViewAppeared ()
		{
			base.NotifyViewAppeared ();
			NavController.NoFooter ();
			NavController.NoBackButton ();
			NavController.NoShadow ();
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ScrollContainer.FixSuckingScrollViewError ();
			new KeyboardVisibilityManager (ScrollContainer, View).SetUp ();
			UserNameTF.Update ("User Name", "Icons/user.png", PassTF);
			PassTF.Update ("Password", "Icons/pass.png", () => {
				submitSignup ();
			});
			PassTF.SecureInput ();
			LoginButton.ApplyMinimalShadow ();
			LoginButton.Layer.CornerRadius = 3;
			LoginButton.HoverBGColor = UIColor.FromRGB (61, 181, 158);
			LoginButton.SetTitleColor (UIColor.FromRGB (61, 181, 158), UIControlState.Normal);
			LoginButton.SetTitleColor (UIColor.White, UIControlState.Highlighted);
			ToSignup.ApplyMinimalShadow ();
			ToSignup.Layer.CornerRadius = 0;
			ToSignup.HoverBGColor = UIColor.FromRGB (203, 203, 203);
			LoginButton.TouchUpInside += (object sender, EventArgs e) => {
				submitSignup ();
			};
			ToSignup.TouchUpInside += (object sender, EventArgs e) => {
				Navigator.SingupScreen ();
			};
		}

		void submitSignup ()
		{
			LoginButton.Enabled = false;
			NavController.ShowLoading ();
//			NoticeLabel.Hidden = true;
			ViewModel.UserName = UserNameTF.Value;
			ViewModel.Password = PassTF.Value;
			ViewModel.TryLogin (() =>  {
				NavController.HideLoading();
				Navigator.HomeFeedScreen ();
			}, () =>  {
				NavController.HideLoading();
				NavController.ShowErrorNotice(ViewModel.Error);
				LoginButton.Enabled = true;
			});
		}
	}
}
