// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace HauteShot.iOS
{
	[Register ("MainNavController")]
	partial class MainNavController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView ContainerView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HTFooterView Footer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HTHeaderView Header { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ContainerView != null) {
				ContainerView.Dispose ();
				ContainerView = null;
			}
			if (Footer != null) {
				Footer.Dispose ();
				Footer = null;
			}
			if (Header != null) {
				Header.Dispose ();
				Header = null;
			}
		}
	}
}
