﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using UIKit;
using Foundation;
using CoreGraphics;

namespace HauteShot.iOS
{
	public static class Layout
	{
		public static void ApplyMinimalShadow(this UIView view){
			var shadowSize = view.Bounds;
			shadowSize.Width -= 0f;
			view.Layer.ShadowPath = UIBezierPath.FromRect (shadowSize).CGPath;
			view.Layer.ShadowColor = UIColor.Black.CGColor;
			view.Layer.ShadowOpacity = 0.4f;
			view.Layer.ShadowOffset = new CGSize (0f, 0.8f);
			view.Layer.ShadowRadius = 0.5f;
			view.ClipsToBounds = false;
		}
		public static void ApplyBoldShadow(this UIView view){
			var shadowSize = view.Bounds;
			shadowSize.Width -= 0f;
			view.Layer.ShadowPath = UIBezierPath.FromRect (shadowSize).CGPath;
			view.Layer.ShadowColor = UIColor.Black.CGColor;
			view.Layer.ShadowOpacity = 0.3f;
			view.Layer.ShadowOffset = new CGSize (0.5f, 0.5f);
//			view.Layer.ShadowRadius = 1f;
			view.ClipsToBounds = false;
		}
		public static void RemoveShadow(this UIView view){
			view.Layer.ShadowOpacity = 0;
		}
		public static void FixSuckingScrollViewError(this UIScrollView scrollContainer){
			var view = new UIView ();
			scrollContainer.AddSubview (view);
			scrollContainer.DefineLayout (new string[]{ "|-0-[view]-0-|"}, "view", view);
			scrollContainer.AddConstraint (NSLayoutConstraint.Create (view, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null, NSLayoutAttribute.Width, 1, scrollContainer.Superview.Bounds.Size.Width));
		}
		public static void DefineLayout(this UIView view, string[] formats, params object[] objectAndViews){
			foreach(object obj in objectAndViews){
				if (obj is string) {
					continue;
				}
				UIView v;
				try{
					v = (UIView)obj;
				}
				catch(Exception e){
					continue;
				}
				if (v == null) {
					continue;
				}
				v.TranslatesAutoresizingMaskIntoConstraints = false;
			}
			foreach (var format in formats) {
				view.AddConstraints(NSLayoutConstraint.FromVisualFormat(format, NSLayoutFormatOptions.DirectionMask, objectAndViews));
			}
		}
		static NSLayoutConstraint CompileConstraint (BinaryExpression expr)
		{
			var rel = NSLayoutRelation.Equal;
			switch (expr.NodeType) {
			case ExpressionType.Equal:
				rel = NSLayoutRelation.Equal;
				break;
			case ExpressionType.LessThanOrEqual:
				rel = NSLayoutRelation.LessThanOrEqual;
				break;
			case ExpressionType.GreaterThanOrEqual:
				rel = NSLayoutRelation.GreaterThanOrEqual;
				break;
			default:
				throw new NotSupportedException ("Not a valid relationship for a constrain.");
			}

			var left = GetViewAndAttribute (expr.Left);
			left.Item1.TranslatesAutoresizingMaskIntoConstraints = false;

			var right = GetRight (expr.Right);
			if (right.Item1 != null) {
				right.Item1.TranslatesAutoresizingMaskIntoConstraints = false;
			}

			return NSLayoutConstraint.Create (
				left.Item1, left.Item2,
				rel,
				right.Item1, right.Item2,
				right.Item3, right.Item4);
		}

		static Tuple<UIView, NSLayoutAttribute, float, float> GetRight (Expression expr)
		{
			var r = expr;

			UIView view = null;
			NSLayoutAttribute attr = NSLayoutAttribute.NoAttribute;
			var mul = 1.0f;
			var add = 0.0f;
			var pos = true;

			if (r.NodeType == ExpressionType.Add || r.NodeType == ExpressionType.Subtract) {
				var rb = (BinaryExpression)r;
				if (rb.Left.NodeType == ExpressionType.Constant) {
					add = Convert.ToSingle (Eval (rb.Left));
					if (r.NodeType == ExpressionType.Subtract) {
						pos = false;
					}
					r = rb.Right;
				}
				else if (rb.Right.NodeType == ExpressionType.Constant) {
					add = Convert.ToSingle (Eval (rb.Right));
					if (r.NodeType == ExpressionType.Subtract) {
						add = -add;
					}
					r = rb.Left;
				}
				else {
					throw new NotSupportedException ("Addition only supports constants.");
				}
			}

			if (r.NodeType == ExpressionType.Multiply) {
				var rb = (BinaryExpression)r;
				if (rb.Left.NodeType == ExpressionType.Constant) {
					mul = Convert.ToSingle (Eval (rb.Left));
					r = rb.Right;
				}
				else if (rb.Right.NodeType == ExpressionType.Constant) {
					mul = Convert.ToSingle (Eval (rb.Right));
					r = rb.Left;
				}
				else {
					throw new NotSupportedException ("Multiplication only supports constants.");
				}
			}

			if (r.NodeType == ExpressionType.MemberAccess) {
				var t = GetViewAndAttribute (r);
				view = t.Item1;
				attr = t.Item2;
			} else if (r.NodeType == ExpressionType.Constant) {
				add = Convert.ToSingle (Eval (r));
			} else {
				throw new NotSupportedException ("Unknown node type " + r.NodeType);
			}

			if (!pos)
				mul = -mul;

			return Tuple.Create (view, attr, mul, add);
		}

		static Tuple<UIView, NSLayoutAttribute> GetViewAndAttribute (Expression expr)
		{
			var attrExpr = expr as MemberExpression;
			if (attrExpr == null)
				throw new NotSupportedException ("Left hand side of a relation must be a simple member expression");

			var attr = NSLayoutAttribute.NoAttribute;
			switch (attrExpr.Member.Name) {
			case "Width":
				attr = NSLayoutAttribute.Width;
				break;
			case "Height":
				attr = NSLayoutAttribute.Height;
				break;
			case "Left":
			case "X":
				attr = NSLayoutAttribute.Left;
				break;
			case "Top":
			case "Y":
				attr = NSLayoutAttribute.Top;
				break;
			case "Right":
				attr = NSLayoutAttribute.Right;
				break;
			case "Bottom":
				attr = NSLayoutAttribute.Bottom;
				break;
			default:
				throw new NotSupportedException ("Property " + attrExpr.Member.Name + " is not recognized.");
			}

			var frameExpr = attrExpr.Expression as MemberExpression;
			if (frameExpr == null)
				throw new NotSupportedException ("Constraints should use the Frame or Bounds property of views.");
			var viewExpr = frameExpr.Expression;

			var view = Eval (viewExpr) as UIView;
			if (view == null)
				throw new NotSupportedException ("Constraints only apply to views.");

			return Tuple.Create (view, attr);
		}

		static object Eval (Expression expr)
		{
			if (expr.NodeType == ExpressionType.Constant) {
				return ((ConstantExpression)expr).Value;
			} else {
				return Expression.Lambda (expr).Compile ().DynamicInvoke ();
			}
		}

		static void FindConstraints (Expression expr, List<BinaryExpression> constraintExprs)
		{
			var b = expr as BinaryExpression;
			if (b == null)
				return;

			if (b.NodeType == ExpressionType.AndAlso) {
				FindConstraints (b.Left, constraintExprs);
				FindConstraints (b.Right, constraintExprs);
			} else {
				constraintExprs.Add (b);
			}
		}
	}
}

