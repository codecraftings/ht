using System;

using UIKit;

using HauteShot.Core;
using Foundation;
using CoreGraphics;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HauteShot.iOS
{
	public class FeedTableSource:UITableViewSource
	{
		public List<Photo> FeedItems {
			get;
			set;
		}

		string cellID = "feedViewCell";
		public NSIndexPath CurrentIndexPath;
		public Action<Photo> OnPhotoSelected;
		public Action<int> OnUserSelected;
		public event Action OnApprochingEnd;

		public FeedTableSource (List<Photo> items)
		{
			FeedItems = items;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return FeedItems.Count;
		}
		private void seeIfApproachingEnd(int feedIndex){
			if (FeedItems.Count < 1) {
				return;
			}
			if (feedIndex == (FeedItems.Count-1)) {
				if (OnApprochingEnd != null) {
					OnApprochingEnd.Invoke ();
				}
			}
		}
		public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			return tableView.Bounds.Width + 60 + 0;
		}
		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			FeedTableCell cell = tableView.DequeueReusableCell (cellID) as FeedTableCell;
			if (cell == null) {
				cell = new FeedTableCell (cellID);
			}
			CurrentIndexPath = indexPath;
			seeIfApproachingEnd (indexPath.Row);
			Photo feed = FeedItems [indexPath.Row];
			if (feed != null) {
				cell.PhotoData = feed;
				cell.UpdateView ();
			}
			cell.OnClickPhoto = (photo) => {
				if(OnPhotoSelected!=null){
					OnPhotoSelected(photo);
				}
			};
			cell.OnClickUser = id => {
				if (OnUserSelected != null) {
					OnUserSelected (id);
				}
			};
			return cell;
		}
	}
}

