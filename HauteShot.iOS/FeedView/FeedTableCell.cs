﻿using System;
using UIKit;
using System.ComponentModel;
using Foundation;
using System.Drawing;
using CoreGraphics;
using HauteShot.Core;
using System.Text.RegularExpressions;

namespace HauteShot.iOS
{
	[Register ("FeedTableCell"), DesignTimeVisible (true)]
	public class FeedTableCell:UITableViewCell
	{
		public UILabel UserLabel;
		public UILabel TimeLabel;
		public UIImageView PhotoHolder;
		public UIImageView UserPhoto;
		public UITextView CaptionTf;
		public HoverableButton FlagButton;
		public PromoteButtonView PromoterView;

		public Photo PhotoData {
			get;
			set;
		}

		public Action<Photo> OnClickPhoto;
		public Action<int> OnClickUser;

		public FeedTableCell (IntPtr p) : base (p)
		{

		}

		public FeedTableCell (string cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			this.Initialize ();
		}

		public override void AwakeFromNib ()
		{
			//base.AwakeFromNib ();
			this.Initialize ();
		}

		public void UpdateView ()
		{
			PromoterView.PhotoData = PhotoData;
			PromoterView.UpdateView ();
			SetCaption (PhotoData.caption);
			UserLabel.Text = PhotoData.user.FullName;
			TimeLabel.Text = PhotoData.GetHumanDate ();
			var uid = PhotoData.id;
			Tag = PhotoData.id;
			PhotoHolder.Image = null;
			UserPhoto.Image = null;
			ImageLoader.Instance.LoadImage (PhotoData.GetThumbSrc ((float)UIScreen.MainScreen.Bounds.Width), img => {
				if (Tag == uid)
					PhotoHolder.Image = img;
			});
			ImageLoader.Instance.LoadImage (PhotoData.user.ProfilePicUrl, img => {
				if (Tag == uid)
					UserPhoto.Image = img;
			});
		}

		private void Initialize ()
		{
			UserLabel = new UILabel {
				Text = "Image Label Here",
				Font = UIFont.SystemFontOfSize (14),
				TextColor = UIColor.DarkGray,
			};
			TimeLabel = new UILabel {
				Text = "....",
				Font = UIFont.SystemFontOfSize (12),
				TextColor = UIColor.Gray
			};
			PromoterView = new PromoteButtonView {
				PhotoData = PhotoData
			};
			UserPhoto = new UIImageView {
				BackgroundColor = UIColor.FromRGB (43, 43, 43),
				ContentMode = UIViewContentMode.ScaleAspectFill,
				ClipsToBounds = true
			};
			UserPhoto.Layer.CornerRadius = 20;
			PhotoHolder = new UIImageView {
				BackgroundColor = UIColor.Gray,
				ContentMode = UIViewContentMode.ScaleAspectFit
			};

			CaptionTf = new UITextView {
				Selectable = false,
				ScrollEnabled = false,
				TextColor = UIColor.FromRGB (43, 43, 43),
				ContentMode = UIViewContentMode.ScaleAspectFill,
				Font = UIFont.SystemFontOfSize (12)
			};

			FlagButton = new HoverableButton ("Icons/flag.png", UIColor.FromRGBA (34, 34, 34, 150), UIColor.FromRGBA (34, 34, 34, 250), 8);
			FlagButton.BackgroundColor = UIColor.FromRGBA (255, 255, 255, 150);
			FlagButton.TouchUpInside += (object sender, EventArgs e) => {
				var manager = new ReportPhotoManager (this.PhotoData);
				manager.ShowFlagActions ();
			};

			this.BackgroundColor = UIColor.Clear;
			this.ContentView.BackgroundColor = UIColor.White;


			ContentView.AddSubviews (new UIView[] {
				UserLabel,
				TimeLabel,
				PromoterView,
				UserPhoto,
				PhotoHolder,
				CaptionTf,
				FlagButton
			});
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			UITouch touch = touches.AnyObject as UITouch;
			if (touch != null && touch.TapCount == 1) {
				if (PhotoHolder.Frame.Contains (touch.LocationInView (this))) {
					Console.WriteLine ("ImageTapped");
					SetShadow (.3f);
					if (OnClickPhoto != null) {
						OnClickPhoto (PhotoData);
					}
				}
				if (UserPhoto.Frame.Contains (touch.LocationInView (this)) || UserLabel.Frame.Contains (touch.LocationInView (this))) {
					if (OnClickUser != null) {
						OnClickUser (PhotoData.user_id);
					}
				}
			}
		}

		private void SetShadow (float size = .5f, float opacity = .3f)
		{
			this.ContentView.Layer.ShadowColor = UIColor.Black.CGColor;
			this.ContentView.Layer.ShadowOpacity = opacity;
			this.ContentView.Layer.ShadowOffset = new SizeF (size, size);
			this.ContentView.Layer.ShadowPath = CGPath.FromRect (ContentView.Bounds);
		}

		public void SetCaption (string text)
		{
			if (String.IsNullOrWhiteSpace (text)) {
				CaptionTf.Hidden = true;
				return;
			} else {
				CaptionTf.Hidden = false;
			}
			MatchCollection matches = Regex.Matches (text, "#(\\w+)");
			var formatedText = new NSMutableAttributedString (text);
			foreach (Match match in matches) {
				formatedText.AddAttributes (new UIStringAttributes {
					ForegroundColor = UIColor.FromRGB (0, 128, 128)
				}, new NSRange (match.Index, match.Length));
			}
			CaptionTf.AttributedText = formatedText;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			ContentView.Frame = new CGRect (4, 5, Bounds.Width - 8, Bounds.Height - 20);
			this.SetShadow ();
			UserPhoto.Frame = new CGRect (7, 5, 40, 40);
			UserLabel.Frame = new CGRect (57, 7, ContentView.Bounds.Width - 30, 20f);
			TimeLabel.Frame = new CGRect (57, 22, ContentView.Bounds.Width - 30, 20f);
			PromoterView.Frame = new CGRect (ContentView.Bounds.Width - 115, 0, 115, 50);
			PhotoHolder.Frame = new CGRect (2f, 50f, ContentView.Bounds.Width - 4, ContentView.Bounds.Height - 52);
			CaptionTf.Frame = new CGRect (PhotoHolder.Frame.Left, PhotoHolder.Frame.Bottom - CaptionTf.IntrinsicContentSize.Height, PhotoHolder.Frame.Width, CaptionTf.IntrinsicContentSize.Height);
			FlagButton.Frame = new CGRect (ContentView.Bounds.Width - 32, ContentView.Bounds.Bottom - 32, 32, 32);
//			FlagButton.Frame = new CGRect(100, 100, 50, 50);
			var frame = CaptionTf.Frame;
			frame.Width = PhotoHolder.Frame.Width;
			CaptionTf.Frame = frame;
			CaptionTf.SizeToFit ();
			frame = CaptionTf.Frame;
			CaptionTf.Frame = new CGRect (PhotoHolder.Frame.Left, PhotoHolder.Frame.Bottom - frame.Height, PhotoHolder.Frame.Width, frame.Height);
		}
	}
}

