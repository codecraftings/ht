using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using HauteShot.Core;
using CoreGraphics;
using System.Collections.Generic;

namespace HauteShot.iOS
{
	partial class FeedViewController : BaseUIController
	{
		public new FeedViewModel ViewModel {
			get {
				return (FeedViewModel)base.ViewModel;
			}
			set {
				base.ViewModel = value as FeedViewModel;
			}
		}

		private UIRefreshControl refreshControl;
		private UITableViewController tableViewController;
		private FeedTableSource feedsource;

		public FeedViewController (IntPtr handle) : base (handle)
		{
			tableViewController = new UITableViewController ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
//			this.FeedTable.RowHeight = FeedTable.Bounds.Width + 60+20;
			this.ViewModel = new FeedViewModel (this);
			tableViewController.TableView = FeedTable;
			tableViewController.RefreshControl = refreshControl;
			feedsource = new FeedTableSource (ViewModel.FeedItems);
			FeedTable.Source = feedsource;
			FeedTable.ScrollIndicatorInsets = new UIEdgeInsets (0, 0, 50, 0);
			feedsource.OnPhotoSelected += photo => {
				Navigator.PhotoScreen (photo.id);
			};
			feedsource.OnUserSelected = id => {
				Navigator.UserProfileScreen (id);
			};
			feedsource.OnApprochingEnd += () => {
				LoadMoreFeedItems ();
			};
			FeedTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			FeedTable.ContentInset = new UIEdgeInsets (0, 0, 50, 0);
			this.OnViewModelUpdated (null);
			ViewModel.FetchFromCache ();
		}

		private bool flagLoadingMore;

		public void LoadMoreFeedItems ()
		{
			if (flagLoadingMore) {
				return;
			}
			addTableFooter ();
			ShowUIBusy (FeedTable.TableFooterView);
			flagLoadingMore = true;
			ViewModel.FetchOlderFeeds ().AndThen (() => {
				InvokeOnMainThread (() => {
					HideUIBusy (FeedTable.TableFooterView);
					FeedTable.TableFooterView = null;
					flagLoadingMore = false;
				});
			});
		}

		private void addTableFooter ()
		{
			var view = new CardView ();
			view.BackgroundColor = UIColor.White;
			view.Frame = new CGRect (0, 0, FeedTable.Bounds.Width - 12, 50);
			FeedTable.TableFooterView = view;
		}

		public void RefreshTableData (object sender, EventArgs e)
		{
			ViewModel.FetchFreshFeeds ().AndThen (() => {
				InvokeOnMainThread (() => {
					if (refreshControl != null)
						refreshControl.EndRefreshing ();
				});
			});
		}

		public override void NotifyViewAppeared ()
		{
			NavController.NeedFooter ();
			NavController.NoBackButton ();
			NavController.HaveShadow ();
			initRefreshControl ();
			if (feedsource.CurrentIndexPath == null || feedsource.CurrentIndexPath.Row < 2)
				FeedTable.ContentOffset = new CGPoint (0, -refreshControl.Frame.Size.Height);
			refreshControl.BeginRefreshing ();
			RefreshTableData (null, null);
			NavController.FooterMenu ().TabHome ();
		}

		public override void NotifyViewDisappearing ()
		{
			destroyRefreshControl ();
		}

		void initRefreshControl ()
		{
			refreshControl = new UIRefreshControl ();
			refreshControl.BackgroundColor = UIColor.White;
			refreshControl.ValueChanged += RefreshTableData;
			tableViewController.RefreshControl = refreshControl;
		}

		void destroyRefreshControl ()
		{
			refreshControl = null;
			tableViewController.RefreshControl = null;
		}

		public override void OnViewModelUpdated (string updateId)
		{
			this.FeedTable.ReloadData ();
		}
	}
}
