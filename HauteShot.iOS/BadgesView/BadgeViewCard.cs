﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;

namespace HauteShot.iOS
{
	[Register("BadgeView"), DesignTimeVisible(true)]
	public class BadgeViewCard:CardView
	{
		[Export("IsExpanded"), Browsable(true)]
		public bool IsExpanded{get;set;}
		public bool Collapsible{ get; set; }
		private UIView DetailView;
		private UIView BarView;
		public UIImageView BadgeIcon;
		public UILabel ContestTitle;
		public UILabel ContestTimeline;
		public UILabel ContestStatus;
		public CircularImage WinnerPhoto;
		public UILabel TopLabel1;
		public UILabel PromoteCount;
		private UIView separator;
		public event EventHandler OnDetailsCardTapped;
		private NSLayoutConstraint DetailTopConstraint;
		private NSLayoutConstraint BarTopConstraint;
		private NSLayoutConstraint BarDetailConstraint;

		public BadgeViewCard(IntPtr p):base(p)
		{
		}
		public BadgeViewCard (bool is_expanded = false, bool collapsible = true):base(collapsible)
		{
			IsExpanded = is_expanded;
			Collapsible = collapsible;
			Inititalize();
		}
		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			Inititalize ();
		}
		private void Inititalize(){
			TranslatesAutoresizingMaskIntoConstraints = false;
			DetailView = new UIView ();
			BarView = new UIView ();
			AddSubviews (new UIView[]{ DetailView, BarView });
			DetailTopConstraint = NSLayoutConstraint.Create (DetailView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this, NSLayoutAttribute.Top, 1, 0);
			BarTopConstraint = NSLayoutConstraint.Create (BarView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this, NSLayoutAttribute.Top, 1, 0);
			BarDetailConstraint = NSLayoutConstraint.Create (BarView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, DetailView, NSLayoutAttribute.Bottom, 1, 0);
			AddConstraint (BarTopConstraint);

			this.DefineLayout (new string[] {
				"|[BarView]|",
				"|[DetailView]|",
				"V:|[DetailView]",
				"V:[BarView]|"
			}, "BarView", BarView, "DetailView", DetailView);
			TopLabel1 = new UILabel {
				Text = "Winner",
				Font = UIFont.SystemFontOfSize (13),
				TextColor = UIColor.FromRGB (76, 76, 76),
				TextAlignment = UITextAlignment.Center
			};
			PromoteCount = new UILabel {
				Text = "00 promotes",
				Font = UIFont.SystemFontOfSize (13),
				TextColor = UIColor.FromRGB (76, 76, 76),
				TextAlignment = UITextAlignment.Center
			};
			WinnerPhoto = new CircularImage {
				Image = UIImage.FromBundle("Sample/photo10.jpg"),
				BackgroundColor = UIColor.DarkGray,
				ContentMode = UIViewContentMode.ScaleAspectFill
			};
			separator = new UIView {
				BackgroundColor = UIColor.LightGray
			};
			DetailView.AddSubviews (new UIView[]{ TopLabel1, PromoteCount, WinnerPhoto, separator });

			DetailView.DefineLayout (new string[] {
				"|-10-[TopLabel1]-10-|",
				"[WinnerPhoto(90)]",
				"|-10-[PromoteCount]-10-|",
				"|-5-[separator]-5-|",
				"V:|-5-[TopLabel1]-2-[WinnerPhoto(90)]-2-[PromoteCount]-5-[separator]|"
			}, "TopLabel1", TopLabel1, "WinnerPhoto", WinnerPhoto, "PromoteCount", PromoteCount, "separator", separator);
			DetailView.AddConstraint(NSLayoutConstraint.Create(WinnerPhoto, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, DetailView, NSLayoutAttribute.CenterX, 1, 0));
			BadgeIcon = new UIImageView {
				Image = UIImage.FromBundle ("Sample/badge1.png"),
				ContentMode = UIViewContentMode.ScaleAspectFill
			};
			ContestTitle = new UILabel {
				Text = "Contest Title Goes Here",
				Font = UIFont.SystemFontOfSize(14),
				TextColor = UIColor.FromRGB(51,51,51)
			};
			ContestTimeline = new UILabel {
				Text = "1st Jan to 7th Jan.",
				Font = UIFont.SystemFontOfSize(11),
				TextColor = UIColor.DarkGray
			};
			ContestStatus = new UILabel {
				Text = "Ended 3 days ago",
				Font = UIFont.SystemFontOfSize(12),
				TextColor = UIColor.LightGray
			};
			BarView.AddSubviews (new UIView[]{ BadgeIcon, ContestTitle, ContestTimeline, ContestStatus });

			BarView.DefineLayout (new string[] {
				"|-10-[BadgeIcon(60)]-12-[ContestTitle]-15-|",
				"[BadgeIcon]-12-[ContestTimeline]-15-|",
				"[BadgeIcon]-12-[ContestStatus]-15-|",
				"V:|-5-[BadgeIcon(60)]-5-|",
				"V:|-10-[ContestTitle]-1-[ContestTimeline]-1-[ContestStatus]"
			}, "BadgeIcon", BadgeIcon, "ContestTitle", ContestTitle, "ContestTimeline", ContestTimeline, "ContestStatus", ContestStatus);
			if (IsExpanded) {
				ShowDetail ();
			} else {
				DetailView.Hidden = true;
			}
			//DetailView.ClipsToBounds = true;
		}
		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			var touch = touches.AnyObject as UITouch;
			if (touch != null) {
				if (touch.TapCount == 1) {
					if (BarView.Frame.Contains (touch.LocationInView (this))) {
						toggleDetail ();
						return;
					}
					if (DetailView.Frame.Contains (touch.LocationInView (this))) {
						handleDetailClicked ();
					}
				}
			}
		}
		public override void LayoutSubviews ()
		{
			//this.UpdateConstraints ();
			base.LayoutSubviews ();
		}
		private void handleDetailClicked(){
			if (OnDetailsCardTapped != null) {
				OnDetailsCardTapped.Invoke (this, new EventArgs ());
			}
		}
		private void toggleDetail(){
			if (!Collapsible)
				return;
			if (IsExpanded)
				hideDetail ();
			else
				ShowDetail();
			//this.LayoutSubviews ();
		}
		public void ShowDetail(){
			DetailView.Hidden = false;
			DetailView.Alpha = 0;
			this.RemoveConstraint (BarTopConstraint);
			this.AddConstraints (new NSLayoutConstraint[]{ DetailTopConstraint, BarDetailConstraint });
			IsExpanded = true;

			Animate (.3, 0, UIViewAnimationOptions.CurveEaseIn, () => {
				DetailView.Alpha = 1;
			}, null);
		}
		private void hideDetail(){
			DetailView.Hidden = true;
			this.RemoveConstraints(new NSLayoutConstraint[]{ DetailTopConstraint, BarDetailConstraint });
			this.AddConstraint (BarTopConstraint);
			IsExpanded = false;
		}
	}
}

