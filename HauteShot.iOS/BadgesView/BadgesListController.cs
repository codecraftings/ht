using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using HauteShot.Core;
using System.Collections.Generic;

namespace HauteShot.iOS
{
	partial class BadgesListController : BaseUIController
	{
		public new BadgesListViewModel ViewModel {
			get {
				return base.ViewModel as BadgesListViewModel;
			}
			set {
				base.ViewModel = value as BaseViewModel;
			}
		}

		private List<BadgeViewCard> BadgeCards;
		private NSLayoutConstraint bottomConstrain;
		public User User;

		public BadgesListController (IntPtr handle) : base (handle)
		{
			this.ViewModel = new BadgesListViewModel (this);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			this.View.AddConstraint (NSLayoutConstraint.Create (ScrollContainer, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View, NSLayoutAttribute.Width, 1, 0));
			ViewModel.FetchBadges (User);

			OnViewModelUpdated ("all");
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			NavController.HaveShadow ();
		}

		public override void OnViewModelUpdated (string updateID)
		{
			base.OnViewModelUpdated (updateID);
			if (ViewModel.BadgesList == null) {
				ShowUIBusy (ScrollContainer);
				return;
			}
			foreach (var badge in ViewModel.BadgesList) {
				addBadgeCard (badge);
			}
			HideUIBusy (ScrollContainer);

		}

		private void addBadgeCard (Badge badgeData)
		{
			var newCard = new BadgeViewCard (false);
			newCard.ContestTitle.Text = badgeData.Caption;
			newCard.ContestTimeline.Text = badgeData.Contest.StartDate.ToShortDateString () + " to " + badgeData.Contest.EndDate.ToShortDateString ();
			newCard.ContestStatus.Text = badgeData.Contest.GetEndDateInDays ();
			newCard.BadgeIcon.Image = null;
			ImageLoader.Instance.LoadImage (badgeData.IconUrl, image => newCard.BadgeIcon.Image = image);
			newCard.WinnerPhoto.Image = null;
			if (badgeData.Winner != null) {
				newCard.PromoteCount.Text = badgeData.Winner.promotes_count + " promotes";
				ImageLoader.Instance.LoadImage (badgeData.Winner.original_url, img => newCard.WinnerPhoto.Image = img);
				newCard.OnDetailsCardTapped += (object sender, EventArgs e) => {
					Navigator.PhotoScreen(badgeData.Winner.id);
				};
			}
			ScrollContainer.AddSubview (newCard);

			ScrollContainer.AddConstraint (NSLayoutConstraint.Create (newCard, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ScrollContainer, NSLayoutAttribute.Width, 1, -12));
			ScrollContainer.DefineLayout (new string[] {
				"|-6-[newCard]"
			}, "newCard", newCard);
			if (BadgeCards == null) {
				BadgeCards = new List<BadgeViewCard> ();
				ScrollContainer.DefineLayout (new string[] {
					"V:|-10-[newCard]"
				}, "newCard", newCard);
			} else {
				if (bottomConstrain != null) {
					ScrollContainer.RemoveConstraint (bottomConstrain);
				}
				var prev = BadgeCards [BadgeCards.Count - 1];
				ScrollContainer.DefineLayout (new string[] {
					"V:[prev]-10-[newCard]"
				}, "prev", prev, "newCard", newCard);
			}
			bottomConstrain = NSLayoutConstraint.Create (newCard, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, ScrollContainer, NSLayoutAttribute.Bottom, 1, -25);
			ScrollContainer.AddConstraint (bottomConstrain);
			BadgeCards.Add (newCard);
		}

		public override void NotifyViewAppeared ()
		{
			base.NotifyViewAppeared ();
		}
	}
}
