using System;
using Foundation;
using UIKit;
using HauteShot.Core;
using System.Collections.Generic;

namespace HauteShot.iOS
{
	partial class ContestsListController : BaseUIController
	{
		public new ContestsViewModel ViewModel {
			get {
				return base.ViewModel as ContestsViewModel;
			}
			set {
				base.ViewModel = value as BaseViewModel;
			}
		}

		private List<BadgeViewCard> ContestCards;
		private NSLayoutConstraint bottomConstrain;

		public ContestsListController (IntPtr handle) : base (handle)
		{
			this.ViewModel = new ContestsViewModel (this);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			this.View.AddConstraint (NSLayoutConstraint.Create (ScrollContainer, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View, NSLayoutAttribute.Width, 1, 0));


			OnViewModelUpdated ("all");
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			NavController.HaveShadow ();
		}

		public override void OnViewModelUpdated (string updateID)
		{
			base.OnViewModelUpdated (updateID);
			if (ViewModel.ContestsList == null) {
				ShowUIBusy (ScrollContainer);
				return;
			}
			if (ContestCards != null) {
				foreach (var card in ContestCards) {
					card.RemoveFromSuperview ();
				}
				ContestCards = null;
			}
			foreach (var contest in ViewModel.ContestsList) {
				addContestCard (contest);
			}
			HideUIBusy (ScrollContainer);

		}

		private void addContestCard (Contest contest)
		{
			var newCard = new BadgeViewCard (false);
			newCard.ContestTitle.Text = contest.Title;
			newCard.ContestTimeline.Text = contest.FriendlyDateInfo;
			newCard.ContestStatus.Text = "Entries: "+contest.TotalEntry + " (Ends: "+contest.GetEndDateInDays ()+")";
			newCard.BadgeIcon.Image = null;
			ImageLoader.Instance.LoadImage (contest.IconUrl, image => newCard.BadgeIcon.Image = image);
			newCard.WinnerPhoto.Image = null;
			if (contest.RankList != null&&contest.RankList.Count>0) {
				newCard.PromoteCount.Text = contest.RankList[0].promotes_count + " promotes";
				ImageLoader.Instance.LoadImage (contest.RankList[0].original_url, img => newCard.WinnerPhoto.Image = img);
				newCard.OnDetailsCardTapped += (object sender, EventArgs e) => {
					Navigator.PhotoScreen(contest.RankList[0].id);
				};
			}
			newCard.TopLabel1.Text = "Currently Top";

			ScrollContainer.AddSubview (newCard);

			ScrollContainer.AddConstraint (NSLayoutConstraint.Create (newCard, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ScrollContainer, NSLayoutAttribute.Width, 1, -12));
			ScrollContainer.DefineLayout (new string[] {
				"|-6-[newCard]"
			}, "newCard", newCard);
			if (ContestCards == null) {
				ContestCards = new List<BadgeViewCard> ();
				ScrollContainer.DefineLayout (new string[] {
					"V:|-10-[newCard]"
				}, "newCard", newCard);
			} else {
				if (bottomConstrain != null) {
					ScrollContainer.RemoveConstraint (bottomConstrain);
				}
				var prev = ContestCards [ContestCards.Count - 1];
				ScrollContainer.DefineLayout (new string[] {
					"V:[prev]-10-[newCard]"
				}, "prev", prev, "newCard", newCard);
			}
			bottomConstrain = NSLayoutConstraint.Create (newCard, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, ScrollContainer, NSLayoutAttribute.Bottom, 1, -25);
			ScrollContainer.AddConstraint (bottomConstrain);
			ContestCards.Add (newCard);
			newCard.ShowDetail ();
		}

		public override void NotifyViewAppeared ()
		{
			base.NotifyViewAppeared ();
			NavController.FooterMenu ().TabRank ();
			ViewModel.FetchContestList ();
		}
	}
}
