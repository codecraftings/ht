﻿using System;
using UIKit;
using HauteShot.Core;
using System.Collections.Generic;

namespace HauteShot.iOS
{
	public abstract class BaseUIController:UIViewController, IViewController
	{
		public BaseViewModel ViewModel {
			get;
			set;
		}
		public HTNavigator Navigator {
			get {
				return HTNavigator.Instance;
			}
		}
		public MainNavController NavController{
			get{
				return ParentViewController as MainNavController;
			}
		}
		private Dictionary<UIView, UIActivityIndicatorView> loaders;
		public BaseUIController (IntPtr p) : base (p)
		{
		}

		public void CallDataChangeAction (Action action)
		{
			InvokeOnMainThread (() => {
				action.Invoke ();
			});
		}

		public new void Dispose ()
		{
			if (ViewModel != null) {
				ViewModel.Dispose ();
				ViewModel = null;
			}
			base.Dispose ();
		}

		public bool IsVisible ()
		{
			return this.IsViewLoaded;
		}

		public void InvokeOnOwnThread (Action action)
		{
			InvokeOnMainThread (action);
		}
		public virtual void NotifyViewAppeared(){

		}
		public virtual void NotifyViewDisappearing(){

		}
		public void NotifyViewModelUpdated(){
			NotifyViewModelUpdated ("none");
		}
		public void NotifyViewModelUpdated(string updateID){
			InvokeOnMainThread (() => {
				this.OnViewModelUpdated (updateID);
			});
		}
		public virtual void OnViewModelUpdated(string updateID){

		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			/*
			var logo = new UIImageView (UIImage.FromBundle ("logo.png"));
			logo.ContentMode = UIViewContentMode.ScaleAspectFit;
			logo.Bounds = new CoreGraphics.CGRect (0, 0, 50, 20);
			this.NavigationItem.TitleView = logo;
			this.NavigationItem.HidesBackButton = false;
			*/
		}
		public override void ViewDidAppear (bool animated)
		{

			base.ViewDidAppear (animated);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
		}
		public bool IsUIBusy(UIView container){
			if (loaders != null && loaders.ContainsKey (container)) {
				return true;
			}
			return false;
		}
		public void ShowUIBusy(UIView container){
			ShowUIBusy (container, null, null, 0f, 0f);
		}
		public void ShowUIBusy(UIView container, UIColor color, UIColor bgColor, nfloat width, nfloat height){
			if (loaders == null) {
				loaders = new Dictionary<UIView, UIActivityIndicatorView> ();
			}
			if (loaders.ContainsKey (container)) {
				return;
			}
			var loader = new UIActivityIndicatorView {
				Color = color!=null?color:UIColor.FromRGB (76, 76, 76),
				BackgroundColor = bgColor!=null?bgColor:UIColor.Clear
			};
			container.AddSubview (loader);
			loader.Hidden = false;
			loader.StartAnimating ();
			if (width > 0 && height > 0) {
				loader.Bounds = new CoreGraphics.CGRect (0f, 0f, width, height);
			}
			loader.Frame = new CoreGraphics.CGRect (container.Bounds.Width / 2 - loader.Bounds.Width / 2, container.Bounds.Height / 2 - loader.Bounds.Height / 2, loader.Bounds.Width, loader.Bounds.Height);
			loaders.Add (container, loader);
		}
		public void HideUIBusy(UIView container){
			if (loaders == null) {
				loaders = new Dictionary<UIView, UIActivityIndicatorView> ();
			}
			if (!loaders.ContainsKey (container)) {
				return;
			}
			var loader = loaders [container];
			loader.StopAnimating ();
			loader.Hidden = true;
			loader.RemoveFromSuperview ();
			loaders.Remove (container);
		}
	}
}

