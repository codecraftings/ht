﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;
using System.Drawing;

namespace HauteShot.iOS
{
	[Register("EffectsPickerView"), DesignTimeVisible(true)]
	public class EffectsPickerView:UIScrollView
	{
		public string[] effects;
		public EffectsPickerCell[] cells;
		public EffectsPickerCell SelectedCell;
		public Action<string> OnEffectSelected;
		public EffectsPickerView (IntPtr p):base(p)
		{
		}
		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			Initialize ();
		}
		private void Initialize(){
			effects = Editor.AvailableEffects;
			cells = new EffectsPickerCell[effects.Length];
			for (int i = 0; i < effects.Length; i++) {
				cells [i] = new EffectsPickerCell (UIImage.FromBundle ("Effects/" +effects[i]+ ".png"), this, effects[i]);
				this.AddSubview (cells [i]);
			}
		}
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			float padding = 5;
			nfloat h = Bounds.Height - 2 * padding;
			nfloat w = h;
			this.ContentSize = new CoreGraphics.CGSize ((w+padding) * effects.Length, Bounds.Height);
			for (int i = 0; i < cells.Length; i++) {
				cells [i].Frame = new CoreGraphics.CGRect (i * w+padding*(1+i), padding, w, h);
			}
		}
		public void OnCellSelected(EffectsPickerCell cell){
			if (SelectedCell != null) {
				if (SelectedCell.Equals (cell)) {
					return;
				}
				this.SelectedCell.undoSelected ();
			}
			cell.setSelected ();
			this.SelectedCell = cell;
			if (OnEffectSelected != null) {
				OnEffectSelected (cell.EffectID);
			}
		}
	}
	public class EffectsPickerCell:UIView
	{
		public UIImageView CellImageView;
		public UIImage CellImage;
		public string EffectID;
		public EffectsPickerView Picker;
		public EffectsPickerCell(UIImage Image, EffectsPickerView picker, string effectID):base()
		{
			EffectID = effectID;
			CellImage = Image;
			Picker = picker;
			init ();
		}
		public EffectsPickerCell(IntPtr p):base(p)
		{

		}
		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			init ();
		}
		private void init(){
			CellImageView = new UIImageView{
				Image = CellImage,
				ContentMode = UIViewContentMode.ScaleAspectFit
			};
			this.BackgroundColor = UIColor.Black;
			this.AddSubview(CellImageView);
		}
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			CellImageView.Frame = new CoreGraphics.CGRect (0, 0, Bounds.Width, Bounds.Height);
		}
		public void setSelected(){
			this.Alpha = 0.5f;
		}
		public void undoSelected(){
			this.Alpha = 1;
		}
		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			Console.WriteLine ("touch start");
			this.Alpha = 0.5f;
		}
		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			Picker.OnCellSelected (this);
			Console.WriteLine ("touch up");
			//this.Alpha = 1;
		}
		public override void TouchesCancelled (NSSet touches, UIEvent evt)
		{
			base.TouchesCancelled (touches, evt);
			Console.WriteLine ("touch cancelled");
			this.Alpha = 1;
		}
	}
}

