using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using CoreGraphics;
using System.Text.RegularExpressions;
using HauteShot.Core;

namespace HauteShot.iOS
{
	partial class PhotoInfoEditorController : BaseUIController
	{
		public int PhotoID;
		public bool NoBackButton;
		public UIImage DefaultPhoto;
		public new PhotoInfoEditorViewModel ViewModel {
			get {
				return (PhotoInfoEditorViewModel)base.ViewModel;
			}
			set {
				base.ViewModel = value as BaseViewModel;
			}
		}
		public PhotoInfoEditorController (IntPtr handle) : base (handle)
		{
			ViewModel = new PhotoInfoEditorViewModel (this);
		}
		public override void NotifyViewAppeared()
		{
			NavController.NoShadow ();
			NavController.RegisterActionFooterEvents (HandleDeleteButtonClicked, HandleDoneButtonClicked);
			NavController.NeedActionFooter ();
			if(!NoBackButton)
			NavController.NeedBackButton ();
			if (DefaultPhoto != null) {
				PhotoHolder.Image = DefaultPhoto;
			}
			if (PhotoID > 0) {
				ViewModel.LoadPhoto (PhotoID);
			}
			NotifyViewModelUpdated ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			setUpShareButtons ();
			setUpCaptionTf ();
			setUpKeyboardVisibility ();
			TwitterButton.TouchUpInside += HandleTwitterButtonClicked;
			FBButton.TouchUpInside += HandleFBButtonClicked;
			ScrollContainer.AddConstraint (NSLayoutConstraint.Create (PhotoHolder, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null, NSLayoutAttribute.Width, 1, UIScreen.MainScreen.Bounds.Width));
		}
		public override void NotifyViewDisappearing()
		{
			NavController.UnRegisterActionFooterEvents (HandleDeleteButtonClicked, HandleDoneButtonClicked);

		}
		public override void OnViewModelUpdated (string updateID)
		{
			base.OnViewModelUpdated (updateID);
			if (ViewModel.PhotoData == null) {
				NavController.ShowLoading ();
			} else {
				NavController.HideLoading ();
			}
			if (updateID == "dataLoaded") {
				ImageLoader.Instance.LoadImage (ViewModel.PhotoData.GetThumbSrc ((int)PhotoHolder.Bounds.Width, (int)PhotoHolder.Bounds.Height), img => {
					PhotoHolder.Image = img;
				});
				if (!String.IsNullOrEmpty (ViewModel.PhotoData.caption)) {
					CaptionTf.Text = ViewModel.PhotoData.caption;
					CaptionTf.DidChangeValue ("Text");
					onCaptionTfEditEnding (CaptionTf);
				}
			}
		}
		public void HandleDeleteButtonClicked(object sender, EventArgs e){
			NavController.ShowConfirmAlert ("Do you really want to delete the photo?", () => {
				NavController.HideLoading();
				NavController.ShowLoading(true);
				ViewModel.DeletePhoto(()=>{
					NavController.HideLoading();
					if(!Navigator.LastScreen()){
						Navigator.HomeFeedScreen();
					}
				});

			}, null);
		}
		public void HandleDoneButtonClicked(object sender, EventArgs e){
			NavController.HideLoading();
			NavController.ShowLoading(true);
			ViewModel.PhotoData.caption = CaptionTf.Text;
			ViewModel.UpdatePhoto (() => {
				NavController.HideLoading();
				Navigator.PhotoScreen (PhotoID);
			});
		}
		void HandleFBButtonClicked (object sender, EventArgs e)
		{
			UIApplication.SharedApplication.OpenUrl (new NSUrl (Server.BASE_URL+"share/facebook"));
		}

		void HandleTwitterButtonClicked (object sender, EventArgs e)
		{
			UIApplication.SharedApplication.OpenUrl (new NSUrl (Server.BASE_URL+"share/twitter"));
		}

		void setUpShareButtons ()
		{
			FBButton.HoverBGColor = UIColor.FromRGB(42,68,131);
			TwitterButton.HoverBGColor = UIColor.FromRGB (27, 136, 212);
			FBButton.ApplyMinimalShadow ();
			TwitterButton.ApplyMinimalShadow ();

		}

		void captionTfLayerDesign ()
		{
			CaptionTf.ApplyMinimalShadow ();
			CaptionTf.Layer.BorderColor = UIColor.FromRGB (233, 233, 233).CGColor;
			CaptionTf.Layer.BorderWidth = 0f;
			CaptionTf.Layer.CornerRadius = 0f;
			CaptionTf.TextContainerInset = new UIEdgeInsets (6, 4, 4, 4);
		}
		private string captionTfPlaceholder;
		private NSMutableAttributedString formatedPlaceholder;
		void setUpCaptionTf ()
		{
			captionTfLayerDesign ();
			captionTfPlaceholder = "add #tags or caption...";
			formatedPlaceholder = new NSMutableAttributedString (captionTfPlaceholder);
			formatedPlaceholder.AddAttributes (new UIStringAttributes {
				ForegroundColor = UIColor.FromRGB (179, 179, 179)
			}, new NSRange (0, captionTfPlaceholder.Length));
			CaptionTf.AttributedText = formatedPlaceholder;
			CaptionTf.ShouldBeginEditing += onCaptionTfEditStarting;
			CaptionTf.ShouldEndEditing += onCaptionTfEditEnding;
			CaptionTf.Changed += (object sender, EventArgs e) => {
				onCaptionTfChanged ();
			};
		}

		bool onCaptionTfEditStarting (UITextView textView)
		{
			if (textView.Text == captionTfPlaceholder) {
				textView.Text = "";
			}
			textView.TextColor = UIColor.FromRGB (76, 76, 76);
			onCaptionTfChanged ();
			return true;
		}

		bool onCaptionTfEditEnding (UITextView textView)
		{
			if (textView.Text == "") {
				textView.AttributedText = formatedPlaceholder;
			}
			return true;
		}

		void onCaptionTfChanged ()
		{
			MatchCollection matches = Regex.Matches (CaptionTf.Text, "#(\\w+)");
			var formatedText = new NSMutableAttributedString (CaptionTf.Text);
			foreach (Match match in matches) {
				formatedText.AddAttributes (new UIStringAttributes {
					ForegroundColor = UIColor.FromRGB(0,128,128)
				}, new NSRange (match.Index, match.Length));
			}
			CaptionTf.AttributedText = formatedText;
		}

		public void KeyboardVisible (NSNotification notification)
		{
			CGSize keyboardInfo = UIKeyboard.FrameEndFromNotification (notification).Size;
			var inset = new UIEdgeInsets (0, 0, keyboardInfo.Height, 0);
			ScrollContainer.ContentInset = inset;
			ScrollContainer.ScrollIndicatorInsets = inset;
			var activeField = ScrollContainer.FindFirstResponder ();
			if (activeField != null)
				ScrollContainer.ScrollRectToVisible (activeField.Frame, true);
		}

		public void KeyboardHidden (NSNotification notification)
		{
			ScrollContainer.ContentInset = UIEdgeInsets.Zero;
			ScrollContainer.ScrollIndicatorInsets = UIEdgeInsets.Zero;
		}

		protected void DismissKeyboardOnBackgroundTap ()
		{
			// Add gesture recognizer to hide keyboard
			var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
			tap.AddTarget (() => View.EndEditing (true));
			View.AddGestureRecognizer (tap);
		}

		void setUpKeyboardVisibility ()
		{
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, KeyboardVisible);
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, KeyboardHidden);
			DismissKeyboardOnBackgroundTap ();
		}
	}
}
