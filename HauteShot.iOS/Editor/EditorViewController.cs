using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using HauteShot.Core;
using System.IO;

namespace HauteShot.iOS
{
	partial class EditorViewController : BaseUIController
	{

		public new EditorViewModel ViewModel {
			get {
				return (EditorViewModel)base.ViewModel;
			}
			set {
				base.ViewModel = value as EditorViewModel;
			}
		}
		private IFileSystem fileSystem;
		public EditorViewController (IntPtr handle) : base (handle)
		{
			this.ViewModel = new EditorViewModel (this);
			fileSystem = ServiceContainer.Resolve<IFileSystem> ();
		}

		public override void NotifyViewAppeared ()
		{
			base.NotifyViewAppeared ();
			NavController.RegisterActionFooterEvents (cancelEditor, onDoneClicked);
			NavController.NeedActionFooter ();
		}

		public override void NotifyViewDisappearing ()
		{
			base.NotifyViewDisappearing ();
			NavController.UnRegisterActionFooterEvents (cancelEditor, onDoneClicked);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			View.AddConstraint (NSLayoutConstraint.Create (PhotoCard, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null, NSLayoutAttribute.Width, 1, View.Bounds.Width - 12));
			//			View.AddConstraint(NSLayoutConstraint.Create(PhotoCard, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, PhotoContainer, NSLayoutAttribute.CenterX, 1, 0));
			//			View.AddConstraint(NSLayoutConstraint.Create(PhotoContainer, NSLayoutAttribute.Height, NSLayoutRelation.Equal, PhotoCard, NSLayoutAttribute.Height, 1, 0));
			//			View.AddConstraint(NSLayoutConstraint.Create(PhotoCard, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, PhotoContainer, NSLayoutAttribute.CenterY, 1, 0));
			if (Editor.Instance.OriginalImage != null) {
				PhotoCard.SetImage (Editor.Instance.OriginalImage);
			}
			//			NavController.NoFooter ();
			EffectsContainer.OnEffectSelected += onEffectPicked;
//			NavController.RegisterActionFooterEvents (cancelEditor, onDoneClicked);
			NavController.NeedActionFooter ();
			NavController.HaveShadow ();
		}

		void onEffectPicked (string effectId)
		{
			ShowUIBusy (PhotoCard, UIColor.Black, UIColor.White, 30, 30);
			PhotoCard.Layer.Opacity = .3f;
			Editor.Instance.ApplyEffectAsync (effectId, (UIImage output) => {
				PhotoCard.SetImage (output);
				HideUIBusy (PhotoCard);
				PhotoCard.Layer.Opacity = 1f;
			}, UIScreen.MainScreen.Bounds.Width * 2, 0);
		}

		public void cancelEditor (object sender, EventArgs arg)
		{
			Editor.Instance.Dispose ();
			Navigator.LastScreen ();
		}

		public void onDoneClicked (object sender, EventArgs arg)
		{
			NavController.HideLoading ();
			NavController.ShowLoading (true);
			Editor.Instance.GetFinalImage (img => {
//				var dir = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
				var path = fileSystem.FromDocumentFolder (System.Guid.NewGuid ().ToString () + ".jpg").GetPath();
				NSData data = img.AsJPEG (100);
				data.Save (path, true);
				ViewModel.CreateImage (path, photo => {
					NavController.HideLoading ();
					Navigator.PhotoInfoEditorScreen (photo.id, false);
				}, error=>{
					NavController.HideLoading ();
				});
			});
		}

	}
}
