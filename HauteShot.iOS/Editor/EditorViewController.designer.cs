// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace HauteShot.iOS
{
	[Register ("EditorViewController")]
	partial class EditorViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView ButtonsContainer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		EffectsPickerView EffectsContainer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		PhotoViewer PhotoCard { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIScrollView PhotoContainer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel StsMsg { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView UploadingStatus { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ButtonsContainer != null) {
				ButtonsContainer.Dispose ();
				ButtonsContainer = null;
			}
			if (EffectsContainer != null) {
				EffectsContainer.Dispose ();
				EffectsContainer = null;
			}
			if (PhotoCard != null) {
				PhotoCard.Dispose ();
				PhotoCard = null;
			}
			if (PhotoContainer != null) {
				PhotoContainer.Dispose ();
				PhotoContainer = null;
			}
			if (StsMsg != null) {
				StsMsg.Dispose ();
				StsMsg = null;
			}
			if (UploadingStatus != null) {
				UploadingStatus.Dispose ();
				UploadingStatus = null;
			}
		}
	}
}
